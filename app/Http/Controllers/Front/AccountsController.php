<?php

namespace App\Http\Controllers\Front;

use App\Shop\Addresses\Repositories\Interfaces\AddressRepositoryInterface;
use App\Shop\Addresses\Transformations\AddressTransformable;
use App\Shop\Couriers\Repositories\Interfaces\CourierRepositoryInterface;
use App\Shop\Customers\Repositories\CustomerRepository;
use App\Shop\Customers\Repositories\Interfaces\CustomerRepositoryInterface;
use App\Http\Controllers\Controller;
use App\Shop\Orders\Transformers\OrderTransformable;
use App\Shop\Customers\Requests\UpdateCustomerRequestFront;
use App\Shop\Orders\Order;
use App\Shop\Orders\Repositories\Interfaces\OrderRepositoryInterface;
use App\Shop\Orders\Repositories\OrderRepository;
use App\Shop\OrderStatuses\OrderStatus;
use App\Shop\OrderStatuses\Repositories\Interfaces\OrderStatusRepositoryInterface;
use App\Shop\OrderStatuses\Repositories\OrderStatusRepository;
use App\Shop\Customers\Customer; 
use Illuminate\Http\Request;
use App\Shop\Wishlist\Wishlist;
use Hash;
use DB;
use Mail;
use App\Shop\Carts\Repositories\Interfaces\CartRepositoryInterface;
use Illuminate\Support\Facades\Password;
class AccountsController extends Controller
{
	use AddressTransformable;
	use OrderTransformable;
    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepo;

   
    private $addressRepo;

    
    /**
     * @var OrderStatusRepositoryInterface
     */
    private $orderStatusRepo;


    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepo;

    /**
     * @var CourierRepositoryInterface
     */
    private $courierRepo;

    /**
     * AccountsController constructor.
     *
     * @param CourierRepositoryInterface $courierRepository
     * @param CustomerRepositoryInterface $customerRepository
     */
    public function __construct(
		OrderRepositoryInterface $orderRepository,
        CourierRepositoryInterface $courierRepository,
        CustomerRepositoryInterface $customerRepository,
		CartRepositoryInterface $cartRepository,
		AddressRepositoryInterface $addressRepository,
		OrderStatusRepositoryInterface $orderStatusRepository
    ) {
        $this->customerRepo = $customerRepository;
        $this->courierRepo = $courierRepository;
		$this->orderRepo = $orderRepository;
		$this->cartRepo = $cartRepository;
		$this->addressRepo = $addressRepository;
		$this->orderStatusRepo = $orderStatusRepository;
    }

    public function index()
    {
        $customer = $this->customerRepo->findCustomerById(auth()->user()->id);

        $customerRepo = new CustomerRepository($customer);
        $orders = $customerRepo->findOrders(['*'], 'created_at');

        $orders->transform(function (Order $order) {
            return $this->transformOrder($order);
        });
		$items = array();
		if(!$orders->isEmpty()){
			foreach($orders as $key=>$order){
				$products = DB::table('order_product')->select('*','order_product.quantity as qty')->where('order_id', '=', $order['id'])->join('products', 'products.id', '=', 'order_product.product_id')->get();
				$items[$order['id']] = $products;
			}
		}		
        $addresses = $customerRepo->findAddresses();
		$Wishlist = new Wishlist();
		$list =  $Wishlist->getWishlistByCustomer();
        return view('front.accounts', [
            'customer' => $customer,
            'orders' => $this->customerRepo->paginateArrayResults($orders->toArray(), 15),
            'addresses' => $addresses,
			'items'=>$items,
			'wishlist'=>$list
        ]);
    }
	
	public function update(UpdateCustomerRequestFront $request)
    {
		
		if($request->has('type')){
			if($request->input('type') == 'details'){
				$customer = $this->customerRepo->findCustomerById(auth()->user()->id);
				$update = new CustomerRepository($customer);
				$data = $request->except('_method', '_token','type');
				$update->updateCustomer($data);
				return redirect()->route('accounts',['tab' => 'profile'])->with('message', 'Account details updated successfuly !.');
			}
			if($request->input('type') == 'password'){
				
				if (!(Hash::check($request->input('current-password'), auth()->user()->password))) {
					// The passwords matches
					return redirect()->route('accounts',['tab' => 'profile'])->with("error","Your current password does not matches with the password you provided. Please try again.");
				}
				if(strcmp($request->input('current-password'), $request->input('password')) == 0){
					//Current password and new password are same
					return redirect()->route('accounts',['tab' => 'profile'])->with("error","New Password cannot be same as your current password. Please choose a different password.");
				}
				if(!strcmp($request->input('password'), $request->input('c-password')) == 0){
					//Current password and new password are same
					return redirect()->route('accounts',['tab' => 'profile'])->with("error","Your password and confirmation password do not match !.");
				}
				
				$customer = $this->customerRepo->findCustomerById(auth()->user()->id);

				$update = new CustomerRepository($customer);
				$data = $request->except('_method', '_token','type');
				if ($request->has('password')) {
					$data['password'] = bcrypt($request->input('password'));
					$data['login_token'] = Password::getRepository()->createNewToken();
				}
				$update->updateCustomer($data);
				return redirect()->route('accounts',['tab' => 'profile'])->with('message', 'Password updated successfuly !.');
			}
		}
    }
	public function wishlist(Request $request){
		$Wishlist = new Wishlist();
		$result = false;
		if($request->input('action') == 'add'){
			$data = $request->except('_method', '_token','action');
			$result = $Wishlist->AddToWishlist($data);
		}
		if($request->input('action') == 'delete'){
			$result = $Wishlist->deleteWishlist($request->input('product_id'));
		}
		$res = array();
		if($result){
			$res['res_code'] = 1;
		}else{
			$res['res_code'] = 0;
		}
		return response()->json($res);
	}
	/**
     * Generate order invoice
     *
     * @param int $id
     * @return mixed
    */
    public function generateInvoice(int $id)
    {
		
		$order = $this->orderRepo->findOrderById($id);
		$courier = $this->courierRepo->findCourierById($order->courier_id);
		$shippingFee = $this->cartRepo->getShippingFee($courier);
		if($order->customer_id == auth()->user()->id){
			$data = [
				'order' => $order,
				'products' => $order->products,
				'customer' => $order->customer,
				'courier' => $order->courier,
				'address' => $this->transformAddress($order->address),
				'shipping_address' => $this->transformAddress($order->shippingAddress),
				'status' => $order->orderStatus,
				'payment' => $order->paymentMethod,
				'shippingfee' => $shippingFee
			];

			$pdf = app()->make('dompdf.wrapper');
			$pdf->loadView('invoices.orders', $data)->stream();
			return $pdf->download($order->reference.'.pdf');;
		}else{
			abort(404);
		}
    }
	public function cancelOrder(int $id)
    {
		$order = $this->orderRepo->findOrderById($id);
		if($order->customer_id == auth()->user()->id){
			$orderRepo = new OrderRepository($order);
			$orderData['order_status_id'] = 3;
			$orderRepo->updateOrder($orderData);
			
			$order->courier = $this->courierRepo->findCourierById($order->courier_id);
			$order->address = $this->addressRepo->findAddressById($order->address_id);

			$orderRepo = new OrderRepository($order);

			$items = $orderRepo->listOrderedProducts();
			$shippingFee = 'Free';
			if($order->courier->is_free == 0){
				$shippingFee = $order->courier->cost;
			}
			$customer = $this->customerRepo->findCustomerById($order->customer_id);
			$customer->order_id = $order->reference;
			Mail::send('emails.order.order_status', [
				'order' => $order,
				'items' => $items,
				'shippingfee'=>$shippingFee,
				'customer' => $customer,
				'currentStatus' => $this->orderStatusRepo->findOrderStatusById($order->order_status_id),
				'payment' => $order->payment,
				'user' => auth()->guard('employee')->user()
			], function ($message) use ($customer) {
				$message
			 ->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'))
				  ->to($customer->email, $customer->name)
				  ->subject('Order cancelled #'.$customer->order_id);
			});
			return redirect()->route('accounts',['tab' => 'orders'])->with('message', "#".$order->reference." Order cancelled!.");
		}else{
			abort(404);
		}
    }
}
