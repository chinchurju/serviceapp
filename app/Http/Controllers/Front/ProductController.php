<?php

namespace App\Http\Controllers\Front;

use App\Shop\Products\Product;
use App\Shop\Products\Repositories\Interfaces\ProductRepositoryInterface;
use App\Http\Controllers\Controller;
use App\Shop\Products\Transformations\ProductTransformable;
use DB;
use App\Shop\Brands\Repositories\BrandRepository;
use App\Shop\Brands\Repositories\BrandRepositoryInterface;
use App\Shop\Categories\Category;
use App\Shop\Categories\Repositories\CategoryRepository;
use App\Shop\Categories\Repositories\Interfaces\CategoryRepositoryInterface;
use App\Shop\Wishlist\Wishlist;

class ProductController extends Controller
{
    use ProductTransformable;

	
    /**
     * @var ProductRepositoryInterface
     */
    private $productRepo;
	/**
     * @var CategoryRepositoryInterface
     */
    private $categoryRepo;
	private $brandRepo;
    /**
     * ProductController constructor.
     * @param ProductRepositoryInterface $productRepository
     */
    public function __construct(ProductRepositoryInterface $productRepository,CategoryRepositoryInterface $categoryRepository,BrandRepositoryInterface $brandRepository)
    {
        $this->productRepo = $productRepository;
		$this->categoryRepo = $categoryRepository;
		$this->brandRepo    = $brandRepository;
    }
	
	public function shop()
    {
		
		$categories_arr =[];
		$brands_arr =[];
		$order_by_fl = 'id';
		$order_by = 'DESC';
		
		if(request()->has("order_by")){
        	if(request()->input("order_by") == "asc"){
        		$order_by_fl = "price";
        		$order_by = 'ASC';
        	}
        	if(request()->input("order_by") == "desc"){
        		$order_by_fl = "price";
        		$order_by = 'DESC';
        	}
        }
		
        if (request()->has('q') && request()->input('q') != '') {
			$list = $this->productRepo->searchProduct(request()->input('q'),$order_by_fl,$order_by);
        } else {
            $list = $this->productRepo->listProducts($order_by_fl,$order_by);
        }
		if(request()->has("categories")){
			$categories_arr = explode(',', request()->input("categories"));
			$list = $list->whereIn('id',$categories_arr);
				
		}
		if(request()->has("brands")){
			$brands_arr = explode(',', request()->input("brands"));
			$list = $list->whereIn('brand_id',$brands_arr);
		}
		
        $products = $list->where('status', 1)->map(function (Product $item) {
            return $this->transformProduct($item);
        });
		
		$res  = $this->productRepo->paginateArrayResults($products->all(), 10);
		$test = array();
		foreach($res as $key=>$row){
			
			//$b = $this->brandRepo->findBrandById($row->brand_id);
			//$res[$key]->brand = $b;
			if((int)$row->brand_id > 0){
				$brand = DB::table('brands')->where('id','=',$row->brand_id)->first();
				//print_r($brand);
				$row->brand_name = $brand->name;
			}
			$test[] = $row;
		}
		$brands = $this->brandRepo->listBrands();
		foreach($brands as $key=>$row){
			$count = Product::where('status', 1)->where('brand_id','=',$row->id)->count(DB::raw('DISTINCT id'));
			//$count =  DB::table('products')->where('brand_id','=',$row->id)->count(DB::raw('DISTINCT id'));
			if($count > 0){
				$brands[$key]->pdr_count = $count;
			}else{
				unset($brands[$key]);
			}
			
		}

		/*if($brands->count() <= 1){
			unset($brands[0]);
		}*/
		$categories = Category::where('parent_id','=',1)->where('status','=',1)->with('children')->get();
		foreach($categories as $key=>$row){
			$count = DB::table('category_product')->where('category_id','=',$row->id)->count(DB::raw('DISTINCT id'));
			if($count > 0){
				$categories[$key]->pdr_count = $count;
			}else{
				unset($categories[$key]);
				//$categories[$key]->pdr_count = 0;
			}
		}
		
		$Wishlist = new Wishlist();
		$list =  $Wishlist->getWishlist();
        return view('front.products.shop', [
            'products' => $test,
			'links'	   => $this->productRepo->paginateArrayResults($products->all(), 10)->links(),
			'mywishlist'=>$list,
			'categories1' =>$categories,
			'brands' =>$brands,
			'brans_arr'=>$brands_arr,
			'categories_arr'=>$categories_arr
        ]);
       
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function search()
    {
        $categories_arr =[];
		$brands_arr =[];
		$order_by_fl = 'id';
		$order_by = 'DESC';
		
		if(request()->has("order_by")){
        	if(request()->input("order_by") == "asc"){
        		$order_by_fl = "price";
        		$order_by = 'ASC';
        	}
        	if(request()->input("order_by") == "desc"){
        		$order_by_fl = "price";
        		$order_by = 'DESC';
        	}
        }

		
        if (request()->has('q') && request()->input('q') != '') {
			$list = $this->productRepo->searchProduct(request()->input('q'),$order_by_fl,$order_by);
        } else {
            $list = $this->productRepo->listProducts($order_by_fl,$order_by);
        }
		if(request()->has("categories")){
			$categories_arr = explode(',', request()->input("categories"));
			$list = $list->whereIn('id',$categories_arr);
				
		}
		if(request()->has("brands")){
			$brands_arr = explode(',', request()->input("brands"));
			$list = $list->whereIn('brand_id',$brands_arr);
		}
		
		
        $products = $list->where('status', 1)->map(function (Product $item) {
            return $this->transformProduct($item);
        });
		
		$res  = $this->productRepo->paginateArrayResults($products->all(), 10);
		$test = array();
		foreach($res as $key=>$row){
			
			//$b = $this->brandRepo->findBrandById($row->brand_id);
			//$res[$key]->brand = $b;
			if((int)$row->brand_id > 0){
				$brand = DB::table('brands')->where('id','=',$row->brand_id)->first();
				//print_r($brand);
				$row->brand_name = $brand->name;
			}
			$test[] = $row;
		}
		$brands = $this->brandRepo->listBrands();
		foreach($brands as $key=>$row){
			$count = Product::where('status', 1)->where('brand_id','=',$row->id)->count(DB::raw('DISTINCT id'));
			//$count =  DB::table('products')->where('brand_id','=',$row->id)->count(DB::raw('DISTINCT id'));
			if($count > 0){
				$brands[$key]->pdr_count = $count;
			}else{
				unset($brands[$key]);
			}
			
		}

		/*if($brands->count() <= 1){
			unset($brands[0]);
		}*/
		
		$categories = Category::where('parent_id','=',1)->where('status','=',1)->with('children')->get();
		foreach($categories as $key=>$row){
			$count = DB::table('category_product')->where('category_id','=',$row->id)->count(DB::raw('DISTINCT id'));
			if($count > 0){
				$categories[$key]->pdr_count = $count;
			}else{
				unset($categories[$key]);
				//$categories[$key]->pdr_count = 0;
			}
		}
		
		$Wishlist = new Wishlist();
		$list =  $Wishlist->getWishlist();
        return view('front.products.product-search', [
            'products' => $test,
			'links'	   => $this->productRepo->paginateArrayResults($products->all(), 10)->links(),
			'mywishlist'=>$list,
			'categories1' =>$categories,
			'brands' =>$brands,
			'brans_arr'=>$brands_arr,
			'categories_arr'=>$categories_arr
        ]);
    }

    /**
     * Get the product
     *
     * @param string $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(string $slug)
    {
        $product = $this->productRepo->findProductBySlug(['slug' => $slug]);
        $images = $product->images()->get();
        $category = $product->categories()->first();
        $productAttributes = $product->attributes;
		if((int)$product->brand_id > 0){
				$brand = DB::table('brands')->where('id','=',$product->brand_id)->first();
				//print_r($brand);
				$product->brand_name = $brand->name;
		}
		$Wishlist = new Wishlist();
		$mywishlist = $Wishlist->getWishlist();
		$products = DB::table('related_products')->join('products', 'products.id', '=', 'related_products.related_product_id')->where('product_id',$product->id)->get();
		//print_r($product);exit;
        return view('front.products.product', compact(
            'product',
            'images',
            'productAttributes',
            'category',
            'combos',
			'products',
			'mywishlist'
        ));
    }
}
