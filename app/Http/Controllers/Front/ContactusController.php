<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Shop\Subscribers\Subscribers;
//validator is builtin class in laravel
use Validator;
use App;
use Lang;
use DB;
//for password encryption or hash protected
use Hash;
use App\Administrator;

//for authenitcate login data
use Auth;
use Mail;

//use Illuminate\Foundation\Auth\ThrottlesLogins;
//use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

//for requesting a value 

class ContactusController extends Controller
{
	public function index(){
		
		return view('front.contactus', []);
	}
	public function send(Request $request){
		
		$data = $request->all();
		Mail::send('emails.contactus', [
            'contact' => $data,
        ], function ($message){
			$message
		 ->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'))
			  ->to('rohith@whytecreations.in', "Rohith")
			  ->subject('Contact From ');
		});
		
		
		return redirect()->route('contactus.index')
            ->with('message', 'Thank you for contacting us.');
		//Unable to submit your request. Please, try again later
		//echo "sssssssssssssssssssssssss";exit;
		//return view('front.page', []);
	}
}
