<?php

namespace App\Http\Controllers\Front;

use App\Shop\Categories\Repositories\CategoryRepository;
use App\Shop\Categories\Repositories\Interfaces\CategoryRepositoryInterface;
use App\Http\Controllers\Controller;
use App\Shop\Brands\Repositories\BrandRepository;
use App\Shop\Brands\Repositories\BrandRepositoryInterface;
use DB;
use App\Shop\Wishlist\Wishlist;
use App\Shop\Categories\Category;
class CategoryController extends Controller
{
    /**
     * @var CategoryRepositoryInterface
     */
    private $categoryRepo;
	private $brandRepo;

    /**
     * CategoryController constructor.
     *
     * @param CategoryRepositoryInterface $categoryRepository
     */
    public function __construct(CategoryRepositoryInterface $categoryRepository,BrandRepositoryInterface $brandRepository)
    {
        $this->categoryRepo = $categoryRepository;
		$this->brandRepo    = $brandRepository;
    }

    /**
     * Find the category via the slug
     *
     * @param string $slug
     * @return \App\Shop\Categories\Category
     */
    public function getCategory(string $slug)
    {
		$categories_arr =[];
		$brands_arr =[];
		$order_by_fl = 'id';
		$order_by = 'DESC';
		
        $category = $this->categoryRepo->findCategoryBySlug(['slug' => $slug]);

        $repo = new CategoryRepository($category);
        if(request()->has("order_by")){
        	if(request()->input("order_by") == "asc"){
        		$order_by_fl = "price";
        		$order_by = 'ASC';
        	}
        	if(request()->input("order_by") == "desc"){
        		$order_by_fl = "price";
        		$order_by = 'DESC';
        	}
        }

		if($order_by == "ASC"){
        	$products =  $repo->findProducts()->where('status', 1)->sortBy($order_by_fl)->all();
    	}else{
    		$products =  $repo->findProducts()->where('status', 1)->sortByDesc($order_by_fl)->all();
    	}

        if(request()->has("categories") && request()->has("brands")){
        	$categories_arr = explode(',', request()->input("categories"));
			$brands_arr = explode(',', request()->input("brands"));
			if($order_by == "ASC"){
				$products =  $repo->findProducts()->where('status', 1)->whereIn('brand_id', $brands_arr)->whereIn('id',$categories_arr)->sortBy($order_by_fl)->all();
			}
			else{
				$products =  $repo->findProducts()->where('status', 1)->whereIn('brand_id', $brands_arr)->whereIn('id',$categories_arr)->sortByDesc($order_by_fl)->all();
			}
        }
        else{
	        if(request()->has("categories")){
				$categories_arr = explode(',', request()->input("categories"));
				if($order_by == "ASC"){
					$products =  $repo->findProducts()->where('status', 1)->whereIn('id',$categories_arr)->sortBy($order_by_fl)->all();
				}else{
					$products =  $repo->findProducts()->where('status', 1)->whereIn('id',$categories_arr)->sortByDesc($order_by_fl)->all();
				}
			}
			if(request()->has("brands")){
				$brands_arr = explode(',', request()->input("brands"));
				$products =  $repo->findProducts()->where('status', 1)->whereIn('brand_id', $brands_arr)->sortByDesc($order_by_fl)->all();
			}
		}

		$res  = $repo->paginateArrayResults($products, 5);
		$test = array();
		foreach($res as $key=>$row){
			
			//$b = $this->brandRepo->findBrandById($row->brand_id);
			//$res[$key]->brand = $b;
			if((int)$row->brand_id > 0){
				$brand = DB::table('brands')->where('id','=',$row->brand_id)->first();
				//print_r($brand);
				$row->brand_name = $brand->name;
			}
			$test[] = $row;
		}
		$brands = $this->brandRepo->listBrands();
		foreach($brands as $key=>$row){
			$count = $repo->findProducts()->where('status', 1)->where('brand_id','=',$row->id)->count(DB::raw('DISTINCT id'));
			//$count =  DB::table('products')->where('brand_id','=',$row->id)->count(DB::raw('DISTINCT id'));
			if($count > 0){
				$brands[$key]->pdr_count = $count;
			}else{
				unset($brands[$key]);
			}
			
		}

		/*if($brands->count() <= 1){
			unset($brands[0]);
		}*/
		
		$categories = Category::where('id','=',$category->id)->where('status','=',1)->with('children')->get();
		foreach($categories as $key=>$row){
			$count = DB::table('category_product')->where('category_id','=',$row->id)->count(DB::raw('DISTINCT id'));
			if($count > 0){
				$categories[$key]->pdr_count = $count;
			}else{
				unset($categories[$key]);
				//$categories[$key]->pdr_count = 0;
			}
		}
		
		$Wishlist = new Wishlist();
		$list =  $Wishlist->getWishlist();
        return view('front.categories.category', [
            'category' => $category,
            'products' => $test,
			'links'	   => $res->links(),
			'mywishlist'=>$list,
			'categories1' =>$categories,
			'brands' =>$brands,
			'brans_arr'=>$brands_arr,
			'categories_arr'=>$categories_arr
        ]);
    }
}
