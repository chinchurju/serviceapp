<?php

namespace App\Http\Controllers\Front;

use App\Shop\Products\Product;
use App\Shop\Products\Repositories\Interfaces\ProductRepositoryInterface;
use App\Http\Controllers\Controller;
use App\Shop\Products\Transformations\ProductTransformable;
use App\Shop\Brands\Repositories\BrandRepository;
use App\Shop\Brands\Repositories\BrandRepositoryInterface;
use App\Shop\Categories\Repositories\CategoryRepository;
use App\Shop\Categories\Repositories\Interfaces\CategoryRepositoryInterface;
use DB;
class ShopController extends Controller
{
    use ProductTransformable;

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepo;

    /**
     * ProductController constructor.
     * @param ProductRepositoryInterface $productRepository
     */
    public function __construct(
		ProductRepositoryInterface $productRepository,
		BrandRepositoryInterface $brandRepository,
		CategoryRepositoryInterface $categoryRepository)
    {
        $this->productRepo = $productRepository;
		$this->brandRepo    = $brandRepository;
		$this->categoryRepo = $categoryRepository;
    }
	public function index()
    {
		
		
        if (request()->has('q') && request()->input('q') != '') {
            $list = $this->productRepo->searchProduct(request()->input('q'));
        } else {
            $list = $this->productRepo->listProducts();
        }
		$recommends_list  = $this->productRepo->listProducts('id','ASC');
		
		$recommends = $recommends_list->where('status', 1)->map(function (Product $item) {
            return $this->transformProduct($item);
        });
		
        $products = $list->where('status', 1)->map(function (Product $item) {
            return $this->transformProduct($item);
        });
		
		$brands = $this->brandRepo->listBrands();
		foreach($brands as $key=>$row){
			$count = DB::table('products')->where('brand_id','=',$row->id)->count(DB::raw('DISTINCT id'));
			if($count > 0){
				$brands[$key]->pdr_count = $count;
			}else{
				unset($brands[$key]);
			}
			
		}
		$categories = DB::table('categories')->where('status','=',1)->where('parent_id','>',1)->get();
		foreach($categories as $key=>$row){
			$count = DB::table('category_product')->where('category_id','=',$row->id)->count(DB::raw('DISTINCT id'));
			if($count > 0){
				$categories[$key]->pdr_count = $count;
			}else{
				unset($categories[$key]);
				//$categories[$key]->pdr_count = 0;
			}
			
		}
		//print_r($categories);exit;
        return view('front.products.shop', [
            'products' => $this->productRepo->paginateArrayResults($products->all(), 10),
			'links'	   => $this->productRepo->paginateArrayResults($products->all(), 10)->links(),
			'brands'   => $brands,
			'categories1' => $categories,
			'recommends' => $this->productRepo->paginateArrayResults($recommends->all(), 3)
        ]);
    }
	

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function search()
    {
        if (request()->has('q') && request()->input('q') != '') {
            $list = $this->productRepo->searchProduct(request()->input('q'));
        } else {
            $list = $this->productRepo->listProducts();
        }

        $products = $list->where('status', 1)->map(function (Product $item) {
            return $this->transformProduct($item);
        });

        return view('front.products.product-search', [
            'products' => $this->productRepo->paginateArrayResults($products->all(), 10)
        ]);
    }

    /**
     * Get the product
     *
     * @param string $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(string $slug)
    {
        $product = $this->productRepo->findProductBySlug(['slug' => $slug]);
        $images = $product->images()->get();
        $category = $product->categories()->first();
        $productAttributes = $product->attributes;
		if((int)$product->brand_id > 0){
				$brand = DB::table('brands')->where('id','=',$product->brand_id)->first();
				//print_r($brand);
				$product->brand_name = $brand->name;
		}
		//print_r($product);exit;
        return view('front.products.product', compact(
            'product',
            'images',
            'productAttributes',
            'category',
            'combos'
        ));
    }
}
