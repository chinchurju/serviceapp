<?php

namespace App\Http\Controllers\Api;

use App\Shop\Products\Product;
use App\Shop\Countries\Country;
use App\Shop\Products\Repositories\Interfaces\ProductRepositoryInterface;
use App\Shop\Products\Transformations\ProductTransformable;
use App\Shop\Countries\Repositories\CountryRepository;
use App\Shop\Countries\Repositories\Interfaces\CountryRepositoryInterface;
use App\Http\Controllers\Controller;
use DB;
class CountryController extends Controller
{
    /**
     * @var CountryRepositoryInterface
     */
    private $countryRepo;

    /**
     * CountryController constructor.
     *
     * @param CountryRepositoryInterface $countryRepository
     */
    public function __construct(
		CountryRepositoryInterface $countryRepository,
		ProductRepositoryInterface $productRepository
	)
    {
        $this->countryRepo = $countryRepository;
		$this->productRepo = $productRepository;
    }
	
   
	public function index()
	{
		$countries = Country::where('status','=','1')->get();
		return response()->json(["status" => "200", 'success'=>1, 'msg'=>"successfull", "countries" =>$countries]);

	}

    /**
     * Find the country 
     *
     * @param string $id
     * @return \App\Shop\Countries\Country
     */
    public function getProducts($id)
    {
        $countries_arr =[];
        $order_by_fl = 'id';
        $order_by = 'DESC';
        $country = $this->countryRepo->findCountryById($id);
        $repo = new CountryRepository($country);
        if(request()->has("order_by")){
            if(request()->input("order_by") == "asc"){
                $order_by_fl = "price";
                $order_by = 'ASC';
            }
            if(request()->input("order_by") == "desc"){
                $order_by_fl = "price";
                $order_by = 'DESC';
            }
        }
        if($order_by == "ASC"){
            $products =  $repo->findProducts()->where('status', 1)->all();
        }else{
            $products =  $repo->findProducts()->where('status', 1)->all();
        }
        $countries = Country::where('id','=',$country->id)->where('status','=',1)->get();
        foreach($countries as $key=>$row){
            $count = DB::table('country_product')->where('country_id','=',$row->id)->count(DB::raw('DISTINCT id'));
            
        }
        
    }


    }
