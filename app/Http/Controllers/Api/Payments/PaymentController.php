<?php

namespace App\Http\Controllers\Api\Payments;

use App\Http\Controllers\Controller;
use App\Shop\Carts\Repositories\Interfaces\CartRepositoryInterface;
use App\Shop\Checkout\CheckoutRepository;

use App\Shop\Addresses\Repositories\Interfaces\AddressRepositoryInterface;
use App\Shop\Addresses\Transformations\AddressTransformable;
use App\Shop\Couriers\Courier;
use App\Shop\Couriers\Repositories\CourierRepository;
use App\Shop\Couriers\Repositories\Interfaces\CourierRepositoryInterface;
use App\Shop\Customers\Customer;
use App\Shop\Customers\Repositories\CustomerRepository;
use App\Shop\Customers\Repositories\Interfaces\CustomerRepositoryInterface;
use App\Shop\Orders\Order;
use App\Shop\Orders\Repositories\Interfaces\OrderRepositoryInterface;
use App\Shop\Orders\Repositories\OrderRepository;
use App\Shop\OrderStatuses\OrderStatus;
use App\Shop\OrderStatuses\Repositories\Interfaces\OrderStatusRepositoryInterface;
use App\Shop\OrderStatuses\Repositories\OrderStatusRepository;


use App\Shop\Carts\Cart;
use App\Shop\Shipping\ShippingInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Ramsey\Uuid\Uuid;
use Shippo_Shipment;
use Shippo_Transaction;
use DB;
use Mail;
class PaymentController extends Controller
{
    /**
     * @var CartRepositoryInterface
     */
    private $cartRepo;

    /**
     * @var int $shipping
     */
    private $shippingFee;

    private $rateObjectId;

    private $shipmentObjId;

    private $billingAddress;

    private $carrier;

    /**
     * BankTransferController constructor.
     *
     * @param Request $request
     * @param CartRepositoryInterface $cartRepository
     * @param ShippingInterface $shippingRepo
     */
    public function __construct(
        Request $request,
        CartRepositoryInterface $cartRepository,
        ShippingInterface $shippingRepo,
		OrderRepositoryInterface $orderRepository,
        CourierRepositoryInterface $courierRepository,
        AddressRepositoryInterface $addressRepository,
        CustomerRepositoryInterface $customerRepository,
        OrderStatusRepositoryInterface $orderStatusRepository
    )
    {
		$this->cartRepo = $cartRepository;
		$this->orderRepo = $orderRepository;
        $this->courierRepo = $courierRepository;
        $this->addressRepo = $addressRepository;
        $this->customerRepo = $customerRepository;
        $this->orderStatusRepo = $orderStatusRepository;
        $fee = 0;
        $rateObjId = null;
        $shipmentObjId = null;
        $billingAddress = $request->input('billing_address');

        if ($request->has('rate')) {
            if ($request->input('rate') != '') {

                $rate_id = $request->input('rate');
                $rates = $shippingRepo->getRates($request->input('shipment_obj_id'));
                $rate = collect($rates->results)->filter(function ($rate) use ($rate_id) {
                    return $rate->object_id == $rate_id;
                })->first();

                $fee = $rate->amount;
                $rateObjId = $rate->object_id;
                $shipmentObjId = $request->input('shipment_obj_id');
                $this->carrier = $rate;
            }
        }

        $this->shippingFee = $fee;
        $this->rateObjectId = $rateObjId;
        $this->shipmentObjId = $shipmentObjId;
        $this->billingAddress = $billingAddress;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
		
        return view('front.cash-on-delivery-redirect', [
            'subtotal' => $this->cartRepo->getSubTotal(),
            'shipping' => $this->shippingFee,
            'tax' => $this->cartRepo->getTax(),
            'total' => $this->cartRepo->getTotal(2, $this->shippingFee),
            'rateObjectId' => $this->rateObjectId,
            'shipmentObjId' => $this->shipmentObjId,
            'billingAddress' => $this->billingAddress
			
        ]);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function store(Request $request)
    {
           $this->validate($request, [
         //  'file_source' => 'mimes:jpg,png,mpeg,ogg,mp4,webm,3gp,mov,flv,avi,wmv,doc,docx,pdf,odt|max:20048',
          // 'servicedate' => 'required',
          // 'servicetime' => 'required',
           'country_id' => 'required'
          // 'msg' => 'required'
       ]);
           //$img = ['jpg','png'];
         //  $video = ['mpeg','ogg','mp4','webm','3gp','mov','flv','avi','wmv'];
          // $doc = ['doc','docx','pdf','odt'];

		$cart = new Cart();
        $checkoutRepo = new CheckoutRepository;
        $orderStatusRepo = new OrderStatusRepository(new OrderStatus);
        $os = $orderStatusRepo->findByName('Pending');
	/*	$courier = $this->courierRepo->findCourierById($request->input('shipping_method_id'));
		$this->shippingFee = 0;
		if($courier->is_free == 0){
			$this->shippingFee = $courier->cost;
		}*/
        $order = $checkoutRepo->buildCheckoutItems([
            'reference' => Uuid::uuid4()->toString(),
            //'courier_id' => $request->input('shipping_method_id') ,
             // 'courier' => $courier->name ,
            'customer_id' => $request->user()->id,
          // 'address_id' => $request->input('billing_address'), 
			//'shipping_address_id' => $request->input('shipping_address'),
            'order_status_id' => $os->id,
            'payment' => strtolower('cash after service'),
           // 'discounts' => 0,
			//'shipping_charge'=>$this->shippingFee,
            'total_products' => $cart->getCartTotal(),
            'total' => $cart->getCartTotal(),
            'sale_price' => $request->input('sale_price'),
           // 'total_paid' => 0,
            //'tax' => 0,
            'servicedate' => $request->input('servicedate') ,
            'servicetime' => $request->input('servicetime'),  
            'flagemergency' => $request->input('flagemergency'), 
            'country_id'  => $request->input('country_id'),
           // 'description' => $request->input('description')
        ],'api');


            //    $order = $this->orderRepo->findOrderById($order->id);
               // $input=$request->all();
              //  $images=array();
              //  if($request->file('images'))
              //  {
                   // foreach($request->file('images') as $file){
                    //    $ext=$file->getClientOriginalExtension();
                    //    if (in_array($ext, $img)){
                     //       $type = 'image';
                     //   }
                      //  elseif(in_array($ext, $video)){
                     //       $type = 'video';
                     //   }
                      //  else{
                      //      $type = 'document';
                      //  }
                    //  echo $name=$file->getClientOriginalName();

                       // $name = $file->store('orderAttachment');
                       // $file->move('images',$name);
                      //  $images[]=['order_id' =>  $order->id,
                     //  'images' =>  $name, 'type' => $type];
                   // }
                   // exit;
             //   }

               // print_r($images);exit;
           //  if($images){
                 //DB::table('request_attachment')->insert($images);
            // }
              Cart::clear();
             return response()->json(["status" => "200", "success" =>1,'mag'=>'Updated successfull !.','order_id'=>$order->id ]);
           // return 'Upload successful!';                        
       

       
		$order = $this->orderRepo->findOrderById($order->id);
        $order->courier = $this->courierRepo->findCourierById($order->courier_id);
        $order->address = $this->addressRepo->findAddressById($order->address_id);

        $orderRepo = new OrderRepository($order);

        $items = $orderRepo->listOrderedProducts();
		$this->shippingFee = 'Free';
		if($order->courier->is_free == 0){
			$this->shippingFee = $order->courier->cost;
		}
		

        return redirect()->route('accounts', ['tab' => 'orders'])->with('message', 'Request successful!');
    }

   public function storeupdate(Request $request, $id)
    {
        $order = $this->orderRepo->findOrderById($id);
        $order->location_latitude =  $request->input('location_latitude');
        $order->location_longitude =  $request->input('location_longitude');
        $order->address =  $request->input('address');
        $order->save();
        return response()->json(["status" => "200", "success" =>1,'mag'=>'Updated successfull !.','order_id'=>$id ]);
    }

    public function storeattachment(Request $request, $id)
    {
        
        $order = $this->orderRepo->findOrderById($id);
        $ord_att = DB::table('request_attachment')->where('order_id', $order->id)->get();
        if($ord_att->count() <=0 ){
            $this->validate($request, [
           // 'file_source' => 'mimes:jpg,png,mpeg,ogg,mp4,webm,3gp,mov,flv,avi,wmv,doc,docx,pdf,odt|max:20048',
           ]);
        
            $img = ['jpg','png','jpeg'];
            $video = ['mpeg','ogg','mp4','webm','3gp','mov','flv','avi','wmv'];
            $doc = ['doc','docx','pdf','odt'];

            $order->summary =  $request->input('summary');
            $order->description =  $request->input('description');
            $order->save();
            $input=$request->all();
            $images=array();
            if($request->file('images'))
            {
                foreach($request->file('images') as $file){
                    $ext=$file->getClientOriginalExtension();
                    if (in_array(strtolower($ext), $img)){
                        $type = 'image';
                    }
                    elseif(in_array(strtolower($ext), $video)){
                        $type = 'video';
                    }
                    else{
                        $type = 'document';
                    }
                    //echo $name=$file->getClientOriginalName();

                    $name = $file->store('orderAttachment');
                    
                    $extension = explode(".", $name);
                    if($extension[count($extension)-1] == "qt"){
                        rename(storage_path('app')."/".$name,storage_path('app')."/".$name.".mp4");
                        $name .= ".mp4";
                        
                    }
                    //echo $name;
                   // $file->move('images',$name);
                    $images[]=['order_id' =>  $order->id,'images' => $name, 'type' => $type];
                }
               // exit;
            }
             if($images){
                DB::table('request_attachment')->insert($images);
                             
              $orderRepo = new OrderRepository($order);
             $items = $orderRepo->listOrderedProducts();
                    $customer = $this->customerRepo->findCustomerById($order->customer_id);
                    $customer->order_id = $order->reference;
                    $img_location = 'https://maps.googleapis.com/maps/api/staticmap?&zoom=13&size=600x300&&markers=color:blue%7Clabel:S%7C'.$order->location_latitude.','.$order->location_longitude.'&key=AIzaSyApCG7RY-VZ1ZYVn7OJzrNQIGivnnZLB-U';
                    Mail::send('emails.order.confirm', [
                        'order' => $order,
                        'items' => $items,
                         'location_url' => 'https://maps.google.com/maps?q='.$order->location_latitude.','.$order->location_longitude.'&hl',
                         'img_location' => $img_location,
                        'customer' => $customer,
                        'currentStatus' => $this->orderStatusRepo->findOrderStatusById($order->order_status_id),
                        'payment' => $order->payment,
                        'user' => auth()->guard('employee')->user()
                    ], function ($message) use ($customer) {
                        $message
                         ->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'))
                          ->to($customer->email, $customer->name)
                          ->subject('Request Confirmation #'.$customer->order_id.' - Your Request with SERVICIO has been successfull!');
                    });
                         }
            }
            
            
             return response()->json(["status" => "200", "success" =>1,'mag'=>'Updated successfull !.' ]);
             
    }


}