<?php

namespace App\Http\Controllers\Api;

use App\Shop\Couriers\Repositories\Interfaces\CourierRepositoryInterface;
use App\Shop\Customers\Repositories\CustomerRepository;
use App\Shop\Customers\Repositories\Interfaces\CustomerRepositoryInterface;
use App\Http\Controllers\Controller;
use App\Shop\Orders\Order;
use App\Shop\Orders\Transformers\OrderTransformable;
use App\Shop\Wishlist\Wishlist;
use Illuminate\Http\Request;
use App\Shop\Addresses\Repositories\Interfaces\AddressRepositoryInterface;
use App\Shop\Addresses\Transformations\AddressTransformable;
use App\Shop\Customers\Requests\UpdateCustomerRequestApi;
use App\Shop\Orders\Repositories\Interfaces\OrderRepositoryInterface;
use App\Shop\Orders\Repositories\OrderRepository;
use App\Shop\OrderStatuses\OrderStatus;
use App\Shop\OrderStatuses\Repositories\Interfaces\OrderStatusRepositoryInterface;
use App\Shop\OrderStatuses\Repositories\OrderStatusRepository;
use App\Shop\Customers\Customer; 
use Hash;
use DB;
use Mail;
use App\Shop\Carts\Repositories\Interfaces\CartRepositoryInterface;
use Illuminate\Support\Facades\Password;
use App\Shop\Addresses\Address;



class AccountsController extends Controller
{
    use OrderTransformable;
    use AddressTransformable;

    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepo;

    /**
     * @var CourierRepositoryInterface
     */
    private $courierRepo;

    /**
     * AccountsController constructor.
     *
     * @param CourierRepositoryInterface $courierRepository
     * @param CustomerRepositoryInterface $customerRepository
     */
    public function __construct(
    	OrderRepositoryInterface $orderRepository,
        CourierRepositoryInterface $courierRepository,
        CustomerRepositoryInterface $customerRepository,
        CartRepositoryInterface $cartRepository,
		AddressRepositoryInterface $addressRepository,
		OrderStatusRepositoryInterface $orderStatusRepository
    ) {
        $this->customerRepo = $customerRepository;
        $this->courierRepo = $courierRepository;
        $this->orderRepo = $orderRepository;
		$this->addressRepo = $addressRepository;
		$this->orderStatusRepo = $orderStatusRepository;
    }

    public function index()
    {
        $customer = $this->customerRepo->findCustomerById(auth()->user()->id);

        $customerRepo = new CustomerRepository($customer);
        $orders = $customerRepo->findOrders(['*'], 'created_at');

        $orders->transform(function (Order $order) {
            return $this->transformOrder($order);
        });

        $addresses = $customerRepo->findAddresses();

        return response()->json(["status" => "200",
            'customer' => $customer,
            'orders' => $this->customerRepo->paginateArrayResults($orders->toArray(), 15),
            'addresses' => $addresses
        ]);
    } 
	public function update(UpdateCustomerRequestApi $request)
    {
		if($request->has('type')){
			if($request->input('type') == 'details'){
				$customer = $this->customerRepo->findCustomerById(auth()->user()->id);
				$update = new CustomerRepository($customer);
				$data = $request->except('_method', '_token','type','profile');
                if ($request->hasFile('profile')) {
					$data['profile_pic'] = $request->file('profile')->store('profile');
				
				}
				Customer::where('id',auth()->user()->id)->update($data);
				//$update->updateCustomer($data);
				$customer = $this->customerRepo->findCustomerById(auth()->user()->id);
			return response()->json(["status" => "200", "success" =>1,'msg'=>'Account details updated successfuly !.','profile-img'=>env("APP_URL")."/storage/app/".$customer->profile_pic]);
			}
			if($request->input('type') == 'password'){
				
				if (!(Hash::check($request->input('current-password'), auth()->user()->password))) {
					// The passwords matches
					return response()->json(["status" => "200", "success" =>0,'msg'=>'Your current password does not matches with the password you provided. Please try again.' ]);
				}
				if(strcmp($request->input('current-password'), $request->input('password')) == 0){
					//Current password and new password are same
					return response()->json(["status" => "200", "success" =>0,'msg'=>"New Password cannot be same as your current password. Please choose a different password." ]);
				}
				
				
				$customer = $this->customerRepo->findCustomerById(auth()->user()->id);

				$update = new CustomerRepository($customer);
				$data = $request->except('_method', '_token','type');
				if ($request->has('password')) {
					$data['password'] = bcrypt($request->input('password'));
					$data['login_token'] = Password::getRepository()->createNewToken();
				}
				$update->updateCustomer($data);
				return response()->json(["status" => "200", "success" =>1,'msg'=>'Password updated successfuly !.','login_token'=> $data['login_token']]);
			}
		}
    }
	public function wishlistAdd(Request $request){
		$Wishlist = new Wishlist();
		$result = false;
		if($request->input('action') == 'add'){
			$data = $request->except('_method', '_token','action');
			$result = $Wishlist->AddToWishlist($data);
		}
		if($request->input('action') == 'delete'){
			$result = $Wishlist->deleteWishlist($request->input('product_id'));
		}
		$res_code = 0;
		if($result){
			$res_code = 1;
		}
		return response()->json(["status" => "200", "success" =>$res_code,'msg'=>'success' ]);
	}
	public function wishlist()
    {
		$Wishlist = new Wishlist();
		$list =  $Wishlist->getWishlistByCustomer();
		foreach($list as $key=>$row){
			$list[$key]->cover = env("APP_URL")."/storage/products/".$row->cover;
		}
		return response()->json(["status" => "200", "wishlist" =>$list]);
	}
	public function my_orders()
    {
		$customer = $this->customerRepo->findCustomerById(auth()->user()->id);
        $customerRepo = new CustomerRepository($customer);
        $orders = $customerRepo->findOrders(['*'], 'created_at');
		$orders->transform(function (Order $order) {
             $ord =  $this->transformOrder($order);
             $ord->customer->profile_pic = env("APP_URL")."/storage/app/".$ord->customer->profile_pic;
             return $ord;
        });
        $addresses = $customerRepo->findAddresses();
        $orders = $this->customerRepo->paginateArrayResults($orders->toArray(), 15)->toArray();
        $orders['data']=array_values($orders['data']);
		return response()->json(["status" => "200", "orders" =>$orders]);
	}
	public function notification()
    {

    	$customer = $this->customerRepo->findCustomerById(auth()->user()->id);
		$notification = DB::table('notification')
		->where('customer_id',auth()->user()->id)
		->orderBy('id', 'DESC')->paginate(150);
		return response()->json(["status" => "200", "success" =>1, "notification" =>$notification]);
    }
	public function orders_details($id)
    {
	    $order = $this->orderRepo->findOrderById($id);
	//	$courier = $this->courierRepo->findCourierById($order->courier_id);
		if($order->customer_id == auth()->user()->id){
			$products = $order->products;
			foreach($products as $key=>$row){
				$products[$key]->cover = env("APP_URL")."/storage/products/".rawurlencode($row->cover);
			}
			
    		return response()->json([
    		        "status" => "200",
    		        'order' => $order,
    				'products' => $order->products,
    				'customer' => $order->customer,
    				//'courier' => $order->courier,
    			//	'address' => $this->transformAddress($order->address),
    				'status' => $order->orderStatus,
    				'payment' => $order->paymentMethod,
    		]);
		}else{
			abort(404);
		}
       
       
           
        //print_r($res);exit;   
		
	}
	public function my_address()
    {
		$customer = $this->customerRepo->findCustomerById(auth()->user()->id);
        $customerRepo = new CustomerRepository($customer);
        $orders = $customerRepo->findOrders(['*'], 'created_at');
        $orders->transform(function (Order $order) {
            return $this->transformOrder($order);
        });
        $addresses = $customerRepo->findAddresses();
		return response()->json(["status" => "200", "address" => $addresses]);
	}
	public function my_details()
    {
		$customer = $this->customerRepo->findCustomerById(auth()->user()->id);
		$customer->profile_pic = env("APP_URL")."/storage/app/".$customer->profile_pic;
		return response()->json(["status" => "200", "success" =>1, "customer" => $customer]);
	}
	
	public function setDefault($addressId)
    {
        $address = $this->addressRepo->findAddressById($addressId);
		if($address->customer_id == auth()->user()->id){
			Address::where('customer_id', $address->customer_id)->update(['is_default' => 0]);	
			$address->is_default = 1;
			$address->save();
		}
        return response()->json(["status" => "200",'msg'=>'success']);
    }
	function registerDevices(Request $request) {
		
		$device_type = $request->device_type;
		$type = 3;
		if($device_type=='iOS'){ 	/* iphone */
			$type = 1;
		}elseif($device_type=='Android'){	/* android */
			$type = 2;
		}elseif($device_type=='Desktop'){ 	/* other */
			$type = 3;
		}	
		
			
		$device_data = array(
			'device_id' => $request->device_id,
			'customers_id' => auth()->user()->id,
			'device_type' =>  $type,
			'register_date' => time(),
			'update_date' => time(),
			'status' => '1',
			'device_os' => $request->device_os,
		);
						
		
		//check device exist
		$device_id = DB::table('devices')->where('device_id','=', $request->device_id)->get();
	
		if(count($device_id)>0){
			
			DB::table('devices')
				->where('device_id', $request->device_id)
				->update($device_data);
			//print 'exist';
		}
		else{
			$device_id = DB::table('devices')->insertGetId($device_data);	
			//print 'new';
		}
		return response()->json(["status" => "200","success" =>1,'msg'=>'Device is registered.']);
		
	}
}
