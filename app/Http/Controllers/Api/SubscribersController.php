<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Shop\Subscribers\Subscribers;
//validator is builtin class in laravel
use Validator;
use App;
use Lang;
use DB;
//for password encryption or hash protected
use Hash;
use App\Administrator;

//for authenitcate login data
use Auth;

//use Illuminate\Foundation\Auth\ThrottlesLogins;
//use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

//for requesting a value 

class SubscribersController extends Controller
{
	
	public function add(Request $request){
		
		$Subscribers = new Subscribers();
		if(count($Subscribers->getSubscribers($request->input('email_address'))) <= 0){
			$data = $request->except('_method', '_token');
			$Subscribers->createSubscribers($data);
			$res = array();
			if($Subscribers){
				$res['status'] = 200;
				$res['res_code'] = 1;
				$res['res_msg'] =  "Thank You For Subscribing!";
			}else{
				$res['status'] = 200;
				$res['res_code'] = 0;
				$res['res_msg'] =  "Error occured while inserting data";
			}
			return response()->json($res);
		}else{
			$res['status'] = 200;
			$res['res_code'] = 0;
			$res['res_msg'] =  "You are already subscribed!";
			return response()->json($res);
		}
		
	}
}
