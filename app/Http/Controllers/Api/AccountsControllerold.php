<?php

namespace App\Http\Controllers\Api;

use App\Shop\Couriers\Repositories\Interfaces\CourierRepositoryInterface;
use App\Shop\Customers\Repositories\CustomerRepository;
use App\Shop\Customers\Repositories\Interfaces\CustomerRepositoryInterface;
use App\Http\Controllers\Controller;
use App\Shop\Orders\Order;
use App\Shop\Orders\Transformers\OrderTransformable;
use App\Shop\Wishlist\Wishlist;
use Illuminate\Http\Request;

class AccountsController extends Controller
{
    use OrderTransformable;

    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepo;

    /**
     * @var CourierRepositoryInterface
     */
    private $courierRepo;

    /**
     * AccountsController constructor.
     *
     * @param CourierRepositoryInterface $courierRepository
     * @param CustomerRepositoryInterface $customerRepository
     */
    public function __construct(
        CourierRepositoryInterface $courierRepository,
        CustomerRepositoryInterface $customerRepository
    ) {
        $this->customerRepo = $customerRepository;
        $this->courierRepo = $courierRepository;
    }

    public function index()
    {
        $customer = $this->customerRepo->findCustomerById(auth()->user()->id);

        $customerRepo = new CustomerRepository($customer);
        $orders = $customerRepo->findOrders(['*'], 'created_at');

        $orders->transform(function (Order $order) {
            return $this->transformOrder($order);
        });

        $addresses = $customerRepo->findAddresses();

        return response()->json(["status" => "200",
            'customer' => $customer,
            'orders' => $this->customerRepo->paginateArrayResults($orders->toArray(), 15),
            'addresses' => $addresses
        ]);
    } 
	public function update(UpdateCustomerRequestFront $request)
    {
		
		if($request->has('type')){
			if($request->input('type') == 'details'){
				$customer = $this->customerRepo->findCustomerById(auth()->user()->id);
				$update = new CustomerRepository($customer);
				$data = $request->except('_method', '_token','type');
				$update->updateCustomer($data);
				return redirect()->route('accounts',['tab' => 'profile'])->with('message', 'Account details updated successfuly !.');
			}
			if($request->input('type') == 'password'){
				
				if (!(Hash::check($request->input('current-password'), auth()->user()->password))) {
					// The passwords matches
					return redirect()->route('accounts',['tab' => 'profile'])->with("error","Your current password does not matches with the password you provided. Please try again.");
				}
				if(strcmp($request->input('current-password'), $request->input('password')) == 0){
					//Current password and new password are same
					return redirect()->route('accounts',['tab' => 'profile'])->with("error","New Password cannot be same as your current password. Please choose a different password.");
				}
				if(!strcmp($request->input('password'), $request->input('c-password')) == 0){
					//Current password and new password are same
					return redirect()->route('accounts',['tab' => 'profile'])->with("error","Your password and confirmation password do not match !.");
				}
				
				$customer = $this->customerRepo->findCustomerById(auth()->user()->id);

				$update = new CustomerRepository($customer);
				$data = $request->except('_method', '_token','type');
				if ($request->has('password')) {
					$data['password'] = bcrypt($request->input('password'));
					$data['login_token'] = Password::getRepository()->createNewToken();
				}
				$update->updateCustomer($data);
				return response()->json(["status" => "200", "success" =>1,'mag'=>'Password updated successfuly !.' ]);
			}
		}
    }
	public function wishlistAdd(Request $request){
		$Wishlist = new Wishlist();
		$result = false;
		if($request->input('action') == 'add'){
			$data = $request->except('_method', '_token','action');
			$result = $Wishlist->AddToWishlist($data);
		}
		if($request->input('action') == 'delete'){
			$result = $Wishlist->deleteWishlist($request->input('product_id'));
		}
		$res_code = 0;
		if($result){
			$res_code = 1;
		}
		return response()->json(["status" => "200", "success" =>$res_code,'mag'=>'success' ]);
	}
	public function wishlist()
    {
		$Wishlist = new Wishlist();
		$list =  $Wishlist->getWishlistByCustomer();
		foreach($list as $key=>$row){
			$list[$key]->cover = env("APP_URL")."/storage/products/".$row->cover;
		}
		return response()->json(["status" => "200", "wishlist" =>$list]);
	}
	public function my_orders()
    {
		$customer = $this->customerRepo->findCustomerById(auth()->user()->id);
        $customerRepo = new CustomerRepository($customer);
        $orders = $customerRepo->findOrders(['*'], 'created_at');
		$orders->transform(function (Order $order) {
            return $this->transformOrder($order);
        });
        $addresses = $customerRepo->findAddresses();
		return response()->json(["status" => "200", "orders" =>$this->customerRepo->paginateArrayResults($orders->toArray(), 15)]);
	}
	public function my_address()
    {
		$customer = $this->customerRepo->findCustomerById(auth()->user()->id);
        $customerRepo = new CustomerRepository($customer);
        $orders = $customerRepo->findOrders(['*'], 'created_at');
        $orders->transform(function (Order $order) {
            return $this->transformOrder($order);
        });
        $addresses = $customerRepo->findAddresses();
		return response()->json(["status" => "200", "address" => $addresses]);
	}
	public function my_details()
    {
		$customer = $this->customerRepo->findCustomerById(auth()->user()->id);
		return response()->json(["status" => "200", "customer" => $customer]);
	}
}
