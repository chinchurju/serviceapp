<?php

namespace App\Http\Controllers\Admin\Reports;

use App\Exports\OrdersExport;
use App\Shop\Addresses\Repositories\Interfaces\AddressRepositoryInterface;
use App\Shop\Addresses\Transformations\AddressTransformable;
use App\Shop\Couriers\Courier;
use App\Shop\Couriers\Repositories\CourierRepository;
use App\Shop\Couriers\Repositories\Interfaces\CourierRepositoryInterface;
use App\Shop\Customers\Customer;
use App\Shop\Customers\Repositories\CustomerRepository;
use App\Shop\Customers\Repositories\Interfaces\CustomerRepositoryInterface;
use App\Shop\Orders\Order;
use App\Shop\Orders\Repositories\Interfaces\OrderRepositoryInterface;
use App\Shop\Orders\Repositories\OrderRepository;
use App\Shop\OrderStatuses\OrderStatus;
use App\Shop\OrderStatuses\Repositories\Interfaces\OrderStatusRepositoryInterface;
use App\Shop\OrderStatuses\Repositories\OrderStatusRepository;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use \Datetime;
use Maatwebsite\Excel\Facades\Excel;

use Mail;


class SalesController extends Controller
{
    use AddressTransformable;

    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepo;

    /**
     * @var CourierRepositoryInterface
     */
    private $courierRepo;

    /**
     * @var AddressRepositoryInterface
     */
    private $addressRepo;

    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepo;

    /**
     * @var OrderStatusRepositoryInterface
     */
    private $orderStatusRepo;

    public function __construct(
        OrderRepositoryInterface $orderRepository,
        CourierRepositoryInterface $courierRepository,
        AddressRepositoryInterface $addressRepository,
        CustomerRepositoryInterface $customerRepository,
        OrderStatusRepositoryInterface $orderStatusRepository
		
    ) {
        $this->orderRepo = $orderRepository;
        $this->courierRepo = $courierRepository;
        $this->addressRepo = $addressRepository;
        $this->customerRepo = $customerRepository;
        $this->orderStatusRepo = $orderStatusRepository;

        $this->middleware(['permission:update-order, guard:employee'], ['only' => ['edit', 'update']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		
		//
		//print_r(request()->all());exit;
        ///$list = $this->orderRepo->listOrders('created_at', 'desc');
		//order = new Order()->where('created_at1' ,'>=',request()->input('frmdate'));
        $order =   Order::where('id','>','0');
		
		if (request()->has('dateRange')) {
			if(!empty(request()->input('dateRange'))){
				$reqst = explode("-",request()->input('dateRange'));
				
				
				$date = date_create_from_format('m/d/Y',trim($reqst[0]));
				$frmdt = date_format($date, 'Y-m-d');
				
				/*$fdate = new DateTime();
				$fdate->createFromFormat('m/d/Y',trim($reqst[0]));
				echo $frmdt = $fdate->format('Y-m-d');
				
				
				$tdate = new DateTime();
				$fdate->createFromFormat('m/d/Y',trim($reqst[1]));
				echo $todt = $fdate->format('Y-m-d');*/
				
				$date1 = date_create_from_format('m/d/Y',trim($reqst[1]));
				$todt = date_format($date1, 'Y-m-d');
				
				$order->where('created_at' ,'>=',$frmdt);	
				$order->where('created_at' ,'<=',$todt);
			}
		}
		
		if(request()->has('status')){
			if(!empty(request()->input('status'))){
				$order->where('order_status_id' ,'=',request()->input('status'));	
			}
		}
		//$order->->join('customers', 'customers.id', '=', 'orders.customer_id')
		if (request()->has('order_by')) {
            $order_sy = "ASC";
            if(request()->has('order'))
             $order_sy = request()->input('order');
             $order->orderBy(request()->input('order_by'), $order_sy);
        }		
		$orders = $order->with(array('customer'=>function($query){
			$query->select('*');
		}))->with(array('orderStatus'=>function($query){
			$query->select('*');
		}))->get();
		$total = 0;
		if($orders){
			foreach($orders as $order){
				$total+=$order->total_paid;
			}
		}
		$input = request()->except('page','order_by','order');
		$ext = '';
		if($input){
		    foreach($input as $key=>$val)
		    $ext .= '&'.$key."=".$val;
		    
		}
	
		
        return view('admin.reports.sale', ['orders' => $orders,'total'=>$total,'statuses' => $this->orderStatusRepo->listOrderStatuses(),'extraparam'=>$ext,'']);
    }
	public function export()
    {
		return Excel::download(new OrdersExport, 'users.xlsx');
        $order =   Order::where('id','>','0');
		
		if (request()->has('dateRange')) {
			if(!empty(request()->input('dateRange'))){
				$reqst = explode("-",request()->input('dateRange'));
				
				
				$date = date_create_from_format('m/d/Y',trim($reqst[0]));
				$frmdt = date_format($date, 'Y-m-d');
				
				/*$fdate = new DateTime();
				$fdate->createFromFormat('m/d/Y',trim($reqst[0]));
				echo $frmdt = $fdate->format('Y-m-d');
				
				
				$tdate = new DateTime();
				$fdate->createFromFormat('m/d/Y',trim($reqst[1]));
				echo $todt = $fdate->format('Y-m-d');*/
				
				$date1 = date_create_from_format('m/d/Y',trim($reqst[1]));
				$todt = date_format($date1, 'Y-m-d');
				
				$order->where('created_at' ,'>=',$frmdt);	
				$order->where('created_at' ,'<=',$todt);
			}
		}
		if(request()->has('status')){
			if(!empty(request()->input('status'))){
				$order->where('order_status_id' ,'=',request()->input('status'));	
			}
		}
		//$order->->join('customers', 'customers.id', '=', 'orders.customer_id')
				
		$orders = $order->with(array('customer'=>function($query){
			$query->select('*');
		}))->with(array('orderStatus'=>function($query){
			$query->select('*');
		}))->get();
		$total = 0;
		if($orders){
			foreach($orders as $order){
				$total+=$order->total_paid;
			}
		}
		
        return view('admin.reports.sale', ['orders' => $orders,'total'=>$total,'statuses' => $this->orderStatusRepo->listOrderStatuses()]);
    }
    /**
     * Display the specified resource.
     *
     * @param  int $orderId
     * @return \Illuminate\Http\Response
     */
    public function show($orderId)
    {
        $order = $this->orderRepo->findOrderById($orderId);
        $order->courier = $this->courierRepo->findCourierById($order->courier_id);
        $order->address = $this->addressRepo->findAddressById($order->address_id);

        $orderRepo = new OrderRepository($order);

        $items = $orderRepo->listOrderedProducts();

        return view('admin.orders.show', [
            'order' => $order,
            'items' => $items,
            'customer' => $this->customerRepo->findCustomerById($order->customer_id),
            'currentStatus' => $this->orderStatusRepo->findOrderStatusById($order->order_status_id),
            'payment' => $order->payment,
            'user' => auth()->guard('employee')->user()
        ]);
    }

    /**
     * @param $orderId
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($orderId)
    {
        $order = $this->orderRepo->findOrderById($orderId);
        $order->courier = $this->courierRepo->findCourierById($order->courier_id);
        $order->address = $this->addressRepo->findAddressById($order->address_id);

        $orderRepo = new OrderRepository($order);

        $items = $orderRepo->listOrderedProducts();

        return view('admin.orders.edit', [
            'statuses' => $this->orderStatusRepo->listOrderStatuses(),
            'order' => $order,
            'items' => $items,
            'customer' => $this->customerRepo->findCustomerById($order->customer_id),
            'currentStatus' => $this->orderStatusRepo->findOrderStatusById($order->order_status_id),
            'payment' => $order->payment,
            'user' => auth()->guard('employee')->user()
        ]);
    }

    /**
     * @param Request $request
     * @param $orderId
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $orderId)
    {
        $order = $this->orderRepo->findOrderById($orderId);
        $orderRepo = new OrderRepository($order);

        if ($request->has('total_paid') && $request->input('total_paid') != null) {
            $orderData = $request->except('_method', '_token');
        } else {
            $orderData = $request->except('_method', '_token', 'total_paid');
        }

        $orderRepo->updateOrder($orderData);

        return redirect()->route('admin.orders.edit', $orderId);
    }

    /**
     * Generate order invoice
     *
     * @param int $id
     * @return mixed
     */
    public function generateInvoice(int $id)
    {
        $order = $this->orderRepo->findOrderById($id);

        $data = [
            'order' => $order,
            'products' => $order->products,
            'customer' => $order->customer,
            'courier' => $order->courier,
            'address' => $this->transformAddress($order->address),
            'status' => $order->orderStatus,
            'payment' => $order->paymentMethod
        ];

        $pdf = app()->make('dompdf.wrapper');
        $pdf->loadView('invoices.orders', $data)->stream();
        return $pdf->stream();
    }

    /**
     * @param Collection $list
     * @return array
     */
    private function transFormOrder(Collection $list)
    {
        $courierRepo = new CourierRepository(new Courier());
        $customerRepo = new CustomerRepository(new Customer());
        $orderStatusRepo = new OrderStatusRepository(new OrderStatus());

        return $list->transform(function (Order $order) use ($courierRepo, $customerRepo, $orderStatusRepo) {
            $order->courier = $courierRepo->findCourierById($order->courier_id);
            $order->customer = $customerRepo->findCustomerById($order->customer_id);
            $order->status = $orderStatusRepo->findOrderStatusById($order->order_status_id);
            return $order;
        })->all();
    }
}
