<?php
/*
Project Name: IonicEcommerce
Project URI: http://ionicecommerce.com
Author: VectorCoder Team
Author URI: http://vectorcoder.com/
Version: 2.1
*/
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

//validator is builtin class in laravel
use Validator;
use App;
use DB;
//for password encryption or hash protected

use Hash;

//for authenitcate login data
use Auth;
//for redirect
use Illuminate\Support\Facades\Redirect;
//use Illuminate\Foundation\Auth\ThrottlesLogins;
//use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

//for requesting a value
use Illuminate\Http\Request;
//use Illuminate\Routing\Controller;

class AdminReviewsController extends Controller
{

    public function index(Request $request){
        $data['results'] = DB::table('reviews')
            ->leftJoin('reviews_description','reviews_description.reviews_id','=','reviews.reviews_id')
			->leftJoin('products','products.id','=','reviews.products_id');
        $title = array('pageTitle' => "Reviews");
        $data['search'] = $request->input('search');
        if($data['search']!=''){
            $data['results'] = $data['results']->where(function($query) use ($data){
                $query->where('reviews.customers_name','LIKE',"%{$data['search']}%")
                    ->orWhere('products.name', 'LIKE',"%{$data['search']}%")
                    ->orWhere('reviews_description.reviews_text', 'LIKE',"%{$data['search']}%");
            });
        }
        $data['results'] =  $data['results']->select('reviews.*','reviews_description.reviews_text','products.name')->orderBy('reviews.reviews_id','DESC')->paginate(20);

        $data['results']->appends(['search'=>$data['search']]);

        return view('admin.reviews',$title,$data);
    }

}
