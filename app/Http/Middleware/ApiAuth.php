<?php

namespace App\Http\Middleware;

use Closure;
use App\Shop\Products\Product;
use App\Shop\Products\Repositories\Interfaces\ProductRepositoryInterface;
use App\Shop\Categories\Category;
use App\Shop\Categories\Repositories\CategoryRepository;
use App\Shop\Categories\Repositories\Interfaces\CategoryRepositoryInterface;
use Illuminate\Http\Request;
use App\Shop\Customers\Repositories\Interfaces\CustomerRepositoryInterface;
use Illuminate\Support\Facades\Auth;
class ApiAuth
{
	/**
     * @var ProductRepositoryInterface
     */
    private $customerRepo;
	
	public function __construct(CustomerRepositoryInterface $customerRepository)
    {
        $this->customerRepo = $customerRepository;
    }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next )
    {
		if(!$request->header('userToken'))
		{
			return response()->json(["status" => "400", "msg" =>"Access Denied"]);
		}	
		$cust = $this->customerRepo->findCustomerByToken(['login_token'=>$request->header('userToken')]);
		if($cust){
			Auth::login($cust);
		}else{
			return response()->json(["status" => "401", "msg" =>"Invalid token"]);
		}
		
        //return redirect(route('admin.login'));
        return $next($request);
    }
}
