<?php

namespace App\Http\Middleware;

use Closure;
use App\Shop\Products\Product;
use App\Shop\Products\Repositories\Interfaces\ProductRepositoryInterface;
use App\Shop\Categories\Category;
use App\Shop\Categories\Repositories\CategoryRepository;
use App\Shop\Categories\Repositories\Interfaces\CategoryRepositoryInterface;
class NewProducts
{
	/**
     * @var ProductRepositoryInterface
     */
    private $productRepo;
	
	public function __construct(ProductRepositoryInterface $productRepository,CategoryRepositoryInterface $categoryRepository)
    {
        $this->productRepo = $productRepository;
		$this->categoryRepo = $categoryRepository;
    }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next )
    {
		$products = [];
		$categories = Category::where('parent_id','=','1')->orderBy('display_order', 'DESC')->with('children')->get();
		
		foreach($categories as $key=>$row){
			$category 	= $this->categoryRepo->findCategoryBySlug(['id' => $row->id]);
			$repo 		= new CategoryRepository($category);
			//$products[$row->id] = $repo->findProducts()->where('status', 1)->first();
			$categories[$key]->products1 = Product::where('status', 1)->where('new_product_flag', 1)->first();
		}
		//print_r($categories);exit;
		
		view()->share('categories_new', $categories);
		
        //return redirect(route('admin.login'));
        return $next($request);
    }
}
