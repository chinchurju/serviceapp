<?php

namespace App\Shop\Devices;
use Illuminate\Database\Eloquent\Model;
use App\Shop\Customers\Customer;
class Device extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	protected $table = 'devices';
	protected $primaryKey = 'id';
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function customer()
    {
        return $this->belongsTo(Customer::class,'customers_id');
    }

   
}
