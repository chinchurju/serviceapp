<?php

namespace App\Shop\Products;

use App\Shop\Brands\Brand;
use App\Shop\Categories\Category;
use App\Shop\Countries\Country;
use App\Shop\ProductAttributes\ProductAttribute;
use App\Shop\ProductImages\ProductImage;
use Gloudemans\Shoppingcart\Contracts\Buyable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;
use Nicolaslopezj\Searchable\SearchableTrait;

class Product extends Model implements Buyable
{
    use SearchableTrait;

    public const MASS_UNIT = [
        'OUNCES' => 'oz',
        'GRAMS' => 'gms',
        'POUNDS' => 'lbs'
    ];

    public const DISTANCE_UNIT = [
        'CENTIMETER' => 'cm',
        'METER' => 'mtr',
        'INCH' => 'in',
        'MILIMETER' => 'mm',
        'FOOT' => 'ft',
        'YARD' => 'yd'
    ];

    /**
     * Searchable rules.
     *
     * @var array
     */
    protected $searchable = [
        'columns' => [
            'products.name' => 10,
            'products.description' => 5
        ]
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'sku',
        'name',
        'name_ar',
        'description',
        'description_ar',
        'short_description',
        'short_description_ar',
        'duration',
        'cover',
        'quantity',
        'payment',
        'price',
        'emprice',
        'status',
        'weight',
        'mass_unit',
        'status',
        'sale_price',
        'length',
        'width',
        'height',
        'distance_unit',
        'slug',
		'new_product_flag'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }
    
     public function countries()
    {
        return $this->belongsToMany(Country::class,'product_id');
    }

    /**
     * Get the identifier of the Buyable item.
     *
     * @param null $options
     * @return int|string
     */
    public function getBuyableIdentifier($options = null)
    {
        return $this->id;
    }

    /**
     * Get the description or title of the Buyable item.
     *
     * @param null $options
     * @return string
     */
    public function getBuyableDescription($options = null)
    {
        return $this->name;
    }

    /**
     * Get the price of the Buyable item.
     *
     * @param null $options
     * @return float
     */
    public function getBuyablePrice($options = null)
    {
        return $this->price;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function images()
    {
        return $this->hasMany(ProductImage::class);
    }

    /**
     * @param string $term
     * @return Collection
     */
    public function searchProduct(string $term) : Collection
    {
         $country_id = '';
        if(request()->has('country_id'))
             $country_id = request()->input('country_id');

    /*    if(auth()->check()){

             $country_id = auth()->user()->country_id;
        }*/

        if($country_id){
            return self::search($term)->select('*','products.id as id')->join('country_product', 'country_product.product_id', '=', 'products.id')->where('country_product.country_id',$country_id)->groupBy('products.brand_id')->get();
         }
         else{
             return self::search($term)->select('*','products.id as id')->join('country_product', 'country_product.product_id', '=', 'products.id')->groupBy('products.brand_id')->get();
         }
    }
    public function listProductCountryWise() : Collection
    {
         $country_id = '';
        if(request()->has('country_id'))
             $country_id = request()->input('country_id');

    /*    if(auth()->check()){

             $country_id = auth()->user()->country_id;
        }*/

        if($country_id){
             return self::select('*','products.id as id')->join('country_product', 'country_product.product_id', '=', 'products.id')->where('country_product.country_id',$country_id)->get();
         }
         else{
             return self::get();
         }
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function attributes()
    {
        return $this->hasMany(ProductAttribute::class);
    }
	/**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function attributesById($id)
    {
        return $this->hasMany(ProductAttribute::class)->where('id', $id);
    }
   
   /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }
}
