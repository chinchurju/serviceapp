<?php

namespace App\Shop\Checkout;

use App\Shop\Carts\Repositories\CartRepository;
use App\Shop\Carts\ShoppingCart;
use App\Shop\Carts\Cart;
use App\Shop\Orders\Order;
use App\Shop\Orders\Repositories\OrderRepository;

class CheckoutRepository
{
    /**
     * @param array $data
     *
     * @return Order
     */
    public function buildCheckoutItems(array $data,$r = 'web') : Order
    {
        $orderRepo = new OrderRepository(new Order);
        $cartRepo = new CartRepository(new ShoppingCart);

        $order = $orderRepo->create([
            //'reference' 			=> $this->generateOrderNR(),
         //   'courier_id' 			=> $data['courier_id'],
			//'courier' 				=> $data['courier'],
            'customer_id' 			=> $data['customer_id'],
          //  'address_id' 			=> $data['address_id'],
			//'shipping_address_id' 	=> $data['shipping_address_id'],
            'order_status_id' 		=> $data['order_status_id'],
            'payment' 				=> $data['payment'],
           // 'discounts' 			=> $data['discounts'],
            'total_products' 		=> $data['total_products'],
			//'shipping_charge' 		=> $data['shipping_charge'],
            'total' 				=> $data['total'],
            //'total_paid' 			=> $data['total_paid'],
           // 'tax' 				=> $data['tax'],
            'sale_price'             =>$data['sale_price'],
            'servicedate'           => $data['servicedate'],
            'servicetime' 			=> $data['servicetime'],
              'flagemergency'           => $data['flagemergency'],
             'country_id' 			=> $data['country_id']
            //'description'           => $data['description']
        ]);
         $number = 2000;
        $order->reference = 'ORD' . sprintf('%010d',intval($number+($order->id)));
        $order->save();
        $orderRepo = new OrderRepository($order);
		if($r=='web'){
			$orderRepo->buildOrderDetails($cartRepo->getCartItems());
		}else{
			$cart = new Cart();
			$orderRepo->buildOrderDetailsApi($cart->getCart());
		}
		
        return $order;
    }
	public function generateOrderNR()
	{
		
		$number = 2000;
		// Get the last created order
		$lastOrder = Order::orderBy('created_at', 'desc')->first();
		if ( $lastOrder ){
			// We get here if there is no order at all
			// If there is no number set it to 0, which will be 1 at the end.

			$number += $lastOrder->id;
		}
		else {
			$number += 0;
		}
		//echo $number;exit;
		// If we have ORD000001 in the database then we only want the number
		// So the substr returns this 000001

		// Add the string in front and higher up the number.
		// the %05d part makes sure that there are always 6 numbers in the string.
		// so it adds the missing zero's when needed.
	 
		return  'ORD' . sprintf('%010d', intval($number) + 1);exit;
	}
}
