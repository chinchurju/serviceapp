<?php

namespace App\Repositories\Eloquent;

use App\Shop\Subscribers\Subscribers;
use App\Repositories\Contracts\SubscribersRepository;

use Kurt\Repoist\Repositories\Eloquent\AbstractRepository;

class EloquentSubscribersRepository extends AbstractRepository implements SubscribersRepository
{
    public function entity()
    {
        return Subscribers::class;
    }
}
