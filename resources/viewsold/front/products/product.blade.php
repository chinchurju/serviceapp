@extends('layouts.front.app')
@section('content')
<div class="detail">
  <div class="container">
    <div class="col-md-12 no-padding">
      <div class="row">
        <div class="col-md-6">
          <div class="item">            
            <div class="clearfix">
                <ul id="vertical" class="gallery list-unstyled ">
				
					@if(isset($product->cover))
                    <li data-thumb="{{ asset("storage/products/$product->cover") }}" ata-src="{{ asset("storage/products/$product->cover") }}"><img src="{{ asset("storage/products/thumb270x300_$product->cover") }}"  data-zoom-image="{{ asset("storage/products/$product->cover") }}" class="img_zomm img-responsive" /></li>
                    @endif
					@if(isset($images) && !$images->isEmpty())
						@foreach($images as $image)
							<li data-thumb="{{ asset("storage/products/$image->src") }}" ata-src="{{ asset("storage/products/$image->src") }}"><img src="{{ asset("storage/products/thumb270x300_$image->src") }}" data-zoom-image="{{ asset("storage/products/$image->src") }}" class="img_zomm img-responsive"/></li>
						@endforeach
					@endif
               
                </ul>
            </div>
        </div>
        </div>
        
        <div class="col-md-6">
		  
          <h2>@if((int)$product->brand_id > 0){{ $product->brand_name }} @else {{ $product->name }} @endif</h2>
		  @if((int)$product->brand_id > 0)
			<h5>{{ $product->name }}</h5>
	      @endif
          <div class="prc">{{ $product->price }} <span>{{ config('cart.currency') }}</span> </div>
          <div class="abf">
            <a href="javascript:void(0);" id="add_to_cart_btn">Ad to BAG</a>
            <a href="#" data-toggle="modal" data-target="#myModal">ADD TO Favorites</a>
          </div>
		  <div class="radio-tile-group">
		  @include('layouts.errors-and-messages')
			<form action="{{ route('cart.store') }}" class="form-inline" method="post" id="cart_frm">
				{{ csrf_field() }}
				@if(isset($productAttributes) && !$productAttributes->isEmpty())
					<div class="form-group">
						<label for="productAttribute">Choose Combination</label> <br />
						<select name="productAttribute" id="productAttribute" class="form-control select2">
							@foreach($productAttributes as $productAttribute)
								<option value="{{ $productAttribute->id }}">
									@foreach($productAttribute->attributesValues as $value)
										{{ $value->attribute->name }} : {{ ucwords($value->value) }}
									@endforeach
									@if(!is_null($productAttribute->price))
										( {{ config('cart.currency_symbol') }} {{ $productAttribute->price }})
									@endif
								</option>
							@endforeach
						</select>
					</div><hr>
				@endif
				<div class="form-group">
					<input type="hidden"
						   class="form-control"
						   name="quantity"
						   id="quantity"
						   placeholder="Quantity"
						   value="1" />
					<input type="hidden" name="product" value="{{ $product->id }}" />
				</div>
			</form>
          
         </div> 
          
          <div class="pih clearfix">
            <article class="accord acrd">
              <h4 class="accord__head">Description </h4>
              <div class="accord__body">
                {!! $product->description !!}
              </div>
            </article>
            
       
            <!--
            <dl class="responsive-tabs clearfix">
            <dt> Product Info</dt>
            <dd >
            <div class="col-md-12 clearfix animated fadeIn no-padding">
              {!! $product->description !!}
            </div>
            </dd>
            
            <dt> SHORT DESCRIPTION</dt>
             <dd >
            <div class="col-md-12 clearfix animated fadeIn no-padding">
              {!! $product->description !!}
            </div>
            </dd>
            </dl>
            -->
            
            
          </div>
          <!--
          <div class="shr">
            <ul>
              <li>Share</li>
              <li><a href="#"><i class="fa fa-facebook"></i></a></li>
              <li><a href="#"><i class="fa fa-youtube-play"></i></a></li>
              <li><a href="#"><i class="fa fa-twitter"></i></a></li>
            </ul>
          </div>
           -->
        </div>
        <div class="row">
			<div class="col-md-12">
				<div class="ja">
				  <h2>RELATED PRODUCTS</h2>
				  <div class="container">
					 <div class="col-md-12 no-padding">
						@if(!$products->isEmpty())
						<div id="owl-demo" class="owl-carousel owl-theme">
					
						 @foreach($products as $product)
						 <div class="item">
						   <figure><img src='{{ asset("storage/products/thumb270x300_$product->cover") }}?w=400' class="prd_{{ $product->id }}">
								<form action="{{ route('cart.ajaxcart') }}" class="form-inline" method="post" id="prd_{{ $product->id }}" style="display:none">
								 {{ csrf_field() }}
									<input type="hidden" name="quantity" value="1" />
									<input type="hidden" name="product" value="{{ $product->id }}">
								</form>
								<form action="{{ route('accounts.wishlist') }}" class="form-inline" method="post" id="wisthlist_{{ $product->id }}" style="display:none">
								 {{ csrf_field() }}
									<input type="hidden" name="product_id" value="{{ $product->id }}">
								</form>
							 <figcaption>
								<a href={{ route('front.get.product', str_slug($product->slug)) }}" ><img src="{!! asset('resources/assets/front/') !!}/images/crt.png"></a>
								<a href=@if(!auth()->check())"{{ route('login') }}" @else"javascript:void(0);" class="wish_list_a @if(in_array($product->id,$mywishlist))  wishlist_img @endif " data-id="wisthlist_{{ $product->id }}" @endif><img src="{!! asset('resources/assets/front/') !!}/images/wish.png"></a>
							 </figcaption>
							</figure>
						   <a href="{{route('front.get.product', $product->slug)}}"><h4>{{ $product->name}}</h4>
							<p>{{$product->price}} {{ config('cart.currency') }}</p>
						   </a>
						 </div>
						 @endforeach
						</div>
						@endif
					 </div>
				  </div>
				</div>
			</div>
		</div> 
      </div>
    </div>
  </div>
</div>



<script src="{!! asset('resources/assets/front/') !!}/js/lightslider.js"></script>
<script src="{!! asset('resources/assets/front/') !!}/js/jquery.elevatezoom.js" type="text/javascript"></script> 
<script>
  $(document).ready(function() {
	  
  });
</script>
<script>
  $(document).ready(function() {
	  $('#add_to_cart_btn').click(function(){
		  $('#cart_frm').submit();
	  });
	  
	  
	  
	$("#content-slider").lightSlider({
		loop:true,
		keyPress:true
	});
	$('#vertical').lightSlider({
		  gallery:true,
		  item:1,
		  vertical:true,
		  verticalHeight:550,
		  vThumbWidth:80,
		  thumbItem:4,
		  thumbMargin:4,
		  slideMargin:0,
		  onAfterSlide:function(el,dd){
			  
			console.log(dd);  
			$('.img_zomm').removeData('elevateZoom');
			$('.zoomWrapper img.zoomed').unwrap();
			$('.zoomContainer').remove();
			$('.lSSlideWrapper li.active .img_zomm').elevateZoom({
					scrollZoom : true,
			});
		  },
		  onSliderLoad:function(el){
			$('.img_zomm').removeData('elevateZoom');
			$('.zoomWrapper img.zoomed').unwrap();
			$('.zoomContainer').remove();
			$('.lSSlideWrapper li.active .img_zomm').elevateZoom({
					scrollZoom : true,
			});
		  }
	});
});

</script>
<script>
   $.fn.responsiveTabs = function () {
    return this.each(function () {
        var el = $(this), tabs = el.find('dt'), content = el.find('dd'), placeholder = $('<div class="responsive-tabs-placeholder"></div>').insertAfter(el);
        tabs.on('click', function () {
            var tab = $(this);
            tabs.not(tab).removeClass('active');
            tab.addClass('active');
            placeholder.html(tab.next().html());
        });
        tabs.filter(':first').trigger('click');
    });
};
</script>


<script src="{!! asset('resources/assets/front/') !!}/js/jquery.accord.min.js"></script> 
<script>
    $('.acrd').accord();
</script>





@endsection