@extends('layouts.front.app')

@section('content')
    
<div class="abt-tp"><h2>{!! stripslashes($page[0]->name) !!}</h2></div>

<div class="cms_div">
	
	<div class="container">
	  <div class="col-md-12 no-padding">
		{!! stripslashes($page[0]->description) !!}
		
	  </div>
	
	</div>
</div>

@endsection