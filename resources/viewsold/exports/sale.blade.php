<table class="table">
	<thead>
		<tr>
			<td class="col-md-3">Order ID</td>
			<td class="col-md-3">Date</td>
			<td class="col-md-3">Customer</td>
			<td class="col-md-3">Status</td>
			<td class="col-md-3">Products</td>

			<td class="col-md-2">Total</td>
		</tr>
	</thead>
	<tbody>
	@if(count($orders)>0)
		@foreach ($orders as $order)
			<tr>
				<td>{{$order->id}}</td>
				<td><a title="Show order" href="{{ route('admin.orders.show', $order->id) }}">{{ date('M d, Y h:i a', strtotime($order->created_at)) }}</a></td>
				<td>{{$order->customer->name}}</td>
				<td>{{$order->orderStatus->name}}</td>
				<td>
					@foreach($order->products as $product)
						{{$product->name}}*{{$product->quantity}} ({{$product->price}}QAR),<br>
					@endforeach
				</td>
				<td>
					<span class="label @if($order->total != $order->total_paid) label-danger @else label-success @endif">{{ config('cart.currency') }} {{ $order->total }}</span>
				</td>
			</tr>
		@endforeach
	@else
		<tr>
			<td colspan="5" align="center">No records found</td>
			
		</tr>
	@endif
	</tbody>
</table>
            