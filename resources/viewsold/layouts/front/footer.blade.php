
<footer>
   <div class="container">
      <div class="col-md-12 no-padding">
         <div class="row">
            <div class="col-md-5 col-sm-8 ">
               <h4>Useful Information</h4>
               <div class="row">
                 <div class="col-md-6 col-xs-6">
                    <ul>
                       <li><a href="{{ route('page','exchange')}}">Exchange & Returns</a></li>
                       <li><a href="{{ route('page','payments')}}">Payments</a></li>
                       <li><a href="{{ route('page','faqs')}}">FAQs</a></li>
						<li><a href="{{ route('contactus.index')}}">Contact Us</a></li>
                    </ul>
                 </div>
                 <div class="col-md-6 col-xs-6">
                    <ul>
                       <li><a href="{{ route('page','terms-conditions')}}">Terms & Conditions</a></li>
                       <li><a href="{{ route('page','privacy-policy')}}">Privacy Policy</a></li>
                       <li><a href="{{ route('page','cookies-policy')}}">Cookie Policy</a></li>
                    </ul>
                 </div>
               </div>
            </div>
            <div class="col-md-3 col-sm-4">
               <h4>Follow Us</h4>
				<ul>
                       <li><a href="#"> <i class="fa fa-facebook-square"></i> Facebook</a></li>
                       <li><a href="#"> <i class="fa fa-instagram"></i> Instagram</a></li>
                       <li><a href="#"> <i class="fa fa-twitter-square"></i> Twitter</a></li>
                    </ul>
			 </div>
            <div class="col-md-4 col-sm-12">
               <h2>stay communicated</h2>
               
               <form method="post" action="{{ route('subscribe')}}" class="clearfix">
				{{ csrf_field() }}
                <input type="email" class="form-control" id="email_input" placeholder="ENTER YOUR EMAIL ADDRESS" name="email_address"  data-rule-required="true" data-rule-email="true" data-msg-email="Please enter a valid email address">
                <button class="sub" type="button" >Subscribe</button>
				<span id="errdiv" ></span>
              </form>
               
            </div>
            
         </div>
      </div>
   </div> 

</footer>
	
	<div class="ftr-btm clearfix">
	  <div class="container">
		<div class="col-md-12 no-padding">
		    <div class="text-center">© 2018 MERWAD | All Rights Reserved.</div>
		
		</div>
	  </div>
	</div>
	
<script src="{!! asset('resources/assets/front') !!}/js/bootsnav.js" type="text/javascript"></script>
<script type="text/javascript" src="{!! asset('resources/assets/front') !!}/js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="{!! asset('resources/assets/front') !!}/js/jquery.themepunch.revolution.min.js"></script>
<script>
var revapi;
jQuery(document).ready(function() {
	   revapi = jQuery('.tp-banner').revolution(
		{
			delay:9000,
			startwidth:1170,
			startheight:650,
			hideThumbs:10,
			fullWidth:"on",
			forceFullWidth:"on"
		});
});	
</script>
<script src="{!! asset('resources/assets/front') !!}/js/owl.carousel.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.18.0/jquery.validate.min.js" type="text/javascript"></script>

<script>
$(document).ready(function() {
 $("#owl-demo").owlCarousel({
	margin: 20,
	nav: true,
	loop: false,
	autoplay:true,
	dots:false,
	rewind:true,
    autoplayTimeout:4000,
	autoplayHoverPause:true,
	navText : ["<i class='flaticon-left-arrow'></i>","<i class='flaticon-right-arrow'></i>"],
	responsive: {
	  0: {
		items: 1
	  },
	  600: {
		items:3
	  },
	  1000: {
		items: 4
	  }
	}
  })
})
</script>
<link href="//cdnjs.cloudflare.com/ajax/libs/animate.css/3.2.0/animate.min.css" rel="stylesheet">
<script src="{!! asset('resources/assets/front') !!}/js/instafeed.min.js" type="text/javascript"></script>
<script src="{!! asset('resources/assets/front') !!}/js/userfeed.js" type="text/javascript"></script>
<script src="{!! asset('resources/assets/front') !!}/js/jquery.collapsible.js"></script> 
<script src="{!! asset('resources/assets/front') !!}/js/bootstrap-notify.min.js"></script> 
<script>
$('#accordion-menu').collapsible({
  contentOpen: 1,
  accordion: true,
});
$('#collapse-menu').collapsible({
  contentOpen: 0
});
</script>
<script>
   $.fn.responsiveTabs = function () {
    return this.each(function () {
        var el = $(this), tabs = el.find('dt'), content = el.find('dd'), placeholder = $('<div class="responsive-tabs-placeholder"></div>').insertAfter(el);
        tabs.on('click', function () {
            var tab = $(this);
            tabs.not(tab).removeClass('active');
            tab.addClass('active');
            placeholder.html(tab.next().html());
        });
        tabs.filter(':first').trigger('click');
    });
};
$('.responsive-tabs').responsiveTabs();

</script>
<style>
.try {
  top:0px;
  left:0;
  margin-left: 15px;  
  margin-right: 15px;
  position: absolute !important;
  z-index:9999999;
  max-width: 90% ;
}

.target {
  font-size: 50px;
  float: right;
}
</style>
<script>
$(document).ready(function(){
	
 	
	$(".wish_list_a").click(function(){
		event.preventDefault();
		var ele = $(this);
		var id = $(this).data('id');
		var frm = $('#'+id);
		var post_url = frm.attr("action"); //get form action url
		var request_method = frm.attr("method"); //get form GET/POST method
		var form_data = frm.serialize(); //Encode form elements for submission
		var action = 'add';
		if($(this).hasClass('wishlist_img')){
			action = 'delete';
		}
		form_data+="&action="+action;
		$.ajax({
			url : post_url,
			type: request_method,
			data : form_data
		}).done(function(response){ //
			ele.toggleClass('wishlist_img');
		})
	});
	$('#email_input').keydown(function(event){
		if(event.keyCode == 13) {
		  event.preventDefault();
		  $(".sub").click();
		  return false;
		}
	 });
	$(".sub").click(function(){
		event.preventDefault();
		var id = $(this).data('id');
		var frm = $(this).parent();
		var post_url = frm.attr("action"); //get form action url
		var request_method = frm.attr("method"); //get form GET/POST method
		var form_data = frm.serialize(); //Encode form elements for submission
		var fl = frm.valid({
			errorElement : '#errdiv',
			errorLabelContainer: '.errorTxt'
		});
		if(fl){
			$.ajax({
				url : post_url,
				type: request_method,
				data : form_data
			}).done(function(response){ 
				var type = (response.res_code == '1')? 'success' : 'danger';
				frm[0].reset();
				$.notify({
					// optionstype: type,
					message: response.res_msg,
				},{
					// settings
					element: 'body',
					position: null,
					type: type,
					allow_dismiss: true,
					newest_on_top: false,
					showProgressbar: false,
					placement: {
						from: "top",
						align: "right"
					},
					offset: 20,
					spacing: 10,
					z_index: 1031,
					delay: 5000,
					timer: 1000,
					url_target: '_blank',
					mouse_over: null,
					animate: {
						enter: 'animated fadeInDown',
						exit: 'animated fadeOutUp'
					},
					onShow: null,
					onShown: null,
					onClose: null,
					onClosed: null,
					icon_type: 'class',
					
				});
				
			})
		}
	});
	
	
	
    $(".add_to_cart").click(function(){
		
		event.preventDefault(); //prevent default action 
		console.log($('.cart').offset())
		
		
		
		
		var id = $(this).data('id');
		
		var frm = $('#'+id);
		
		
		var post_url = frm.attr("action"); //get form action url
		var request_method = frm.attr("method"); //get form GET/POST method
		var form_data = frm.serialize(); //Encode form elements for submission
		
		$.ajax({
			url : post_url,
			type: request_method,
			data : form_data
		}).done(function(response){ //
		
		
			var obj = response;//jQuery.parseJSON( response );
			
			$('.skip-cart .count').html(obj.cart_item_cound);
			$('#Growler').html(obj.cart_item);
			$('#Growler').hide();
			$('#Growler').slideDown( "slow", function() {
				// Animation complete.
					setTimeout(function(){
						$('#Growler').slideUp();
					}, 3000);
			});
			ss.hide();
			ss.remove();
		});
        
    });
	
	$('.filter_chk').change(function(){

		var brand =[];
		var categories=[];
		$("input[name='brands[]']:checked").each(function(){
			brand.push($(this).val());
		})

		/*$("input[name='categories[]']:checked").each(function(){
			categories.push($(this).val());
		})*/
		if(brand.length > 0 && categories.length >0){
			window.location = '{{url()->current()}}?brands='+brand.join(',')+'&categories='+categories.join(',');
		}else{
			if(brand.length > 0){
				window.location = '{{url()->current()}}?brands='+brand.join(',');
			}else if(categories.length > 0){
				window.location = '{{url()->current()}}?categories='+categories.join(',');
			}
			else{
				window.location = '{{url()->current()}}';
			}

		}
	})
	$('#select-profession').change(function(){

		var brand =[];
		var categories=[];
		var orderBy = '';
		$("input[name='brands[]']:checked").each(function(){
			brand.push($(this).val());
		})
		

		/*$("input[name='categories[]']:checked").each(function(){
			categories.push($(this).val());
		})*/
		if(brand.length > 0 && categories.length >0){
			if($(this).val() != ''){
				orderBy = '&order_by='+$(this).val();
			}
			window.location = '{{url()->current()}}?brands='+brand.join(',')+'&categories='+categories.join(',')+orderBy;
		}else{
			if(brand.length > 0){
				if($(this).val() != ''){
					orderBy = '&order_by='+$(this).val();
				}
				window.location = '{{url()->current()}}?brands='+brand.join(',')+orderBy;
			}else if(categories.length > 0){
				if($(this).val() != ''){
					orderBy = '&order_by='+$(this).val();
				}
				window.location = '{{url()->current()}}?categories='+categories.join(',')+orderBy;
			}
			else{
				if($(this).val() != ''){
					orderBy = '?order_by='+$(this).val();
				}
				window.location = '{{url()->current()}}'+orderBy;
			}

		}
	})

	
	
});
</script>	
<?php //echo url()->current();?>
<script>
    
    (function($) {

		$('#toggle-search').click(function() {
			$('#search-form, #toggle-search').toggleClass('open');
			return false;
		});

		$('#search-form input[type=submit]').click(function() {
			$('#search-form, #toggle-search').toggleClass('open');
			return true;
		});

		$(document).click(function(event) {
			var target = $(event.target);
      
			if (!target.is('#toggle-search') && !target.closest('#search-form').size()) {
				$('#search-form, #toggle-search').removeClass('open');
			}
		});

})(jQuery);
    
</script>

<script>
    
$('.sel').each(function() {
  $(this).children('select').css('display', 'none');
  
  var $current = $(this);
  
  $(this).find('option').each(function(i) {
    if (i == 0) {
      $current.prepend($('<div>', {
        class: $current.attr('class').replace(/sel/g, 'sel__box')
      }));
      
      var placeholder = $(this).text();
      $current.prepend($('<span>', {
        class: $current.attr('class').replace(/sel/g, 'sel__placeholder'),
        text: placeholder,
        'data-placeholder': placeholder
      }));
      
      return;
    }
    
    $current.children('div').append($('<span>', {
      class: $current.attr('class').replace(/sel/g, 'sel__box__options'),
      text: $(this).text()
    }));
  });
});

$('.sel').click(function() {
  $(this).toggleClass('active');
});

$('.sel__box__options').click(function() {
  var txt = $(this).text();
  var index = $(this).index();
  
  $(this).siblings('.sel__box__options').removeClass('selected');
  $(this).addClass('selected');
  
  var $currentSel = $(this).closest('.sel');
  $currentSel.children('.sel__placeholder').text(txt);
  $currentSel.children('select').prop('selectedIndex', index + 1);
});
    
</script>



<div class="Growler" id="Growler" style="position: fixed; padding: 10px; width: 250px; z-index: 50000; top: 0px; right: 0px;display:none;">
	

</body>
</html>
