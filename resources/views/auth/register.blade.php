@extends('layouts.front.app')

@section('content')
<div class="abt-tp">
  <h2>REGISTER AND BE PART OF US</h2>
</div>

<div class="login-sec">
   <div class="container">
      <div class="col-md-12 no-padding">
         <div class="rgn">
           <div class="col-md-12">
              <div class="login">
                 <form  role="form" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}
                    <div class="form-group clearfix">
						<input type="text" class="form-control" placeholder="First Name" required name="name" value="{{ old('name') }}" autofocus style="border-radius:5px 5px 0 0">
						@if ($errors->has('name'))
							<span class="help-block">
								<strong>{{ $errors->first('name') }}</strong>
							</span>
                        @endif
                    </div>
                    
					<div class="form-group clearfix">
						<input type="text" class="form-control" placeholder="Last Name" required name="lname" value="{{ old('lname') }}">
						@if ($errors->has('lname'))
							<span class="help-block">
								<strong>{{ $errors->first('lname') }}</strong>
							</span>
                        @endif
                    </div>
                    
                    <div class="form-group clearfix">
					<input type="text" class="form-control" placeholder="Your Email" required name="email" value="{{ old('email') }}">
						@if ($errors->has('email'))
							<span class="help-block">
								<strong>{{ $errors->first('email') }}</strong>
							</span>
                        @endif
                    </div>
                    
                    <div class="form-group clearfix">
							<input type="password" class="form-control" placeholder="Password" required  name="password">
                    </div>
                    
                    <div class="form-group clearfix">
						<input type="password" class="form-control" placeholder="Confirm Password" required name="password_confirmation" style="border-radius: 0 0 5px 5px">
                    </div>
                    
                    <button type="submit" class="log">Register</button>
                 </form>
              </div>
           </div>
           
           
           
         </div>
      </div>
   </div>
</div>

@endsection
