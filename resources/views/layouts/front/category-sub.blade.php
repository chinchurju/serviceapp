<ul class="dropdown-menu">
	<div class="col-md-12">
		@foreach($subs as $sub)
				<li><a href="{{route('front.category.slug', $sub->slug)}}">{{$sub->name}}</a></li>
		@endforeach	
		
		@if($category->products1)
			@php $src_url = $category->products1->cover; @endphp
			<li class="img-thumbnail"><a href="{{ route('front.get.product', str_slug($category->products1->slug)) }}"><img src="{{ asset("storage/products/thumb270x300_$src_url") }}" alt="{{ $category->products1->name }}" class="img-responsive">
				{{$category->products1->name}}</a></li>
		@endif
	</div>
</ul>