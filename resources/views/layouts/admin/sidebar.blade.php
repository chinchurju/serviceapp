<!-- =============================================== -->

<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ asset('resources/views/admin/images/admin_profile/1499174950.avatar5.png') }}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{ $user->name }}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">HOME</li>
				<li><a href="{{ route('admin.dashboard', ['reportBase' => 'this_month']) }}"> <i class="fa fa-home"></i> Dashboard</a></li>
				<li><a href="{{ route('admin.pages')}}"> <i class="fa fa-file-text"></i> Pages</a></li>
            <li class="header">SELL</li>
            <li class="treeview @if(request()->segment(2) == 'products' || request()->segment(2) == 'categories' || request()->segment(2) == 'attributes' || request()->segment(2) == 'brands') active @endif">
                <a href="#">
                    <i class="fa fa-gift"></i> <span>Services</span>
                    <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    @if($user->hasPermission('view-product'))<li><a href="{{ route('admin.products.index') }}"><i class="fa fa-circle-o"></i> List Services</a></li>@endif
                    @if($user->hasPermission('create-product'))<li><a href="{{ route('admin.products.create') }}"><i class="fa fa-plus"></i> Create Services</a></li>@endif
                    <li class="@if(request()->segment(2) == 'categories') active @endif">
                    <a href="#">
                        <i class="fa fa-folder"></i> <span>Categories</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{ route('admin.categories.index') }}"><i class="fa fa-circle-o"></i> List categories</a></li>
                        <li><a href="{{ route('admin.categories.create') }}"><i class="fa fa-plus"></i> Create category</a></li>
                    </ul>
                    </li>
                   <!-- <li class="@if(request()->segment(2) == 'attributes') active @endif">
                    <a href="#">
                        <i class="fa fa-gear"></i> <span>Attributes</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{ route('admin.attributes.index') }}"><i class="fa fa-circle-o"></i> List attributes</a></li>
                        <li><a href="{{ route('admin.attributes.create') }}"><i class="fa fa-plus"></i> Create attribute</a></li>
                    </ul>
                    </li>-->
                  <!--  <li class="@if(request()->segment(2) == 'brands') active @endif">
                    <a href="#">
                        <i class="fa fa-tag"></i> <span>Brands</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{ route('admin.brands.index') }}"><i class="fa fa-circle-o"></i> List brands</a></li>
                        <li><a href="{{ route('admin.brands.create') }}"><i class="fa fa-plus"></i> Create brand</a></li>
                    </ul>
                    </li>-->
                </ul>
            </li>
            <li class="treeview @if(request()->segment(2) == 'customers' || request()->segment(2) == 'addresses') active @endif">
                <a href="#">
                    <i class="fa fa-user"></i> <span>Customers</span>
                    <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('admin.customers.index') }}"><i class="fa fa-circle-o"></i> List customers</a></li>
                    <li><a href="{{ route('admin.customers.create') }}"><i class="fa fa-plus"></i> Create customer</a></li>
                    <!--<li class="@if(request()->segment(2) == 'addresses') active @endif">
                        <a href="#"><i class="fa fa-map-marker"></i> Addresses
                            <span class="pull-right-container">
                              <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="{{ route('admin.addresses.index') }}"><i class="fa fa-circle-o"></i> List addresses</a></li>
                            <li><a href="{{ route('admin.addresses.create') }}"><i class="fa fa-plus"></i> Create address</a></li>
                        </ul>
                    </li>-->
                </ul>
            </li>
			<!--<li class="treeview @if(request()->segment(2) == 'reviews') active @endif">
                <a href="{{ route('admin.Reviews') }}">
                    <i class="fa fa-star-half-o"></i> <span>Reviews</span>
                </a>
			</li>-->	
			<li class="treeview @if(request()->segment(2) == 'subscribers') active @endif">
                <a href="{{ route('admin.subscribers') }}">
                    <i class="fa fa-star-half-o"></i> <span>Subscribers</span>
                </a>
			</li>
            <li class="header">SERVICE REQUEST</li>
            <li class="treeview @if(request()->segment(2) == 'orders') active @endif">
                <a href="#">
                    <i class="fa fa-money"></i> <span>Request</span>
                    <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('admin.orders.index') }}"><i class="fa fa-circle-o"></i> List request</a></li>
                </ul>
            </li>
            <!--<li class="treeview @if(request()->segment(2) == 'order-statuses') active @endif">
                <a href="#">
                    <i class="fa fa-anchor"></i> <span>Order Status</span>
                    <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('admin.order-statuses.index') }}"><i class="fa fa-circle-o"></i> List order status</a></li>
                    <li><a href="{{ route('admin.order-statuses.create') }}"><i class="fa fa-plus"></i> Create order status</a></li>
                </ul>
            </li>-->
          <!--  <li class="header">DELIVERY</li>
            <li class="treeview @if(request()->segment(2) == 'couriers') active @endif">
                <a href="#">
                    <i class="fa fa-truck"></i> <span>Shipping Methods</span>
                    <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('admin.couriers.index') }}"><i class="fa fa-circle-o"></i> Shipping Methods</a></li>
                    <li><a href="{{ route('admin.couriers.create') }}"><i class="fa fa-plus"></i> Create Shipping Methods</a></li>
                    <li hidden class="{{ Request::is('admin/listingTaxClass') ? 'active' : '' }} {{ Request::is('admin/addTaxClass') ? 'active' : '' }} {{ Request::is('admin/editTaxClass/*') ? 'active' : '' }} "><a href="{{ URL::to('admin/listingTaxClass')}}"><i class="fa fa-circle-o"></i> {{ trans('labels.link_tax_classes') }}</a></li>
                    <li hidden class="{{ Request::is('admin/listingTaxRates') ? 'active' : '' }} {{ Request::is('admin/addTaxRate') ? 'active' : '' }} {{ Request::is('admin/editTaxRate/*') ? 'active' : '' }} "><a href="{{ URL::to('admin/listingTaxRates')}}"><i class="fa fa-circle-o"></i> {{ trans('labels.link_tax_rates') }}</a></li>
                    <!--<li><a href="listingTaxZones"><i class="fa fa-circle-o"></i> Tax Zones</a></li>-->
                  <!--  <li class="{{ Request::is('admin/listingZones') ? 'active' : '' }} {{ Request::is('admin/addZone') ? 'active' : '' }} {{ Request::is('admin/editZone/*') ? 'active' : '' }}"><a href="{{ URL::to('admin/listingZones')}}"><i class="fa fa-circle-o"></i> {{ trans('labels.link_zones') }}</a></li>

                </ul>
            </li>-->
			<li class="header">REPORTS</li>
            <li class="treeview @if(request()->segment(3) == 'sale') active @endif">
                <a href="{{ route('admin.sale.index') }}"><i class="fa fa-circle-o"></i> Reports <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                    </span></a>

                <ul class="treeview-menu">
                    <li><a href="{{ route('admin.sale.index') }}"><i class="fa fa-circle-o"></i> Services Report</a></li>
          <!--  <li class="{{ Request::is('admin/productsStock') ? 'active' : '' }} "><a href="{{ URL::to('admin/productsStock')}}"><i class="fa fa-circle-o"></i> {{ trans('labels.link_products_stock') }}</a></li>-->
            <li class="{{ Request::is('admin/statsCustomers') ? 'active' : '' }} "><a href="{{ URL::to('admin/statsCustomers')}}"><i class="fa fa-circle-o"></i>Customer Request Total</a></li>
            <li hidden class="{{ Request::is('admin/statsProductsPurchased') ? 'active' : '' }}"><a href="{{ URL::to('admin/statsProductsPurchased')}}"><i class="fa fa-circle-o"></i> {{ trans('labels.link_total_purchased') }}</a></li>
            <li hidden class="{{ Request::is('admin/statsProductsLiked') ? 'active' : '' }}"><a href="{{ URL::to('admin/statsProductsLiked')}}"><i class="fa fa-circle-o"></i> {{ trans('labels.link_products_liked') }}</a></li>
                </ul>
            </li> 
			
            <li class="header">CONFIG</li>
            @if($user->hasRole('admin|superadmin'))
                <li class="treeview @if(request()->segment(2) == 'employees' || request()->segment(2) == 'roles' || request()->segment(2) == 'permissions') active @endif">
            <a href="#">
                <i class="fa fa-star"></i> <span>Users</span>
                <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
                <li><a href="{{ route('admin.employees.index') }}"><i class="fa fa-circle-o"></i> List Users</a></li>
                <li><a href="{{ route('admin.employees.create') }}"><i class="fa fa-plus"></i> Create USer</a></li>
                <li class="@if(request()->segment(2) == 'roles') active @endif">
                    <a href="#">
                        <i class="fa fa-star-o"></i> <span>Roles</span>
                        <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{ route('admin.roles.index') }}"><i class="fa fa-circle-o"></i> List roles</a></li>
                    </ul>
                </li>
            <!--    <li class="@if(request()->segment(2) == 'permissions') active @endif">
                    <a href="#">
                        <i class="fa fa-star-o"></i> <span>Permissions</span>
                        <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{ route('admin.permissions.index') }}"><i class="fa fa-circle-o"></i> List permissions</a></li>
                    </ul>
                </li>-->
            </ul>
        </li>
            @endif
            <li class="treeview @if(request()->segment(2) == 'countries' || request()->segment(2) == 'provinces') active @endif">
                <a href="#">
                    <i class="fa fa-flag"></i> <span>Countries</span>
                    <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('admin.countries.index') }}"><i class="fa fa-circle-o"></i> List</a></li>
                </ul>
            </li>
			<li hidden class="treeview {{ Request::is('admin/listingCountries') ? 'active' : '' }} {{ Request::is('admin/addCountry') ? 'active' : '' }} {{ Request::is('admin/editCountry/*') ? 'active' : '' }} {{ Request::is('admin/listingZones') ? 'active' : '' }} {{ Request::is('admin/addZone') ? 'active' : '' }} {{ Request::is('admin/editZone/*') ? 'active' : '' }} {{ Request::is('admin/listingTaxClass') ? 'active' : '' }} {{ Request::is('admin/addTaxClass') ? 'active' : '' }} {{ Request::is('admin/editTaxClass/*') ? 'active' : '' }} {{ Request::is('admin/listingTaxRates') ? 'active' : '' }} {{ Request::is('admin/addTaxRate') ? 'active' : '' }} {{ Request::is('admin/editTaxRate/*') ? 'active' : '' }}">
                <a href="#">
                    <i class="fa fa-money"></i> <span>Tax / Locations</span>
                    <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                  {{--  <li class="{{ Request::is('admin/listingTaxClass') ? 'active' : '' }} {{ Request::is('admin/addTaxClass') ? 'active' : '' }} {{ Request::is('admin/editTaxClass/*') ? 'active' : '' }} "><a href="{{ URL::to('admin/listingTaxClass')}}"><i class="fa fa-circle-o"></i> {{ trans('labels.link_tax_classes') }}</a></li>
					<li class="{{ Request::is('admin/listingTaxRates') ? 'active' : '' }} {{ Request::is('admin/addTaxRate') ? 'active' : '' }} {{ Request::is('admin/editTaxRate/*') ? 'active' : '' }} "><a href="{{ URL::to('admin/listingTaxRates')}}"><i class="fa fa-circle-o"></i> {{ trans('labels.link_tax_rates') }}</a></li>
					<!--<li><a href="listingTaxZones"><i class="fa fa-circle-o"></i> Tax Zones</a></li>-->
					<li class="{{ Request::is('admin/listingZones') ? 'active' : '' }} {{ Request::is('admin/addZone') ? 'active' : '' }} {{ Request::is('admin/editZone/*') ? 'active' : '' }}"><a href="{{ URL::to('admin/listingZones')}}"><i class="fa fa-circle-o"></i> {{ trans('labels.link_zones') }}</a></li>
            --}}  </ul>
            </li>
			<li class="treeview @if(request()->segment(2) == 'listingBanners'|| request()->segment(2) == 'editBanner' || request()->segment(2) == 'addBanner') active @endif">
                <a href="#">
                    <i class="fa fa-credit-card"></i> <span>Banners</span>
                    <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('admin.listingBanners') }}"><i class="fa fa-circle-o"></i> List</a></li>
                </ul>
            </li>
			<!--<li class="treeview @if(request()->segment(2) == 'block') active @endif">
                <a href="#">
                    <i class="fa fa-credit-card"></i> <span>Blocks</span>
                    <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('admin.block.index') }}"><i class="fa fa-circle-o"></i> List</a></li>
                </ul>
            </li>-->
            <li hidden class="treeview {{ Request::is('admin/statsCustomers') ? 'active' : '' }} {{ Request::is('admin/productsStock') ? 'active' : '' }} {{ Request::is('admin/statsProductsPurchased') ? 'active' : '' }} {{ Request::is('admin/statsProductsLiked') ? 'active' : '' }} ">
                <a href="#">
                    <i class="fa fa-file-text-o" aria-hidden="true"></i>
                    <span>{{ trans('labels.link_reports') }}</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                  </ul>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>

<!-- =============================================== -->