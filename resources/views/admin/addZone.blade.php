@extends('layouts.admin.app')
@section('content')
<!-- Main content -->
    <section class="content">
    <!-- Default box -->
    <div class="box">
        <div class="box-body">
            <h2>{{ trans('labels.AddZone') }}</h2>

          <!-- /.box-header -->
          <div class="box-body">
            <div class="row">
              <div class="col-xs-12">
              	  <div class="box box-info"><br>
                                   
                       	@if(($result['message']))
						
						<div class="alert alert-success alert-dismissible" role="alert">
						  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						 {{ $result['message'] }}
						</div>						
						@endif 
						
                        <!--<div class="box-header with-border">
                          <h3 class="box-title">Edit category</h3>
                        </div>-->
                        <!-- /.box-header -->
                        <!-- form start -->                        
                         <div class="box-body">
                         
                            {!! Form::open(array('url' =>'admin/addNewZone', 'method'=>'post', 'class' => 'form-horizontal form-validate', 'enctype'=>'multipart/form-data')) !!}
                            <div class="form-group">
								<label for="name" class="col-sm-2 col-md-3 control-label">{{ trans('labels.Country') }}
                                </label>
								<div class="col-sm-10 col-md-4">
                                	<select name="zone_country_id" class='form-control'>
                                        @foreach( $result['countries'] as $countries_data)
                                        <option value="{{ $countries_data->id }}"> {{ $countries_data->name }} </option>
                                        @endforeach
                                    </select>
                               		<span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">
                                   {{ trans('labels.ChooseZoneCountry') }}</span>
                                    <span class="help-block hidden">{{ trans('labels.textRequiredFieldMessage') }}</span>
								</div>
							</div>
                            
                            <div class="form-group">
								<label for="name" class="col-sm-2 col-md-3 control-label">{{ trans('labels.ZoneName') }}
                                </label>
								<div class="col-sm-10 col-md-4">
									{!! Form::text('zone_name',  '', array('class'=>'form-control field-validate', 'id'=>'zone_name'))!!}
                                	<span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">
                                   {{ trans('labels.ZoneNameText') }}</span>
                                    <span class="help-block hidden">{{ trans('labels.textRequiredFieldMessage') }}</span>
								</div>
							</div>
							
							<div class="form-group">
								<label for="name" class="col-sm-2 col-md-3 control-label">{{ trans('labels.ZoneCode') }}
                                </label>
								<div class="col-sm-10 col-md-4">
									{!! Form::text('zone_code',  '', array('class'=>'form-control  field-validate', 'id'=>'zone_code'))!!}
                                    <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">
                                    {{ trans('labels.ZoneCodeText') }}</span>
                                    <span class="help-block hidden">{{ trans('labels.textRequiredFieldMessage') }}</span>
								</div>
							</div>
							
							
							<!-- /.box-body -->
							<div class="box-footer text-center">
								<button type="submit" class="btn btn-primary">{{ trans('labels.AddZone') }}</button>
								<a href="listingZones" type="button" class="btn btn-default">{{ trans('labels.back') }}</a>
							</div>
                              <!-- /.box-footer -->
                            {!! Form::close() !!}
                        </div>
                  </div>
              </div>
            </div>
            
          </div>
          
       
    
    <!-- Main row --> 
    
    <!-- /.row --> 
	 </div>
    </div>
  </section>
  <!-- /.content --> 
</div>
@endsection 