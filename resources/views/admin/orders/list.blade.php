@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content">

    @include('layouts.errors-and-messages')
    <!-- Default box -->
        @if($orders)
            <div class="box">
                <div class="box-body">
                    <h2>Request</h2>
                    @include('layouts.search', ['route' => route('admin.orders.index')])
                    <table id="example" class="table table-striped table-bordered dataTable" style="width:100%">
                        <thead>
                            <tr>
								<th class=" @if(request()->has('order_by')) @if(request()->input('order_by') == 'id') @if(request()->input('order') == 'asc') sorting_asc @else sorting_desc @endif @else sorting @endif @else sorting @endif" data-orderby="id">ID</th>
								<th class="">Service</th>
								<th class="">Customer</th>
								<th class="">Customer Type</th>
                                <th class=" @if(request()->has('order_by')) @if(request()->input('order_by') == 'created_at') @if(request()->input('order') == 'asc') sorting_asc @else sorting_desc @endif @else sorting @endif @else sorting @endif" data-orderby="created_at">Date</th>
                                <th class=" @if(request()->has('order_by')) @if(request()->input('order_by') == 'servicedate') @if(request()->input('order') == 'asc') sorting_asc @else sorting_desc @endif @else sorting @endif @else sorting @endif" data-orderby="servicedate">Servicedate</th>
                                <th class="2 @if(request()->has('order_by')) @if(request()->input('order_by') == 'total') @if(request()->input('order') == 'asc') sorting_asc @else sorting_desc @endif @else sorting @endif @else sorting @endif" data-orderby="total">Total</th>
                                <th class=" @if(request()->has('order_by')) @if(request()->input('order_by') == 'order_status_id') @if(request()->input('order') == 'asc') sorting_asc @else sorting_desc @endif @else sorting @endif @else sorting @endif" data-orderby="order_status_id">Status</th>
                           	   <!-- <th class="" width="30%">Summary</th>-->
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($orders as $order)
                            <tr>
								<td><a title="Show order" href="{{ route('admin.orders.show', $order->id) }}">{{ $order->reference }}</a></td>
								<td><a title="Show order" href="{{ route('admin.orders.show', $order->id) }}">{{ $order->service_name }}</a></td>
								<td><a title="Show order" href="{{ route('admin.orders.show', $order->id) }}">{{ $order->customer->name }}</a></td>
								<td><a title="Show order" href="{{ route('admin.orders.show', $order->id) }}">{{ $order->customer->customertype }}</a></td>
                                <td><a title="Show order" href="{{ route('admin.orders.show', $order->id) }}">{{ date('M d, Y h:i a', strtotime($order->created_at)) }}</a></td>
                                <td><a title="Show order" href="{{ route('admin.orders.show', $order->id) }}">{{ date('M d, Y h:i a', strtotime($order->servicedate)) }}</a></td>
                                <td>
                                    <span class="label @if($order->total != $order->total_paid) label-danger @else label-success @endif">{{ config('cart.currency') }} {{ $order->total }}</span>
                                </td>
                                <td><p class="text-center" style="color: #ffffff; background-color: {{ $order->status->color }}">{{ $order->status->name }}</p></td>
                               <!-- <td><p class="text-center" >{{ $order->summary }}</p></td>-->
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    {{ $orders->links() }}
                </div>
            </div>
            <!-- /.box -->
        @endif

    </section>
    <!-- /.content -->
@endsection

@section('js')
<link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css" media="all" rel="stylesheet" type="text/css" />

<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script>
$(document).ready(function() {
    $('.dataTable th').click(function(){
        var sort = 'asc';
        if($(this).hasClass('sorting_asc')){
            var sort = 'desc';
        }
        window.location="{{route('admin.orders.index')}}?order_by="+$(this).data('orderby')+"&order="+sort;
    } );
} );   
</script>

@endsection