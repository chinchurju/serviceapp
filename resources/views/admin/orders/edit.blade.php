@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content">
    @include('layouts.errors-and-messages')
    <!-- Default box -->
        <div class="box">
            <div class="box-header">
                <div class="row">
                    <div class="col-md-6">
                        <h2>
                            <a href="{{ route('admin.customers.show', $customer->id) }}">{{$customer->name}}</a> <br />
                            <small>{{$customer->email}}</small> <br />
                            <small>Request ID: <strong>{{$order->reference}}</strong></small>
                        </h2>
                    </div>
                    <div class="col-md-3 col-md-offset-3">
                        <h2><a href="{{route('admin.orders.invoice.generate', $order['id'])}}" class="btn btn-primary btn-block">Download Invoice</a></h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="box">
            <div class="box-body">
                <h4> <i class="fa fa-shopping-bag"></i> Request Information</h4>
                <table class="table">
                    <thead>
                        <tr>
                            <td class="col-md-3">Date</td>
                            <td class="col-md-3">Customer</td>
                            <td class="col-md-3">Payment</td>
                            <td class="col-md-3">Status</td>
                        </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>{{ date('M d, Y h:i a', strtotime($order['created_at'])) }}</td>
                        <td><a href="{{ route('admin.customers.show', $customer->id) }}">{{ $customer->name }}</a></td>
                        <td><strong>{{ $order['payment'] }}</strong></td>
                        <td>
                            <form action="{{ route('admin.orders.update', $order->id) }}" method="post">
                                {{ csrf_field() }}
                                <input type="hidden" name="_method" value="put">
                                <label for="order_status_id" class="hidden">Update status</label>
                                <input type="text" name="total_paid" class="form-control" placeholder="Total paid" style="margin-bottom: 5px; display: none" value="{{ old('total_paid') ?? $order->total_paid }}" />
                                <div class="input-group">
                                    <select name="order_status_id" id="order_status_id" class="form-control select2">
                                        @foreach($statuses as $status)
                                            <option @if($currentStatus->id == $status->id) selected="selected" @endif value="{{ $status->id }}">{{ $status->name }}</option>
                                        @endforeach
                                    </select>
                                    <span class="input-group-btn"><button onclick="return confirm('Are you sure?')" type="submit" class="btn btn-primary">Update</button></span>
                                </div>
                            </form>
                        </td>
                    </tr>
                    </tbody>
                    <tbody>
                  <!--  <tr>
                        <td></td>
                        <td></td>
                        <td class="bg-warning">Subtotal</td>
                        <td class="bg-warning">{{ $order['total_products'] }}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td class="bg-warning">{{ $order['courier'] }}</td>
                        <td class="bg-warning">{{ $order['shipping_charge'] }}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td class="bg-success text-bold">Order Total</td>
                        <td class="bg-success text-bold">{{ $order['total'] }}</td>
                    </tr>-->
                     @if($order['total_paid'] != $order['total'])
                        <tr>
                            <td></td>
                            <td></td>
                            <td class="bg-danger text-bold">Service Total</td>
                            @if($order->country_id !=174)
                            <td class="bg-danger text-bold">USD {{ $order['total']*(env('USD_RATE')) }}</td>
                            @else
                            <td class="bg-danger text-bold">QAR {{ $order['total'] }}</td>
                            @endif
                        </tr>
                    @endif
                    </tbody>
                </table>
            </div>
            <!-- /.box-body -->
        </div>
        @if($order)
           
            <div class="box">
                @if(!$items->isEmpty())
                    <div class="box-body">
                        <h4> <i class="fa fa-gift"></i> Services</h4>
                        <table class="table">
                            <thead>
                           <!-- <th class="col-md-2">SKU</th>-->
                            <th class="col-md-2">Services</th>
                           <!-- <th class="col-md-2">Description</th>
                            <th class="col-md-2">Quantity</th>-->
                            <th class="col-md-2">Price</th>
                            </thead>
                            <tbody>
                           @foreach($items as $item)
                                <tr>
                               <!--    <td>{{ $item->sku }}</td>-->
                                    <td>{{ $item->name }}</td>
                                  <!--  <td>{!! $item->description !!}</td>
                                    <td>{{ $item->pivot->quantity }}</td>-->
                                    @if($order->country_id !=174)
                                    
                                    <td>USD {{ $item->price*(env('USD_RATE')) }}</td>
                                    @else
                                    <td>QAR {{ $item->price }}</td>
                                    @endif
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                @endif
				
				<div class="box-body">
                    <table class="table">
                        <thead>
                            <th class="col-md-2">Date</th>
                            <th class="col-md-2">Time</th>
                            <th class="col-md-2">Description</th>
                        </thead>
						<tbody>
							<tr>
								<td>{{ $order->servicedate }}</td>
								<td>{{ $order->servicetime }}</td>
								<td>{{ $order->description }}</td>
								
							</tr>
						</tbody>
                    </table>
                </div>
				
 <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <h4> <i class="fa fa-shopping-bag"></i> Attachments</h4>
                            @foreach($order->attachment as $attach)
                                <td>
                                    @if($attach->type == 'image')
                                        <a href="{{asset("storage/app/$attach->images") }}" target="_blank" class="imggg group fancybox"  rel="group"> <img src="{{ asset("storage/app/$attach->images") }}" alt="" class="img-responsive"></a>

                                        @elseif($attach->type == 'document')
                                        <a href="{{asset("storage/app/$attach->images") }}" target="_blank" class="imggg"> <img src="{{ asset("storage /app/$attach->images") }}" alt="" class="img-responsive">Document</a>

                                        @else
                                        <a href="{{asset("storage/app/$attach->images") }}" target="_blank" class="imggg group fancybox"  rel="group"> <img src="{{ asset("storage/app/$attach->images") }}" alt="" class="img-responsive">Video</a>
                                    @endif
                                </td>
                            @endforeach
                        </div>
                    </div>
                </div>
                
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <h4> <i class="fa fa-map-marker"></i> Location</h4>
                            <table class="table">
                               
                                <tbody>
                                <tr>
                                    
                                      <td width="40%">
                                            <div id="map" style="width:100%;height:250px"></div><br/>
                                            <div class="sharethis-inline-share-buttons"></div>
                                        </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.box -->
            <div class="box-footer">
                <div class="btn-group">
                    <a href="{{ route('admin.orders.show', $order->id) }}" class="btn btn-default">Back</a>
                </div>
            </div>
        @endif

    </section>
    <!-- /.content -->
@endsection
@section('js')
<style>
.imggg img{
    max-width:10%;
    display: inline-block;
}
</style>
<link href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        $(document).ready(function () {
            let osElement = $('#order_status_id');
            osElement.change(function () {
                if (+$(this).val() === 1) {
                    $('input[name="total_paid"]').fadeIn();
                } else {
                    $('input[name="total_paid"]').fadeOut();
                }
            });
        })
    </script>
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyBXqOs4tDhl2cuTeIS63KRS78RGhNQYMy0"></script>

<script type="text/javascript">
    function initMap() {
       // alert('sssssssssss');
        var myLatLng = {lat: {{ $order->location_latitude }}, lng: {{ $order->location_longitude }}};

        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 4,
          center: myLatLng
        });

        var marker = new google.maps.Marker({
          position: myLatLng,
          map: map
          //title: 'Hello World!'
        });
    }
    initMap();
</script>
    <script type="text/javascript">
$(document).ready(function() {
$("a.group").fancybox();
	
});
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.js">
    </script>
@endsection