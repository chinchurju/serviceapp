@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content">
        @include('layouts.errors-and-messages')
        <?php //print_r($customer->toArray());?>
        <div class="box">
            <form action="{{ route('admin.customers.update', $customer->id) }}" method="post" class="form">
                <div class="box-body">
                    {{ csrf_field() }}
                    <input type="hidden" name="_method" value="put">
                    <div class="form-group">
                        <label for="name">First Name <span class="text-danger">*</span></label>
                        <input type="text" name="name" id="name" placeholder="First Name" class="form-control" value="{!! $customer->name ?: old('name')  !!}">
                    </div>
					<div class="form-group">
                        <label for="name">Last Name <span class="text-danger">*</span></label>
                        <input type="text" name="lname" id="lname" placeholder="Last Name" class="form-control" value="{!! $customer->lname ?: old('lname')  !!}">
                    </div>
                    <div class="form-group">
                        <label for="email">Email <span class="text-danger">*</span></label>
                        <div class="input-group">
                            <span class="input-group-addon">@</span>
                            <input type="text" name="email" id="email" placeholder="Email" class="form-control" value="{!! $customer->email ?: old('email')  !!}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email">Phone <span class="text-danger">*</span></label>
                        <input type="text" name="phone" id="phone" placeholder="Phone" class="form-control" value="{!! $customer->phonephone ?: old('phone')  !!}">
                    </div>
                    <div class="form-group">
                        <label for="password">Password <span class="text-danger">*</span></label>
                        <input type="password" name="password" id="password" placeholder="xxxxx" class="form-control">
                    </div>
                    
                     <div class="form-group">
                        <label for="type">Customer Type </label>
                        <select name="customertype" id="customertype" class="form-control">
                            <option value="Individual" @if($customer->customertype == 'Individual') selected="selected" @endif>Individual</option>
                            <option value="Business" @if($customer->customertype == 'Business') selected="selected" @endif>Business</option>
                        </select>
                    </div>


                    <div class="form-group">
                        <label for="country_id">Countries</label>
                        <select name="country_id" id="country_id" class="form-control select2">
                            <option value=""></option>
                            @foreach($countries as $country)
                                <option @if($country->id == $customer->country_id) selected="selected" @endif value="{{ $country->id }}">{{ $country->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="status">Status </label>
                        <select name="status" id="status" class="form-control">
                            <option value="0" @if($customer->status == 0) selected="selected" @endif>Disable</option>
                            <option value="1" @if($customer->status == 1) selected="selected" @endif>Enable</option>
                        </select>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <div class="btn-group">
                        <a href="{{ route('admin.customers.index') }}" class="btn btn-default btn-sm">Back</a>
                        <button type="submit" class="btn btn-primary btn-sm">Update</button>
                    </div>
                </div>
            </form>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
@endsection
