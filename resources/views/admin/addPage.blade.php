@extends('layouts.admin.app')
@section('content')
 <section class="content">
   
    <!-- Default box -->
    <div class="box">
        <div class="box-body">
            <h2>Add Page</h2>
          <!-- /.box-header -->
          <div class="box-body">
		<div class="row">
      <div class="col-md-12">
        <div class="box">

          
          <!-- /.box-header -->
          <div class="box-body">
            <div class="row">
              <div class="col-xs-12">
              		<div class="box box-info">
                        <!-- /.box-header -->
                        <!-- form start -->                        
                         <div class="box-body">
                          @if( count($errors) > 0)
                            @foreach($errors->all() as $error)
                                <div class="alert alert-success" role="alert">
                                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                    <span class="sr-only">Error:</span>
                                    {{ $error }}
                                </div>
                             @endforeach
                          @endif
                        
                            {!! Form::open(array('url' =>'admin/addNewPage', 'method'=>'post', 'class' => 'form-horizontal form-validate', 'enctype'=>'multipart/form-data')) !!}
                            
                             <div class="form-group">
							  <label for="name" class="col-sm-2 col-md-3 control-label">{{ trans('labels.PageSlug') }}</label>
							  <div class="col-sm-10 col-md-4">
								{!! Form::text('slug',  '', array('class'=>'form-control field-validate', 'id'=>'slug')) !!}
								<span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.pageSlugWithDashesText') }}</span>
							  </div>
							 </div>
                             <div class="form-group">
                                      <label for="name" class="col-sm-2 col-md-3 control-label">{{ trans('labels.PageName') }}</label>
                                      <div class="col-sm-10 col-md-4">
										<input type="text" name="name_1" class="form-control field-validate">
										<span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.PageName') }}</span>

										<span class="help-block hidden">{{ trans('labels.textRequiredFieldMessage') }}</span>
                                      </div>
                             </div>





                    <div class="form-group">
                      <label for="name" class="col-sm-2 col-md-3 control-label">Page Name Arabic</label>
                      <div class="col-sm-10 col-md-4">
                    <input type="text" name="name_ar" class="form-control field-validate">

                                      </div>
                             </div>

                                
                             <div class="form-group">
                                      <label for="name" class="col-sm-2 col-md-3 control-label">{{ trans('labels.Description') }} </label>
                                      <div class="col-sm-10 col-md-8">
                                        <textarea id="editor1" name="description_1" class="summernote" rows="10" cols="80">    	  
                                        </textarea>
                                        <span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.Description') }}</span>
                                     
                                        <br>
                                      </div>
                             </div> 



                          <div class="form-group">
                                      <label for="name" class="col-sm-2 col-md-3 control-label">Description Arabic</label>
                                      <div class="col-sm-10 col-md-8">
                                        <textarea id="editor1" name="description_ar" class="summernote" rows="10" cols="80">       
                                        </textarea>
                                       
                                        <br>
                                      </div>
                             </div> 



                              <!-- /.box-body -->
							<div class="box-footer text-center">
								<button type="submit" class="btn btn-primary">{{ trans('labels.AddPage') }}</button>
								<a href="{{ URL::to('admin/listingPages')}}" type="button" class="btn btn-default">{{ trans('labels.back') }}</a>
							</div>
                              
                              <!-- /.box-footer -->
                            {!! Form::close() !!}
                        </div>
                  </div>
              </div>
            </div>
            
          </div>
          <!-- /.box-body --> 
        </div>
        <!-- /.box --> 
      </div>
      <!-- /.col --> 
    </div>
    <!-- /.row --> 
    </div>
    <!-- Main row --> 
    </div> 
	
	</div>
    <!-- /.row --> 
  </section>
  <!-- /.content --> 

 @endsection 
  @section('js')
<script src="//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.min.js"></script>
<script type="text/javascript">
		$(function () {
			
			//for multiple languages
				// Replace the <textarea id="editor1"> with a CKEditor
				// instance, using default configuration.
			//CKEDITOR.replace('editor1');
			
			$("textarea").summernote({height: "400px"});
			//bootstrap WYSIHTML5 - text editor
			//$(".textarea").wysihtml5();
			
    });
</script>

@endsection 