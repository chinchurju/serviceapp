@extends('layouts.admin.app')
@section('content')
 <section class="content">
   
    <!-- Default box -->
    <div class="box">
        <div class="box-body">
            <h2>Add Tax Class</h2>
          <!-- /.box-header -->
    
          <!-- /.box-header -->
          <div class="box-body">
          <div class="row">
              <div class="col-xs-12">              		
				  @if (count($errors) > 0)
					  @if($errors->any())
						<div class="alert alert-success alert-dismissible" role="alert">
						  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						  {{$errors->first()}}
						</div>
					  @endif
				  @endif
              </div>
            </div>
            <div class="row">
              <div class="col-xs-12">
              	  <div class="box box-info"><br>
                                   
                       	@if(count($result['message'])>0)
						<div class="alert alert-success alert-dismissible" role="alert">
						  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						 {{ $result['message'] }}
						</div>						
						@endif 
						
                        <!--<div class="box-header with-border">
                          <h3 class="box-title">Edit category</h3>
                        </div>-->
                        <!-- /.box-header -->
                        <!-- form start -->                        
                         <div class="box-body">
                         
                            {!! Form::open(array('url' =>'admin/addNewTaxClass', 'method'=>'post', 'class' => 'form-horizontal form-validate', 'enctype'=>'multipart/form-data')) !!}
                            
                            <div class="form-group">
								<label for="name" class="col-sm-2 col-md-3 control-label">{{ trans('labels.Title') }}</label>
								<div class="col-sm-10 col-md-4">
									{!! Form::text('tax_class_title',  '', array('class'=>'form-control field-validate', 'id'=>'tax_class_title'))!!}
                                	<span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">
                                   {{ trans('labels.TaxClassTitleText') }}</span>
                                    <span class="help-block hidden">{{ trans('labels.textRequiredFieldMessage') }}</span>
								</div>
							</div>
							
							<div class="form-group">
								<label for="name" class="col-sm-2 col-md-3 control-label">{{ trans('labels.Description') }}
                                </label>
								<div class="col-sm-10 col-md-4">
									{!! Form::textarea('tax_class_description',  '', array('class'=>'form-control', 'id'=>'tax_class_description'))!!}
                                	<span class="help-block" style="font-weight: normal;font-size: 11px;margin-bottom: 0;">{{ trans('labels.TaxClassDescriptionText') }}</span>
								</div>
							</div>							
							<!-- /.box-body -->
							<div class="box-footer text-center">
								<button type="submit" class="btn btn-primary">{{ trans('labels.AddTaxClass') }}</button>
								<a href="listingTaxClass" type="button" class="btn btn-default">{{ trans('labels.back') }}</a>
							</div>
                              <!-- /.box-footer -->
                            {!! Form::close() !!}
                        </div>
                  </div>
              </div>
            </div>
            
          </div>
          
          
      
    
    <!-- Main row --> 
    </div>
	</div>

    <!-- /.row --> 
  </section>
  <!-- /.content --> 
@endsection 