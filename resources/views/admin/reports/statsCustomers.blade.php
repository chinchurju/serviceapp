@extends('layouts.admin.app')

@section('content')

  <!-- Main content -->
  <section class="content"> 
    <!-- Info boxes --> 
    
    <!-- /.row -->

    <div class="row">
      <div class="col-md-12">
        <div class="box">
            <h2 class="box-title">Customer Request Total </h2>
          
          <!-- /.box-header -->
          <div class="box-body">
           
            <div class="row">
              <div class="col-xs-12">
              		
              </div>
            </div>
            <div class="row">
              <div class="col-xs-12">
                <table id="t1" class="table table-bordered table-responsive table-striped table-hover js-basic-example dataTable" style="width: 100%">
                  <thead>
                    <tr>
                      <th>{{ trans('labels.No') }}.</th>
                      <th>{{ trans('labels.CustomerName') }}</th>
                      <th>Total Request Amount</th>
                      <th>{{ trans('labels.Edit') }}</th>
                    </tr>
                  </thead>
                  <tbody>
                  @if(count($result['data'])>0)
                    @foreach ($result['data'] as $key=>$orderData)
                        <tr>
                            <td>{{ ++$key }}</td>
                            <td> {{ $orderData->name }}</td>
                            <td>QAR{{ $orderData->orders()->sum('total') }}</td>
                            <td><a href="{{ URL::to('admin/customers/')}}/{{$orderData->id}}/edit" class="badge bg-light-blue"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></td>
                        </tr>
                    @endforeach
                  @else
                  	<tr>
                    	<td colspan="6"><strong>{{ trans('labels.NoRecordFound') }}</strong></td>
                    </tr>
                  @endif
                  </tbody>
                </table>
                <div class="col-xs-12 text-right">
                	{{$result['data']->links()}}
                </div>
              </div>
            </div>
          </div>
          <!-- /.box-body --> 
        </div>
        <!-- /.box --> 
      </div>
      <!-- /.col --> 
    </div>
    <!-- /.row --> 
    
    <!-- Main row --> 
    
    <!-- /.row --> 
  </section>
  <!-- /.content --> 

@endsection 