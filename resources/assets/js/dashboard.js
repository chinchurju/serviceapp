/* ------------------------------------------------------------------------------
 *
 *  # Dashboard configuration
 *
 *  Demo dashboard configuration. Contains charts and plugin inits
 *
 * ---------------------------------------------------------------------------- */

document.addEventListener('DOMContentLoaded', function() {


    var area_values_element = document.getElementById('simple');
    // Display point values
    if (area_values_element) {

        // Initialize chart
        var area_values = echarts.init(area_values_element);


        //
        // Chart config
        //

        // Options
        $.ajax({
            type: 'GET',
            url: "../getOrderData",
            dataType: 'json',
            success: function (data) {

                area_values.setOption({

                    // Define colors
                    color: ['#EC407A'],

                    // Global text styles
                    textStyle: {
                        fontFamily: 'Roboto, Arial, Verdana, sans-serif',
                        fontSize: 13
                    },

                    // Chart animation duration
                    animationDuration: 750,

                    // Setup grid
                    grid: {
                        left: 0,
                        right: 40,
                        top: 10,
                        bottom: 0,
                        containLabel: true
                    },

                    // Add tooltip
                    tooltip: {
                        trigger: 'axis',
                        backgroundColor: 'rgba(0,0,0,0.75)',
                        padding: [10, 15],
                        textStyle: {
                            fontSize: 13,
                            fontFamily: 'Roboto, sans-serif'
                        }
                    },


                    // Horizontal axis
                    xAxis: [{
                        type: 'category',
                        boundaryGap: false,
                        data: data.dates.reverse(),
                        axisLabel: {
                            color: '#333'
                        },
                        axisLine: {
                            lineStyle: {
                                color: '#999'
                            }
                        },
                        splitLine: {
                            lineStyle: {
                                color: '#eee'
                            }
                        }
                    }],

                    // Vertical axis
                    yAxis: [{
                        name: 'Visits',
                        type: 'value',
                        axisLabel: {
                            formatter: '{value}',
                            color: '#333'
                        },
                        axisLine: {
                            lineStyle: {
                                color: '#999'
                            }
                        },
                        splitLine: {
                            lineStyle: {
                                color: '#eee'
                            }
                        },
                        splitArea: {
                            show: true,
                            areaStyle: {
                                color: ['rgba(250,250,250,0.1)', 'rgba(0,0,0,0.01)']
                            }
                        }
                    }],

                    // Add series
                    series: [
                        {
                            name: '',
                            type: 'line',
                            data: data.data.reverse(),
                            smooth: true,
                            symbolSize: 7,
                            label: {
                                normal: {
                                    show: true
                                }
                            },
                            areaStyle: {
                                normal: {
                                    opacity: 0.25
                                }
                            },
                            itemStyle: {
                                normal: {
                                    borderWidth: 2
                                }
                            }
                        }
                    ]
                });
            }})
    }

});