-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Oct 22, 2019 at 05:06 AM
-- Server version: 5.7.21
-- PHP Version: 7.1.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `service`
--

-- --------------------------------------------------------

--
-- Table structure for table `addresses`
--

DROP TABLE IF EXISTS `addresses`;
CREATE TABLE IF NOT EXISTS `addresses` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `alias` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address_1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address_2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `province_id` int(11) DEFAULT NULL,
  `country_id` int(10) UNSIGNED DEFAULT NULL,
  `customer_id` int(10) UNSIGNED DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `phone` int(15) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `location_latitude` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location_longitude` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_default` int(5) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `addresses_country_id_index` (`country_id`),
  KEY `addresses_customer_id_index` (`customer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `addresses`
--

INSERT INTO `addresses` (`id`, `alias`, `address_1`, `address_2`, `zip`, `state_code`, `city`, `province_id`, `country_id`, `customer_id`, `status`, `phone`, `deleted_at`, `created_at`, `updated_at`, `location_latitude`, `location_longitude`, `is_default`) VALUES
(11, 'zx', 'test', 'test', '123456', '', 'fsdf', 5, 174, 15, 1, NULL, '2018-11-01 22:56:56', '2018-11-01 21:18:18', '2018-11-01 22:56:56', NULL, NULL, NULL),
(12, 'SANIL', 'test addr', 'test addr 2', '1256', NULL, NULL, NULL, 174, 15, 1, NULL, NULL, '2018-11-01 22:56:47', '2019-02-07 00:31:34', '10.850516', '76.271080', 0),
(13, 'Home', 'test addr', 'test', '123456', NULL, NULL, NULL, 174, 15, 1, NULL, NULL, '2018-11-13 00:59:59', '2019-02-07 00:31:34', NULL, NULL, 0),
(14, 'Home1', 'ssss', 'sss', '123456789', NULL, NULL, 7, 174, 15, 1, NULL, NULL, '2018-11-18 20:27:01', '2019-02-07 00:31:34', NULL, NULL, 0),
(19, 'Billing', 'dfgdgdgdfg', 'dfgdfg', NULL, NULL, NULL, 1, 174, 15, 1, NULL, '2018-11-26 02:35:57', '2018-11-19 01:41:36', '2018-11-26 02:35:57', NULL, NULL, NULL),
(20, 'Billing', 'asdasdsd', 'asdasd', NULL, NULL, NULL, 1, 174, 15, 1, NULL, NULL, '2018-11-19 01:42:08', '2019-02-07 00:31:34', NULL, NULL, 0),
(21, 'Billing', 'dfgdfgdg', 'dfgdfg', NULL, NULL, NULL, 1, 174, 15, 1, NULL, NULL, '2018-11-19 01:44:54', '2019-02-07 00:31:34', NULL, NULL, 0),
(22, 'Billing', 'dfffffffffg', 'dgdfgdfg', '123456', NULL, NULL, 4, 174, 15, 1, NULL, NULL, '2018-11-19 01:54:52', '2019-02-07 00:31:34', NULL, NULL, 0),
(23, 'Billing', 'sdfsdf', 'sdfsdf', '123456', NULL, NULL, 3, 174, 15, 1, NULL, NULL, '2018-11-19 01:57:02', '2019-02-07 00:31:34', NULL, NULL, 0),
(24, 'Billing', 'sdfsdfsdasdasdasdasd', 'sdfsdf', '123456', NULL, NULL, 3, 174, 15, 1, NULL, '2018-11-26 02:35:14', '2018-11-19 01:57:35', '2018-11-26 02:35:14', NULL, NULL, NULL),
(25, 'Billing', 'sdfsdfsdasdasdasdasdddasdasd', 'sdfsdf', '123456', NULL, NULL, 3, 174, 15, 1, NULL, NULL, '2018-11-19 01:57:58', '2019-02-07 00:31:34', NULL, NULL, 0),
(26, 'Billing', 'ghjghjgj', 'ghjghj', '1256', NULL, NULL, 5, 174, 15, 1, NULL, NULL, '2018-11-19 02:02:17', '2019-02-07 00:31:34', NULL, NULL, 0),
(27, 'Billing', 'fghfghfg', 'hfghfgh', '123456', NULL, NULL, 5, 174, 15, 1, NULL, '2018-11-26 02:37:04', '2018-11-19 02:05:19', '2018-11-26 02:37:04', NULL, NULL, NULL),
(28, 'test', 'xdvdcgd', 'fbcf', '123456', NULL, 'sdsd', 1, 174, 27, 1, 123456789, NULL, '2018-12-11 01:41:55', '2018-12-11 01:41:55', '10.850516', '76.271080', NULL),
(29, 'opl', 'oopl', 'oopl', NULL, NULL, NULL, 7, 174, 28, 1, NULL, NULL, '2018-12-14 05:26:57', '2018-12-14 05:26:57', NULL, NULL, NULL),
(30, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '2019-01-04 01:18:37', '2019-01-04 01:18:37', '9.9312', '76.2673', NULL),
(31, NULL, 'test address', NULL, NULL, NULL, NULL, NULL, NULL, 42, 1, NULL, NULL, '2019-02-14 23:45:21', '2019-02-14 23:45:21', '10.850516', '76.271080', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `attributes`
--

DROP TABLE IF EXISTS `attributes`;
CREATE TABLE IF NOT EXISTS `attributes` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_searchable` enum('1','0') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `is_configurable` enum('1','0') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `attributes_name_unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `attribute_values`
--

DROP TABLE IF EXISTS `attribute_values`;
CREATE TABLE IF NOT EXISTS `attribute_values` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `value` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `attribute_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `attribute_values_attribute_id_foreign` (`attribute_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `attribute_value_product_attribute`
--

DROP TABLE IF EXISTS `attribute_value_product_attribute`;
CREATE TABLE IF NOT EXISTS `attribute_value_product_attribute` (
  `attribute_value_id` int(10) UNSIGNED NOT NULL,
  `product_attribute_id` int(10) UNSIGNED NOT NULL,
  KEY `attribute_value_product_attribute_attribute_value_id_foreign` (`attribute_value_id`),
  KEY `attribute_value_product_attribute_product_attribute_id_foreign` (`product_attribute_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

DROP TABLE IF EXISTS `banners`;
CREATE TABLE IF NOT EXISTS `banners` (
  `banners_id` int(11) NOT NULL AUTO_INCREMENT,
  `banners_title` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `banners_desc` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `banners_link_text` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `banners_link` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `banners_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `banners_image` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `banners_group` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `banners_html_text` mediumtext COLLATE utf8_unicode_ci,
  `expires_impressions` int(7) DEFAULT '0',
  `expires_date` datetime DEFAULT NULL,
  `date_scheduled` datetime DEFAULT NULL,
  `date_added` datetime NOT NULL,
  `date_status_change` datetime DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `type` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'home1',
  PRIMARY KEY (`banners_id`),
  KEY `idx_banners_group` (`banners_group`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `banners`
--

INSERT INTO `banners` (`banners_id`, `banners_title`, `banners_desc`, `banners_link_text`, `banners_link`, `banners_url`, `banners_image`, `banners_group`, `banners_html_text`, `expires_impressions`, `expires_date`, `date_scheduled`, `date_added`, `date_status_change`, `status`, `type`) VALUES
(9, 'Get your Dream Hair', 'Get longer, thicker hair instantly with our high quality clip-in hair extensions. Are you ready for your transformation?', NULL, '#', '', 'resources/assets/images/banner_images/1540989553.1.jpg', NULL, NULL, 0, '2019-05-03 00:00:00', NULL, '2018-10-31 12:39:13', '2018-10-31 12:39:13', 1, 'home1'),
(10, 'IN JUST MINUTES YOU CAN Get your Dream Hair.', 'Get longer, thicker hair instantly with our high quality clip-in hair extensions. Are you ready for your transformation?', NULL, '#', '', 'resources/assets/images/banner_images/1540989566.1.jpg', NULL, NULL, 0, '2019-02-28 00:00:00', NULL, '2018-10-31 12:39:26', '2018-10-31 12:39:26', 1, 'home1'),
(12, NULL, NULL, NULL, NULL, '', 'resources/assets/images/banner_images/1543477808.banner-image.jpg', NULL, NULL, 0, '2018-12-31 00:00:00', NULL, '2018-11-29 07:50:08', NULL, 1, 'home1');

-- --------------------------------------------------------

--
-- Table structure for table `banners_history`
--

DROP TABLE IF EXISTS `banners_history`;
CREATE TABLE IF NOT EXISTS `banners_history` (
  `banners_history_id` int(11) NOT NULL AUTO_INCREMENT,
  `banners_id` int(11) NOT NULL,
  `banners_shown` int(5) NOT NULL DEFAULT '0',
  `banners_clicked` int(5) NOT NULL DEFAULT '0',
  `banners_history_date` datetime NOT NULL,
  PRIMARY KEY (`banners_history_id`),
  KEY `idx_banners_history_banners_id` (`banners_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `blocks`
--

DROP TABLE IF EXISTS `blocks`;
CREATE TABLE IF NOT EXISTS `blocks` (
  `block_id` int(11) NOT NULL AUTO_INCREMENT,
  `block_title` varchar(250) DEFAULT NULL,
  `block_content` text,
  `display_order` int(11) DEFAULT '1',
  `status` enum('0','1') DEFAULT '1',
  PRIMARY KEY (`block_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blocks`
--

INSERT INTO `blocks` (`block_id`, `block_title`, `block_content`, `display_order`, `status`) VALUES
(1, '<span> MADE THE HARD WAY </span><br> FEATURED CATEGORIES', '<div class=\"col-md-4 col-sm-4\">\r\n				 <figure><a href=\"#\"><img src=\"http://192.168.0.106/merwad/resources/views/admin/plugins//vendor/ckeditor/plugins/imageuploader/uploads/210fa2314.jpg\" class=\"img-responsive\"></a></figure>\r\n				 <figure><a href=\"#\"><img src=\"http://192.168.0.106/merwad/resources/views/admin/plugins//vendor/ckeditor/plugins/imageuploader/uploads/21198de44.jpg\" class=\"img-responsive\"></a></figure>\r\n			   </div>\r\n			   <div class=\"col-md-4 col-sm-4\">\r\n				 <figure><a href=\"#\"><img src=\"http://192.168.0.106/merwad/resources/views/admin/plugins//vendor/ckeditor/plugins/imageuploader/uploads/212184777.jpg\" class=\"img-responsive\"></a></figure>\r\n			   </div>\r\n			   <div class=\"col-md-4 col-sm-4\">\r\n				 <figure><a href=\"#\"><img src=\"http://192.168.0.106/merwad/resources/views/admin/plugins//vendor/ckeditor/plugins/imageuploader/uploads/213bd8ee5.jpg\" class=\"img-responsive\"></a></figure>\r\n				 <figure><a href=\"#\"><img src=\"http://192.168.0.106/merwad/resources/views/admin/plugins//vendor/ckeditor/plugins/imageuploader/uploads/21481914b.jpg\" class=\"img-responsive\"></a></figure>\r\n			   </div>', 1, '1'),
(2, 'ASASAS', '<img alt=\"\" src=\"http://localhost/merwad/resources/views/admin/plugins//vendor/ckeditor/plugins/imageuploader/uploads/209b98f55.jpg\" style=\"height:332px; width:370px\" />', 1, '0');

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

DROP TABLE IF EXISTS `brands`;
CREATE TABLE IF NOT EXISTS `brands` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'One Time Payment', '2018-10-17 04:31:27', '2018-10-17 04:31:27'),
(2, 'After payment', '2018-11-15 05:08:02', '2018-11-15 05:08:02');

-- --------------------------------------------------------

--
-- Table structure for table `carts`
--

DROP TABLE IF EXISTS `carts`;
CREATE TABLE IF NOT EXISTS `carts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_attr_id` int(11) DEFAULT NULL,
  `option` varchar(250) DEFAULT NULL,
  `qty` int(11) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `sub_total` decimal(10,2) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `carts`
--

INSERT INTO `carts` (`id`, `customer_id`, `product_id`, `product_attr_id`, `option`, `qty`, `price`, `sub_total`, `updated_at`, `created_at`) VALUES
(15, 28, 4, NULL, NULL, 1, '15.00', '15.00', '2019-05-20 08:43:47', '2019-05-20 08:43:47'),
(13, 30, 4, NULL, NULL, 1, '15.00', '15.00', '2019-02-05 09:31:10', '2019-02-05 09:31:10');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_ar` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `description_ar` longtext COLLATE utf8mb4_unicode_ci,
  `cover` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `parent_id` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `display_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `categories_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `name_ar`, `slug`, `description`, `description_ar`, `cover`, `status`, `parent_id`, `created_at`, `updated_at`, `display_order`) VALUES
(1, 'Uncategorized', '', 'uncategorized', 'Doloremque sint praesentium et voluptatem. Quas fugit perferendis sit consectetur aliquam voluptatibus expedita vitae. Exercitationem veritatis omnis suscipit dolores quis qui.', NULL, 'categories/HHm4zs0P87hsWsI23luAqNLTDglea2xAtBvr3HQi.png', 1, 0, '2018-10-17 01:31:24', '2018-10-17 01:31:24', 0),
(2, 'Plumbing', '', 'plumbing', NULL, NULL, 'categories/1543824186.images (2).jpg', 1, 1, '2018-12-03 05:03:06', '2018-12-03 05:03:06', 0),
(3, 'Air Condition', '', 'air-condition', NULL, NULL, 'categories/1543824248.window-ac-1525200738.jpg', 1, 1, '2018-12-03 05:04:08', '2018-12-03 05:04:08', 0),
(4, 'Tiling', '', 'tiling', NULL, NULL, NULL, 1, 1, '2018-12-03 05:05:01', '2018-12-03 05:05:01', 0),
(5, 'Others', '', 'others', NULL, NULL, NULL, 1, 1, '2018-12-03 05:05:13', '2018-12-03 05:05:13', 0),
(7, 'test category en', 'test cat arabic', 'test-category-en', 'test des en', 'test des ar', NULL, 1, 3, '2019-08-23 06:49:45', '2019-08-23 06:49:45', 0);

-- --------------------------------------------------------

--
-- Table structure for table `category_product`
--

DROP TABLE IF EXISTS `category_product`;
CREATE TABLE IF NOT EXISTS `category_product` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `category_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  KEY `category_product_category_id_index` (`category_id`),
  KEY `category_product_product_id_index` (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `category_product`
--

INSERT INTO `category_product` (`id`, `category_id`, `product_id`) VALUES
(1, 2, 1),
(2, 2, 2),
(3, 2, 3),
(4, 3, 4),
(5, 3, 5),
(6, 3, 6),
(7, 3, 7),
(8, 4, 8),
(9, 4, 9),
(10, 5, 10),
(11, 5, 11),
(18, 3, 19),
(19, 5, 19),
(22, 5, 25),
(23, 2, 25),
(26, 2, 27),
(27, 2, 28);

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

DROP TABLE IF EXISTS `cities`;
CREATE TABLE IF NOT EXISTS `cities` (
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `province_id` int(10) UNSIGNED DEFAULT NULL,
  KEY `cities_province_id_foreign` (`province_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

DROP TABLE IF EXISTS `contacts`;
CREATE TABLE IF NOT EXISTS `contacts` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `email_address` varchar(15) NOT NULL,
  `phone` int(15) DEFAULT NULL,
  `created_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

DROP TABLE IF EXISTS `countries`;
CREATE TABLE IF NOT EXISTS `countries` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `iso` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `iso3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `numcode` int(11) DEFAULT NULL,
  `phonecode` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `countries_name_unique` (`name`),
  UNIQUE KEY `countries_iso_unique` (`iso`)
) ENGINE=InnoDB AUTO_INCREMENT=240 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `name`, `iso`, `iso3`, `numcode`, `phonecode`, `status`, `created_at`, `updated_at`) VALUES
(1, 'AFGHANISTAN', 'AF', 'AFG', 4, 93, 0, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(2, 'ALBANIA', 'AL', 'ALB', 8, 355, 0, '2018-10-17 04:31:25', '2019-01-30 03:10:23'),
(3, 'ALGERIA', 'DZ', 'DZA', 12, 213, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(4, 'AMERICAN SAMOA', 'AS', 'ASM', 16, 1684, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(5, 'ANDORRA', 'AD', 'AND', 20, 376, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(6, 'ANGOLA', 'AO', 'AGO', 24, 244, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(7, 'ANGUILLA', 'AI', 'AIA', 660, 1264, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(8, 'ANTARCTICA', 'AQ', NULL, NULL, 0, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(9, 'ANTIGUA AND BARBUDA', 'AG', 'ATG', 28, 1268, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(10, 'ARGENTINA', 'AR', 'ARG', 32, 54, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(11, 'ARMENIA', 'AM', 'ARM', 51, 374, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(12, 'ARUBA', 'AW', 'ABW', 533, 297, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(13, 'AUSTRALIA', 'AU', 'AUS', 36, 61, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(14, 'AUSTRIA', 'AT', 'AUT', 40, 43, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(15, 'AZERBAIJAN', 'AZ', 'AZE', 31, 994, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(16, 'BAHAMAS', 'BS', 'BHS', 44, 1242, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(17, 'BAHRAIN', 'BH', 'BHR', 48, 973, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(18, 'BANGLADESH', 'BD', 'BGD', 50, 880, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(19, 'BARBADOS', 'BB', 'BRB', 52, 1246, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(20, 'BELARUS', 'BY', 'BLR', 112, 375, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(21, 'BELGIUM', 'BE', 'BEL', 56, 32, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(22, 'BELIZE', 'BZ', 'BLZ', 84, 501, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(23, 'BENIN', 'BJ', 'BEN', 204, 229, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(24, 'BERMUDA', 'BM', 'BMU', 60, 1441, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(25, 'BHUTAN', 'BT', 'BTN', 64, 975, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(26, 'BOLIVIA', 'BO', 'BOL', 68, 591, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(27, 'BOSNIA AND HERZEGOVINA', 'BA', 'BIH', 70, 387, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(28, 'BOTSWANA', 'BW', 'BWA', 72, 267, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(29, 'BOUVET ISLAND', 'BV', NULL, NULL, 0, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(30, 'BRAZIL', 'BR', 'BRA', 76, 55, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(31, 'BRITISH INDIAN OCEAN TERRITORY', 'IO', NULL, NULL, 246, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(32, 'BRUNEI DARUSSALAM', 'BN', 'BRN', 96, 673, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(33, 'BULGARIA', 'BG', 'BGR', 100, 359, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(34, 'BURKINA FASO', 'BF', 'BFA', 854, 226, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(35, 'BURUNDI', 'BI', 'BDI', 108, 257, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(36, 'CAMBODIA', 'KH', 'KHM', 116, 855, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(37, 'CAMEROON', 'CM', 'CMR', 120, 237, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(38, 'CANADA', 'CA', 'CAN', 124, 1, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(39, 'CAPE VERDE', 'CV', 'CPV', 132, 238, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(40, 'CAYMAN ISLANDS', 'KY', 'CYM', 136, 1345, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(41, 'CENTRAL AFRICAN REPUBLIC', 'CF', 'CAF', 140, 236, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(42, 'CHAD', 'TD', 'TCD', 148, 235, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(43, 'CHILE', 'CL', 'CHL', 152, 56, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(44, 'CHINA', 'CN', 'CHN', 156, 86, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(45, 'CHRISTMAS ISLAND', 'CX', NULL, NULL, 61, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(46, 'COCOS (KEELING) ISLANDS', 'CC', NULL, NULL, 672, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(47, 'COLOMBIA', 'CO', 'COL', 170, 57, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(48, 'COMOROS', 'KM', 'COM', 174, 269, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(49, 'CONGO', 'CG', 'COG', 178, 242, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(50, 'CONGO, THE DEMOCRATIC REPUBLIC OF THE', 'CD', 'COD', 180, 242, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(51, 'COOK ISLANDS', 'CK', 'COK', 184, 682, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(52, 'COSTA RICA', 'CR', 'CRI', 188, 506, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(53, 'COTE D\'IVOIRE', 'CI', 'CIV', 384, 225, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(54, 'CROATIA', 'HR', 'HRV', 191, 385, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(55, 'CUBA', 'CU', 'CUB', 192, 53, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(56, 'CYPRUS', 'CY', 'CYP', 196, 357, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(57, 'CZECH REPUBLIC', 'CZ', 'CZE', 203, 420, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(58, 'DENMARK', 'DK', 'DNK', 208, 45, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(59, 'DJIBOUTI', 'DJ', 'DJI', 262, 253, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(60, 'DOMINICA', 'DM', 'DMA', 212, 1767, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(61, 'DOMINICAN REPUBLIC', 'DO', 'DOM', 214, 1809, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(62, 'ECUADOR', 'EC', 'ECU', 218, 593, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(63, 'EGYPT', 'EG', 'EGY', 818, 20, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(64, 'EL SALVADOR', 'SV', 'SLV', 222, 503, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(65, 'EQUATORIAL GUINEA', 'GQ', 'GNQ', 226, 240, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(66, 'ERITREA', 'ER', 'ERI', 232, 291, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(67, 'ESTONIA', 'EE', 'EST', 233, 372, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(68, 'ETHIOPIA', 'ET', 'ETH', 231, 251, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(69, 'FALKLAND ISLANDS (MALVINAS)', 'FK', 'FLK', 238, 500, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(70, 'FAROE ISLANDS', 'FO', 'FRO', 234, 298, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(71, 'FIJI', 'FJ', 'FJI', 242, 679, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(72, 'FINLAND', 'FI', 'FIN', 246, 358, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(73, 'FRANCE', 'FR', 'FRA', 250, 33, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(74, 'FRENCH GUIANA', 'GF', 'GUF', 254, 594, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(75, 'FRENCH POLYNESIA', 'PF', 'PYF', 258, 689, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(76, 'FRENCH SOUTHERN TERRITORIES', 'TF', NULL, NULL, 0, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(77, 'GABON', 'GA', 'GAB', 266, 241, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(78, 'GAMBIA', 'GM', 'GMB', 270, 220, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(79, 'GEORGIA', 'GE', 'GEO', 268, 995, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(80, 'GERMANY', 'DE', 'DEU', 276, 49, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(81, 'GHANA', 'GH', 'GHA', 288, 233, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(82, 'GIBRALTAR', 'GI', 'GIB', 292, 350, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(83, 'GREECE', 'GR', 'GRC', 300, 30, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(84, 'GREENLAND', 'GL', 'GRL', 304, 299, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(85, 'GRENADA', 'GD', 'GRD', 308, 1473, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(86, 'GUADELOUPE', 'GP', 'GLP', 312, 590, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(87, 'GUAM', 'GU', 'GUM', 316, 1671, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(88, 'GUATEMALA', 'GT', 'GTM', 320, 502, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(89, 'GUINEA', 'GN', 'GIN', 324, 224, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(90, 'GUINEA-BISSAU', 'GW', 'GNB', 624, 245, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(91, 'GUYANA', 'GY', 'GUY', 328, 592, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(92, 'HAITI', 'HT', 'HTI', 332, 509, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(93, 'HEARD ISLAND AND MCDONALD ISLANDS', 'HM', NULL, NULL, 0, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(94, 'HOLY SEE (VATICAN CITY STATE)', 'VA', 'VAT', 336, 39, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(95, 'HONDURAS', 'HN', 'HND', 340, 504, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(96, 'HONG KONG', 'HK', 'HKG', 344, 852, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(97, 'HUNGARY', 'HU', 'HUN', 348, 36, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(98, 'ICELAND', 'IS', 'ISL', 352, 354, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(99, 'INDIA', 'IN', 'IND', 356, 91, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(100, 'INDONESIA', 'ID', 'IDN', 360, 62, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(101, 'IRAN, ISLAMIC REPUBLIC OF', 'IR', 'IRN', 364, 98, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(102, 'IRAQ', 'IQ', 'IRQ', 368, 964, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(103, 'IRELAND', 'IE', 'IRL', 372, 353, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(104, 'ISRAEL', 'IL', 'ISR', 376, 972, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(105, 'ITALY', 'IT', 'ITA', 380, 39, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(106, 'JAMAICA', 'JM', 'JAM', 388, 1876, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(107, 'JAPAN', 'JP', 'JPN', 392, 81, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(108, 'JORDAN', 'JO', 'JOR', 400, 962, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(109, 'KAZAKHSTAN', 'KZ', 'KAZ', 398, 7, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(110, 'KENYA', 'KE', 'KEN', 404, 254, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(111, 'KIRIBATI', 'KI', 'KIR', 296, 686, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(112, 'KOREA, DEMOCRATIC PEOPLE\'S REPUBLIC OF', 'KP', 'PRK', 408, 850, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(113, 'KOREA, REPUBLIC OF', 'KR', 'KOR', 410, 82, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(114, 'KUWAIT', 'KW', 'KWT', 414, 965, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(115, 'KYRGYZSTAN', 'KG', 'KGZ', 417, 996, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(116, 'LAO PEOPLE\'S DEMOCRATIC REPUBLIC', 'LA', 'LAO', 418, 856, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(117, 'LATVIA', 'LV', 'LVA', 428, 371, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(118, 'LEBANON', 'LB', 'LBN', 422, 961, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(119, 'LESOTHO', 'LS', 'LSO', 426, 266, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(120, 'LIBERIA', 'LR', 'LBR', 430, 231, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(121, 'LIBYAN ARAB JAMAHIRIYA', 'LY', 'LBY', 434, 218, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(122, 'LIECHTENSTEIN', 'LI', 'LIE', 438, 423, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(123, 'LITHUANIA', 'LT', 'LTU', 440, 370, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(124, 'LUXEMBOURG', 'LU', 'LUX', 442, 352, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(125, 'MACAO', 'MO', 'MAC', 446, 853, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(126, 'MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF', 'MK', 'MKD', 807, 389, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(127, 'MADAGASCAR', 'MG', 'MDG', 450, 261, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(128, 'MALAWI', 'MW', 'MWI', 454, 265, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(129, 'MALAYSIA', 'MY', 'MYS', 458, 60, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(130, 'MALDIVES', 'MV', 'MDV', 462, 960, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(131, 'MALI', 'ML', 'MLI', 466, 223, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(132, 'MALTA', 'MT', 'MLT', 470, 356, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(133, 'MARSHALL ISLANDS', 'MH', 'MHL', 584, 692, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(134, 'MARTINIQUE', 'MQ', 'MTQ', 474, 596, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(135, 'MAURITANIA', 'MR', 'MRT', 478, 222, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(136, 'MAURITIUS', 'MU', 'MUS', 480, 230, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(137, 'MAYOTTE', 'YT', NULL, NULL, 269, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(138, 'MEXICO', 'MX', 'MEX', 484, 52, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(139, 'MICRONESIA, FEDERATED STATES OF', 'FM', 'FSM', 583, 691, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(140, 'MOLDOVA, REPUBLIC OF', 'MD', 'MDA', 498, 373, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(141, 'MONACO', 'MC', 'MCO', 492, 377, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(142, 'MONGOLIA', 'MN', 'MNG', 496, 976, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(143, 'MONTSERRAT', 'MS', 'MSR', 500, 1664, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(144, 'MOROCCO', 'MA', 'MAR', 504, 212, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(145, 'MOZAMBIQUE', 'MZ', 'MOZ', 508, 258, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(146, 'MYANMAR', 'MM', 'MMR', 104, 95, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(147, 'NAMIBIA', 'NA', 'NAM', 516, 264, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(148, 'NAURU', 'NR', 'NRU', 520, 674, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(149, 'NEPAL', 'NP', 'NPL', 524, 977, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(150, 'NETHERLANDS', 'NL', 'NLD', 528, 31, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(151, 'NETHERLANDS ANTILLES', 'AN', 'ANT', 530, 599, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(152, 'NEW CALEDONIA', 'NC', 'NCL', 540, 687, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(153, 'NEW ZEALAND', 'NZ', 'NZL', 554, 64, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(154, 'NICARAGUA', 'NI', 'NIC', 558, 505, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(155, 'NIGER', 'NE', 'NER', 562, 227, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(156, 'NIGERIA', 'NG', 'NGA', 566, 234, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(157, 'NIUE', 'NU', 'NIU', 570, 683, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(158, 'NORFOLK ISLAND', 'NF', 'NFK', 574, 672, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(159, 'NORTHERN MARIANA ISLANDS', 'MP', 'MNP', 580, 1670, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(160, 'NORWAY', 'NO', 'NOR', 578, 47, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(161, 'OMAN', 'OM', 'OMN', 512, 968, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(162, 'PAKISTAN', 'PK', 'PAK', 586, 92, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(163, 'PALAU', 'PW', 'PLW', 585, 680, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(164, 'PALESTINIAN TERRITORY, OCCUPIED', 'PS', NULL, NULL, 970, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(165, 'PANAMA', 'PA', 'PAN', 591, 507, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(166, 'PAPUA NEW GUINEA', 'PG', 'PNG', 598, 675, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(167, 'PARAGUAY', 'PY', 'PRY', 600, 595, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(168, 'PERU', 'PE', 'PER', 604, 51, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(169, 'PHILIPPINES', 'PH', 'PHL', 608, 63, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(170, 'PITCAIRN', 'PN', 'PCN', 612, 0, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(171, 'POLAND', 'PL', 'POL', 616, 48, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(172, 'PORTUGAL', 'PT', 'PRT', 620, 351, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(173, 'PUERTO RICO', 'PR', 'PRI', 630, 1787, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(174, 'QATAR', 'QA', 'QAT', 634, 974, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(175, 'REUNION', 'RE', 'REU', 638, 262, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(176, 'ROMANIA', 'RO', 'ROM', 642, 40, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(177, 'RUSSIAN FEDERATION', 'RU', 'RUS', 643, 70, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(178, 'RWANDA', 'RW', 'RWA', 646, 250, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(179, 'SAINT HELENA', 'SH', 'SHN', 654, 290, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(180, 'SAINT KITTS AND NEVIS', 'KN', 'KNA', 659, 1869, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(181, 'SAINT LUCIA', 'LC', 'LCA', 662, 1758, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(182, 'SAINT PIERRE AND MIQUELON', 'PM', 'SPM', 666, 508, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(183, 'SAINT VINCENT AND THE GRENADINES', 'VC', 'VCT', 670, 1784, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(184, 'SAMOA', 'WS', 'WSM', 882, 684, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(185, 'SAN MARINO', 'SM', 'SMR', 674, 378, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(186, 'SAO TOME AND PRINCIPE', 'ST', 'STP', 678, 239, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(187, 'SAUDI ARABIA', 'SA', 'SAU', 682, 966, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(188, 'SENEGAL', 'SN', 'SEN', 686, 221, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(189, 'SERBIA AND MONTENEGRO', 'CS', NULL, NULL, 381, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(190, 'SEYCHELLES', 'SC', 'SYC', 690, 248, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(191, 'SIERRA LEONE', 'SL', 'SLE', 694, 232, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(192, 'SINGAPORE', 'SG', 'SGP', 702, 65, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(193, 'SLOVAKIA', 'SK', 'SVK', 703, 421, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(194, 'SLOVENIA', 'SI', 'SVN', 705, 386, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(195, 'SOLOMON ISLANDS', 'SB', 'SLB', 90, 677, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(196, 'SOMALIA', 'SO', 'SOM', 706, 252, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(197, 'SOUTH AFRICA', 'ZA', 'ZAF', 710, 27, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(198, 'SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS', 'GS', NULL, NULL, 0, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(199, 'SPAIN', 'ES', 'ESP', 724, 34, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(200, 'SRI LANKA', 'LK', 'LKA', 144, 94, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(201, 'SUDAN', 'SD', 'SDN', 736, 249, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(202, 'SURINAME', 'SR', 'SUR', 740, 597, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(203, 'SVALBARD AND JAN MAYEN', 'SJ', 'SJM', 744, 47, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(204, 'SWAZILAND', 'SZ', 'SWZ', 748, 268, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(205, 'SWEDEN', 'SE', 'SWE', 752, 46, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(206, 'SWITZERLAND', 'CH', 'CHE', 756, 41, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(207, 'SYRIAN ARAB REPUBLIC', 'SY', 'SYR', 760, 963, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(208, 'TAIWAN, PROVINCE OF CHINA', 'TW', 'TWN', 158, 886, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(209, 'TAJIKISTAN', 'TJ', 'TJK', 762, 992, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(210, 'TANZANIA, UNITED REPUBLIC OF', 'TZ', 'TZA', 834, 255, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(211, 'THAILAND', 'TH', 'THA', 764, 66, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(212, 'TIMOR-LESTE', 'TL', NULL, NULL, 670, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(213, 'TOGO', 'TG', 'TGO', 768, 228, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(214, 'TOKELAU', 'TK', 'TKL', 772, 690, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(215, 'TONGA', 'TO', 'TON', 776, 676, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(216, 'TRINIDAD AND TOBAGO', 'TT', 'TTO', 780, 1868, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(217, 'TUNISIA', 'TN', 'TUN', 788, 216, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(218, 'TURKEY', 'TR', 'TUR', 792, 90, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(219, 'TURKMENISTAN', 'TM', 'TKM', 795, 7370, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(220, 'TURKS AND CAICOS ISLANDS', 'TC', 'TCA', 796, 1649, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(221, 'TUVALU', 'TV', 'TUV', 798, 688, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(222, 'UGANDA', 'UG', 'UGA', 800, 256, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(223, 'UKRAINE', 'UA', 'UKR', 804, 380, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(224, 'UNITED ARAB EMIRATES', 'AE', 'ARE', 784, 971, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(225, 'UNITED KINGDOM', 'GB', 'GBR', 826, 44, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(226, 'UNITED STATES OF AMERICA', 'US', 'USA', 840, 1, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(227, 'UNITED STATES MINOR OUTLYING ISLANDS', 'UM', NULL, NULL, 1, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(228, 'URUGUAY', 'UY', 'URY', 858, 598, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(229, 'UZBEKISTAN', 'UZ', 'UZB', 860, 998, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(230, 'VANUATU', 'VU', 'VUT', 548, 678, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(231, 'VENEZUELA', 'VE', 'VEN', 862, 58, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(232, 'VIET NAM', 'VN', 'VNM', 704, 84, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(233, 'VIRGIN ISLANDS, BRITISH', 'VG', 'VGB', 92, 1284, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(234, 'VIRGIN ISLANDS, U.S.', 'VI', 'VIR', 850, 1340, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(235, 'WALLIS AND FUTUNA', 'WF', 'WLF', 876, 681, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(236, 'WESTERN SAHARA', 'EH', 'ESH', 732, 212, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(237, 'YEMEN', 'YE', 'YEM', 887, 967, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(238, 'ZAMBIA', 'ZM', 'ZMB', 894, 260, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25'),
(239, 'ZIMBABWE', 'ZW', 'ZWE', 716, 263, 1, '2018-10-17 04:31:25', '2018-10-17 04:31:25');

-- --------------------------------------------------------

--
-- Table structure for table `country_product`
--

DROP TABLE IF EXISTS `country_product`;
CREATE TABLE IF NOT EXISTS `country_product` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `country_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  KEY `country_product_country_id_index` (`country_id`),
  KEY `country_product_product_id_index` (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `country_product`
--

INSERT INTO `country_product` (`id`, `country_id`, `product_id`) VALUES
(18, 6, 27),
(19, 24, 27),
(23, 3, 25),
(24, 187, 4),
(25, 174, 25),
(26, 174, 28),
(27, 33, 28);

-- --------------------------------------------------------

--
-- Table structure for table `couriers`
--

DROP TABLE IF EXISTS `couriers`;
CREATE TABLE IF NOT EXISTS `couriers` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_free` int(11) NOT NULL,
  `cost` decimal(8,2) DEFAULT '0.00',
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

DROP TABLE IF EXISTS `customers`;
CREATE TABLE IF NOT EXISTS `customers` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lname` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profile_pic` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customertype` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fb_id` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gmail_id` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_id` int(10) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `stripe_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_brand` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_last_four` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trial_ends_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `login_token` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `customers_email_unique` (`email`),
  UNIQUE KEY `token_unique` (`login_token`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `name`, `lname`, `profile_pic`, `email`, `password`, `phone`, `customertype`, `fb_id`, `gmail_id`, `country_id`, `status`, `stripe_id`, `card_brand`, `card_last_four`, `trial_ends_at`, `deleted_at`, `remember_token`, `login_token`, `created_at`, `updated_at`) VALUES
(15, 'Sanil', 'asasghfghfgh', NULL, 'sanil637@gmail.com', '$2y$10$Y4RMh/5o4btZq3ounIi8i.mNSi.Wtk7ugp6LtWCiORP.xZQTT0NzS', '6767676767', '', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 'X6iq9um7BdhcsTzRQKPU7ehwpo12YDf73Lk17TaQ8BT8gtOR9xCWf7jqyp3S', 'c6372e53ef4404b33e7dbcefded6c77d', '2018-11-01 21:18:02', '2018-11-13 00:58:43'),
(22, 'sssss', 'ssssss', NULL, 'sanil637111@gmail.com', '$2y$10$q1fBFpIL49OeQt5ewDdqSel9dQDe3ueC1BwyqKT//YEya1wJ6mrxO', '5656565656', '', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-11-26 21:10:26', '2018-11-26 21:10:26'),
(23, 'sssss', 'ssssss', NULL, 'sanil637111s@gmail.com', '$2y$10$KmNZN.ow/Zx.wg7USOdGkeeuv683vi20MbBt6TeLz8ZSFu1RzSQRe', '4545454545', '', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-11-26 21:21:00', '2018-11-26 21:21:00'),
(26, 'SSSSS', 'SSSSS', NULL, 'sanil6370@gmail.com', '$2y$10$dOTG/SexcexCHNT34lWmLuJKmUZD3UY0sSbf1jT1E3zd0w7zqmsEC', '3434343434', '', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, '385e266f5a8d1ef39f6585df5cb60bb3e0e3a1cb76a52cbb97be20fa60e8bc7d', '2018-11-27 03:43:58', '2018-11-27 03:43:58'),
(27, 'test', 'ttt', NULL, 'test@gmail.com', '$2y$10$gtIVY2L2fRCNiGStLBieSe57BYE2.V16355hi5aTNm1qVA7gj6ktG', '2323232323', '', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, '8eef05ceee0856471865337d710951fbb2d9ae3fe7677c82080d00e5e79f55ea', '2018-12-11 00:53:23', '2018-12-11 00:53:23'),
(28, 'test2', 'tst', NULL, 'test2@gmail.com', '$2y$10$SUzp5rbZMfWH1LVx0/sreuru2Q1WaaOF9OKfew/NO37/Uejn.enFa', '1212121212', '', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'a8b6c1f0d093616e1c850de7ad6b8a7ef1ca0d34b645de8eb07aa216889f485e', '2018-12-14 03:39:08', '2018-12-14 03:39:08'),
(29, 'fgf', NULL, NULL, 'vbvg@gmail.com', '$2y$10$gj3GPs.N7Q1vxLGT6Qwk1OR6rMH3guzfu/YdGCr4qzxZMLh9OPb2y', '1212121122', '', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, '6bd3800313ed491b9414264becb2834f37c914357e4be6ad7b48f1cd9c312906', '2019-01-02 05:27:06', '2019-01-02 05:27:06'),
(30, 'zsd', NULL, NULL, 'fsdfs@gmail.com', '$2y$10$0zFbUoASlEZvgaMp./BJS.rAh5Z2EWvMi0uEBNIUd5MNZVnj.N/Dy', '64565555', 'Business', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, '98eaf13c443b6c9da70002e38886b72bb9c553a876191bd0ca2df21680926425', '2019-01-04 00:26:08', '2019-01-24 06:19:05'),
(33, 'testuser', NULL, NULL, 'testuser@gmail.com', '$2y$10$j3KkAL/cBYd9mbXvjstvIeHXRtcKp9AH0nV5m3.a0fmE2RJbQu4pK', NULL, 'Business', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, '5988800572429b5d1556de015b230ac20eac24ab4d48ce815f2b5af5dcc94b06', '2019-01-24 06:11:47', '2019-01-24 06:18:22'),
(34, 'qwe', NULL, NULL, 'qwe@gmail.com', '$2y$10$h/7z4wch7ckJGW3eZVRscuw8488axhSh4nDXH2A5FpdNWfWXWAXV2', NULL, 'business', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'ff8b8df610c2569e14b99a14d8523634194a74369a9ebd8cbd259c4b2597785e', '2019-01-24 23:34:23', '2019-01-24 23:34:23'),
(41, 'opop', NULL, NULL, 'opp@gmail.com', '$2y$10$0JwQWcRfc0TXO9iBW4b9PepCBxJ27xzkSdmWP5wFYq35nC/GwQ2OK', NULL, 'Individual', NULL, NULL, 10, 1, NULL, NULL, NULL, NULL, NULL, NULL, '3c9271cc85ba0fdc0c133d34eae2a37c846f80476127616117307262a666456a', '2019-01-29 01:14:58', '2019-01-29 02:11:59'),
(42, 'erer', NULL, 'profile/KuKMGbKeIroKOkX2DLO1FPoLnnzczRUBuePmUHum.jpeg', 'raju@gmail.com', '$2y$10$aXEo5Z.vrp0YaDzgU3AJfO8iYitGL32LkUzev7Q5aUbhN4Q2cMGW2', '9632587410', 'business', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, '63284ae94fa5c53e51246d7295df3ca9c36f96c4e7dcffd2d68b1ec7e7e36722', '2019-02-05 01:30:10', '2019-02-14 23:13:21'),
(43, 'jaime', NULL, 'profile/bG7j0CiKVCAm75iOcVeZSWO1SCUucMlVm6cQYPMc.jpeg', 'chinjuraju1204@gmail.com', '$2y$10$DmHYWqK7ZJ1E6Z0IxCFRsu5DUHFxuvW07D5oyVjiyUhBQG8Zjf4TO', '7896541238', 'business', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'efd202730237608e29dc9f6820915f4d0de901d0c001e952f2033358bdf7e98f', '2019-02-15 00:56:08', '2019-02-15 02:04:06'),
(44, 'jisto', NULL, 'profile/fLaGovrVcPnppBdoSn2ebz7nODcX1aPcLCm3MnQA.jpeg', 'jisto@gmail.com', '$2y$10$zu9Ch4ev9lRnvSeO1D6wo.IfE1uC2vOovHC2p44HKyH9/Nd/LnvuG', '9856321587', 'business', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, '321c61be27f9e40486c6ae13598db6ecd6b1821a42c22568e96ab884abedaf28', '2019-02-15 02:10:08', '2019-02-15 04:48:15'),
(45, 'yyy', NULL, NULL, 'yyy@gmail.com', '$2y$10$T3bT9pMRQiu.IZGF/MtkAO3yVJqJMuS1fhkkItj7dhiaJWA9tgnYy', '9856321856', 'business', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, '4a267974289317018586d67703240e538138710c681dba30b8c860f391ab3c35', '2019-03-01 00:23:41', '2019-03-01 00:25:24'),
(46, 'demouserrr', 'tyty', NULL, 'demouser@gmail.com', '$2y$10$8go9qug2U4qnHRGNwhVkGuVW6oMwskv99n5lWLUxK0LfucRj8JiyW', NULL, 'Individual', 'fgfg5657576b6774', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'bff7f27a683e1ffcc7ec727943406a4cfec560940a9f2e873832dab1c408ee64', '2019-03-08 00:57:39', '2019-09-04 04:34:30');

-- --------------------------------------------------------

--
-- Table structure for table `devices`
--

DROP TABLE IF EXISTS `devices`;
CREATE TABLE IF NOT EXISTS `devices` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `device_id` text COLLATE utf8_unicode_ci NOT NULL,
  `customers_id` int(100) NOT NULL DEFAULT '0',
  `device_type` text COLLATE utf8_unicode_ci NOT NULL,
  `register_date` int(100) NOT NULL DEFAULT '0',
  `update_date` int(100) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `isDesktop` tinyint(1) NOT NULL DEFAULT '0',
  `oneSignalId` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `isEnableMobile` tinyint(1) NOT NULL DEFAULT '1',
  `isEnableDesktop` tinyint(1) NOT NULL DEFAULT '1',
  `ram` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `processor` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `device_os` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `device_model` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `manufacturer` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_notify` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

DROP TABLE IF EXISTS `employees`;
CREATE TABLE IF NOT EXISTS `employees` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `employees_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `name`, `email`, `password`, `status`, `deleted_at`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Marcel', 'john@doe.com', '$2y$10$5ZrA4DfB5j1pybCFryKQyugMfls.Ql6acYHNoopX4W60hJfegExJW', 1, NULL, 'hJ1WcgFyXm', '2018-10-17 04:31:24', '2018-10-17 04:31:24'),
(2, 'Uni', 'admin@uni.com', '$2y$10$qC1g9GZQymqRzE26pgD29uC17Sde/gqZudmTYI5KoeMuO2r9joVFS', 1, NULL, 'RIp4ZcxS1M', '2018-10-17 04:31:24', '2018-11-29 09:02:26'),
(3, 'Rex', 'clerk@doe.com', '$2y$10$5ZrA4DfB5j1pybCFryKQyugMfls.Ql6acYHNoopX4W60hJfegExJW', 1, NULL, 'hr1OoDSHKB', '2018-10-17 04:31:24', '2018-10-17 04:31:24');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_customers_table', 1),
(2, '2014_10_12_000010_create_employees_table', 1),
(3, '2014_10_12_100000_create_password_resets_table', 1),
(4, '2016_05_26_020731_create_country_table', 1),
(5, '2016_05_26_035202_create_provinces_table', 1),
(6, '2016_05_26_051502_create_cities_table', 1),
(7, '2017_06_10_225235_create_products_table', 1),
(8, '2017_06_11_015526_create_categories_table', 1),
(9, '2017_06_11_033553_create_category_product_table', 1),
(10, '2017_06_11_073305_create_address_table', 1),
(11, '2017_06_12_225546_create_order_status_table', 1),
(12, '2017_06_13_044714_create_couriers_table', 1),
(13, '2017_06_13_053346_create_orders_table', 1),
(14, '2017_06_13_091740_create_order_products_table', 1),
(15, '2017_06_17_011245_create_shoppingcart_table', 1),
(16, '2018_01_18_163143_create_product_images_table', 1),
(17, '2018_02_19_151228_create_cost_column', 1),
(18, '2018_03_10_024148_laratrust_setup_tables', 1),
(19, '2018_03_10_110530_create_attributes_table', 1),
(20, '2018_03_10_150920_create_attribute_values_table', 1),
(21, '2018_03_11_014046_create_product_attributes_table', 1),
(22, '2018_03_11_090249_create_attribute_value_product_attribute_table', 1),
(23, '2018_03_15_232344_create_customer_subscription_table', 1),
(24, '2018_06_16_000410_add_fields_on_order_product_table', 1),
(25, '2018_06_16_102641_create_brands_table', 1),
(26, '2018_06_17_175657_add_brand_id_in_products_table', 1),
(27, '2018_06_18_135142_add_columns_in_product_attributes_table', 1),
(28, '2018_06_30_041523_add_product_attributes', 1),
(29, '2018_07_03_023925_create_states_table', 1),
(30, '2018_07_16_184224_add_phone_number_in_address_table', 1),
(31, '2018_07_16_190024_add_tracking_number_and_label_url_to_orders_table', 1),
(32, '2018_07_17_184437_add_sale_price_in_products_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

DROP TABLE IF EXISTS `notification`;
CREATE TABLE IF NOT EXISTS `notification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(5) NOT NULL,
  `order_id` int(10) NOT NULL,
  `title` varchar(250) NOT NULL,
  `message` varchar(300) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notification`
--

INSERT INTO `notification` (`id`, `customer_id`, `order_id`, `title`, `message`) VALUES
(1, 30, 22, 'Request Requested', 'Request ID #ORD0000002022 has been Requested'),
(2, 30, 22, 'Request Received', 'Request ID #ORD0000002022 has been Received'),
(3, 27, 2, 'Request Requested', 'Request ID #ORD0000002002 has been Requested'),
(4, 27, 2, 'Request Cancelled', 'Request ID #ORD0000002002 has been Cancelled'),
(5, 27, 2, 'Request Completed', 'Request ID #ORD0000002002 has been Completed');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `reference` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_id` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customer_id` int(10) UNSIGNED NOT NULL,
  `address_id` int(10) UNSIGNED DEFAULT NULL,
  `shipping_address_id` int(11) DEFAULT NULL,
  `coupon_code` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_status_id` int(10) UNSIGNED NOT NULL,
  `payment` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `discounts` decimal(8,2) NOT NULL DEFAULT '0.00',
  `total_products` decimal(8,2) DEFAULT NULL,
  `shipping_charge` decimal(10,2) DEFAULT NULL,
  `tax` decimal(8,2) NOT NULL DEFAULT '0.00',
  `total` decimal(8,2) DEFAULT NULL,
  `total_paid` decimal(8,2) NOT NULL DEFAULT '0.00',
  `invoice` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `label_url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tracking_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `servicedate` date DEFAULT NULL,
  `servicetime` time(6) DEFAULT NULL,
  `summary` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(900) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `uploadfile` varchar(900) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location_latitude` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location_longitude` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `orders_reference_unique` (`reference`),
  KEY `orders_customer_id_index` (`customer_id`),
  KEY `orders_address_id_index` (`address_id`),
  KEY `orders_order_status_id_index` (`order_status_id`),
  KEY `shipping_address_id` (`shipping_address_id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `reference`, `order_id`, `customer_id`, `address_id`, `shipping_address_id`, `coupon_code`, `order_status_id`, `payment`, `discounts`, `total_products`, `shipping_charge`, `tax`, `total`, `total_paid`, `invoice`, `label_url`, `tracking_number`, `created_at`, `updated_at`, `servicedate`, `servicetime`, `summary`, `description`, `uploadfile`, `location_latitude`, `location_longitude`, `address`, `country_id`) VALUES
(1, 'ORD0000002001', NULL, 27, 28, NULL, NULL, 5, 'cash_on_delivery', '0.00', '21.00', NULL, '0.00', '21.00', '0.00', NULL, NULL, NULL, '2018-12-27 03:38:21', '2018-12-27 03:46:04', '2018-02-14', '838:59:59.000000', NULL, 'qqq www eee rrr ttt', NULL, '10.850516', '76.271080', NULL, 0),
(2, 'ORD0000002002', NULL, 27, 28, NULL, NULL, 6, 'cash_on_delivery', '0.00', '37.00', NULL, '0.00', '37.00', '0.00', NULL, NULL, NULL, '2018-12-27 04:51:58', '2019-02-18 01:38:38', '2018-12-12', '838:59:59.000000', NULL, 'aaa bbb ccc ddd', NULL, '10.850516', '76.271080', NULL, 0),
(3, 'ORD0000002003', NULL, 27, 28, NULL, NULL, 5, 'cash_on_delivery', '0.00', '48.00', NULL, '0.00', '48.00', '0.00', NULL, NULL, NULL, '2018-12-27 04:55:46', '2019-01-04 00:49:46', '2018-12-12', '838:59:59.000000', NULL, 'pppp pppp  pppp', NULL, '10.850516', '76.271080', NULL, 0),
(4, 'ORD0000002004', NULL, 28, NULL, NULL, NULL, 5, 'cash_on_delivery', '0.00', '21.00', NULL, '0.00', '21.00', '0.00', NULL, NULL, NULL, '2019-01-02 06:10:49', '2019-01-02 06:10:49', '2018-02-14', '838:59:59.000000', NULL, 'tytyty tyty tyty', NULL, '10.850516', '76.271080', NULL, 0),
(5, 'ORD0000002005', NULL, 28, NULL, NULL, NULL, 5, 'cash_on_delivery', '0.00', '0.00', NULL, '0.00', '0.00', '0.00', NULL, NULL, NULL, '2019-01-02 06:17:38', '2019-01-02 06:42:34', '2018-02-14', '838:59:59.000000', NULL, 'tytyty tyty tyty', NULL, '10.850516', '76.271080', NULL, 0),
(6, 'ORD0000002006', NULL, 29, NULL, NULL, NULL, 3, 'cash_on_delivery', '0.00', '22.00', NULL, '0.00', '22.00', '0.00', NULL, NULL, NULL, '2019-01-04 04:20:16', '2019-01-04 07:19:28', '2018-12-12', '838:59:59.000000', 'ererer erer', 'ertert ert ert ertert ert', NULL, '10.850516', '76.271080', NULL, 0),
(7, 'ORD0000002007', NULL, 30, NULL, NULL, NULL, 1, 'cash_on_delivery', '0.00', '22.00', NULL, '0.00', '22.00', '0.00', NULL, NULL, NULL, '2019-01-07 00:17:06', '2019-01-07 02:00:54', '2019-01-07', '05:40:38.000000', 'aaa bbb ccc', 'ddd eee fff ggg hhh iii', NULL, '10.850516', '76.271080', NULL, 0),
(8, 'ORD0000002008', NULL, 30, NULL, NULL, NULL, 5, 'cash_on_delivery', '0.00', '44.00', NULL, '0.00', '44.00', '0.00', NULL, NULL, NULL, '2019-01-12 03:54:39', '2019-01-12 03:54:39', '2019-01-07', '05:40:38.000000', NULL, NULL, NULL, NULL, NULL, NULL, 0),
(9, 'ORD0000002009', NULL, 30, NULL, NULL, NULL, 6, 'cash_on_delivery', '0.00', '22.00', NULL, '0.00', '22.00', '0.00', NULL, NULL, NULL, '2019-01-12 03:58:01', '2019-02-05 01:16:55', '2019-01-07', '15:40:38.000000', NULL, NULL, NULL, NULL, NULL, NULL, 0),
(10, 'ORD0000002010', NULL, 30, NULL, NULL, NULL, 5, 'cash_on_delivery', '0.00', '0.00', NULL, '0.00', '0.00', '0.00', NULL, NULL, NULL, '2019-01-12 03:58:20', '2019-01-12 03:58:20', '2019-01-07', '15:40:00.000000', NULL, NULL, NULL, NULL, NULL, NULL, 0),
(11, 'ORD0000002011', NULL, 30, NULL, NULL, NULL, 5, 'cash_on_delivery', '0.00', '0.00', NULL, '0.00', '0.00', '0.00', NULL, NULL, NULL, '2019-01-12 04:05:25', '2019-01-12 04:05:25', '2015-02-15', '15:40:00.000000', NULL, NULL, NULL, NULL, NULL, NULL, 0),
(12, 'ORD0000002012', NULL, 30, NULL, NULL, NULL, 5, 'cash_on_delivery', '0.00', '0.00', NULL, '0.00', '0.00', '0.00', NULL, NULL, NULL, '2019-01-12 04:05:34', '2019-01-12 04:05:34', '2015-02-15', '10:20:00.000000', NULL, NULL, NULL, NULL, NULL, NULL, 0),
(13, 'ORD0000002013', NULL, 30, NULL, NULL, NULL, 5, 'cash_on_delivery', '0.00', '0.00', NULL, '0.00', '0.00', '0.00', NULL, NULL, NULL, '2019-01-12 05:58:18', '2019-01-12 05:58:18', '2015-02-15', '10:20:00.000000', NULL, NULL, NULL, NULL, NULL, NULL, 0),
(14, 'ORD0000002014', NULL, 30, NULL, NULL, NULL, 5, 'cash_on_delivery', '0.00', '0.00', NULL, '0.00', '0.00', '0.00', NULL, NULL, NULL, '2019-01-12 05:58:40', '2019-01-12 05:58:40', '2015-02-15', '00:00:00.000000', NULL, NULL, NULL, NULL, NULL, NULL, 0),
(15, 'ORD0000002015', NULL, 30, NULL, NULL, NULL, 5, 'cash_on_delivery', '0.00', '0.00', NULL, '0.00', '0.00', '0.00', NULL, NULL, NULL, '2019-01-12 06:00:09', '2019-01-12 06:00:09', '2018-02-15', '10:20:00.000000', NULL, NULL, NULL, NULL, NULL, NULL, 0),
(16, 'ORD0000002016', NULL, 30, NULL, NULL, NULL, 5, 'cash_on_delivery', '0.00', '0.00', NULL, '0.00', '0.00', '0.00', NULL, NULL, NULL, '2019-01-12 06:00:41', '2019-01-12 06:00:41', '2016-02-15', '10:20:00.000000', NULL, NULL, NULL, NULL, NULL, NULL, 0),
(17, 'ORD0000002017', NULL, 30, NULL, NULL, NULL, 5, 'cash_on_delivery', '0.00', '0.00', NULL, '0.00', '0.00', '0.00', NULL, NULL, NULL, '2019-01-12 06:01:44', '2019-01-12 06:01:44', '2016-02-15', '10:20:00.000000', NULL, NULL, NULL, NULL, NULL, NULL, 0),
(18, 'ORD0000002018', NULL, 30, NULL, NULL, NULL, 5, 'cash_on_delivery', '0.00', '0.00', NULL, '0.00', '0.00', '0.00', NULL, NULL, NULL, '2019-01-12 06:01:54', '2019-01-12 06:01:54', '2016-02-15', '10:20:00.000000', NULL, NULL, NULL, NULL, NULL, NULL, 0),
(19, 'ORD0000002019', NULL, 30, NULL, NULL, NULL, 5, 'cash_on_delivery', '0.00', '0.00', NULL, '0.00', '0.00', '0.00', NULL, NULL, NULL, '2019-01-12 06:02:48', '2019-01-12 06:02:48', '2016-02-15', '10:20:00.000000', NULL, NULL, NULL, NULL, NULL, NULL, 0),
(20, 'ORD0000002020', NULL, 30, NULL, NULL, NULL, 5, 'cash_on_delivery', '0.00', '0.00', NULL, '0.00', '0.00', '0.00', NULL, NULL, NULL, '2019-01-12 06:03:03', '2019-01-12 06:03:03', '2016-02-15', '10:20:00.000000', NULL, NULL, NULL, NULL, NULL, NULL, 0),
(21, 'ORD0000002021', NULL, 30, NULL, NULL, NULL, 6, 'cash_on_delivery', '0.00', '15.00', NULL, '0.00', '15.00', '0.00', NULL, NULL, NULL, '2019-01-21 01:45:10', '2019-02-05 01:07:44', '2019-01-07', '05:40:38.000000', 'gdfg', 'dfgfdgfg', NULL, '10.850516', '76.271080', NULL, 0),
(22, 'ORD0000002022', NULL, 30, NULL, NULL, NULL, 3, 'cash_on_delivery', '0.00', '15.00', NULL, '0.00', '15.00', '0.00', NULL, NULL, NULL, '2019-02-05 03:58:35', '2019-02-16 05:35:49', '2019-01-07', '05:40:38.000000', NULL, NULL, NULL, NULL, NULL, NULL, 0),
(23, 'ORD0000002023', NULL, 42, NULL, NULL, NULL, 5, 'cash_on_delivery', '0.00', '15.00', NULL, '0.00', '15.00', '0.00', NULL, NULL, NULL, '2019-02-15 00:04:22', '2019-02-15 00:52:26', '2019-02-14', '838:59:59.000000', NULL, NULL, NULL, '10.850516', '76.271080', 'aaa bbb', 187);

-- --------------------------------------------------------

--
-- Table structure for table `order_product`
--

DROP TABLE IF EXISTS `order_product`;
CREATE TABLE IF NOT EXISTS `order_product` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `order_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `quantity` int(11) NOT NULL,
  `product_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_sku` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_description` text COLLATE utf8mb4_unicode_ci,
  `product_price` decimal(8,2) DEFAULT NULL,
  `productAttribute` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `order_product_order_id_index` (`order_id`),
  KEY `order_product_product_id_index` (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `order_product`
--

INSERT INTO `order_product` (`id`, `order_id`, `product_id`, `quantity`, `product_name`, `product_sku`, `product_description`, `product_price`, `productAttribute`) VALUES
(1, 1, 3, 1, 'water heaters', NULL, 'derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.', '21.00', NULL),
(2, 2, 5, 1, 'AC Cleaning', NULL, 'derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.', '22.00', NULL),
(3, 2, 4, 1, 'Gas', NULL, 'derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.', '15.00', NULL),
(4, 3, 1, 1, 'Leakages', NULL, 'derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.', '25.00', NULL),
(5, 3, 2, 1, 'Blockages', NULL, 'derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.', '23.00', NULL),
(6, 4, 3, 1, 'water heaters', NULL, 'derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.', '21.00', NULL),
(7, 6, 5, 1, 'AC Cleaning', NULL, 'derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.', '22.00', NULL),
(8, 7, 5, 1, 'AC Cleaning', NULL, 'derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.', '22.00', NULL),
(9, 8, 5, 2, 'AC Cleaning', NULL, 'derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.', '22.00', NULL),
(10, 9, 5, 1, 'AC Cleaning', NULL, 'derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.', '22.00', NULL),
(11, 21, 4, 1, 'Gas', NULL, 'derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.', '15.00', NULL),
(12, 22, 4, 1, 'Gas', NULL, 'derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.', '15.00', NULL),
(13, 23, 4, 1, 'Gas', NULL, 'derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.', '15.00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `order_statuses`
--

DROP TABLE IF EXISTS `order_statuses`;
CREATE TABLE IF NOT EXISTS `order_statuses` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `order_statuses`
--

INSERT INTO `order_statuses` (`id`, `name`, `color`, `created_at`, `updated_at`) VALUES
(1, 'Received', 'green', '2018-10-17 04:31:27', '2018-10-17 04:31:27'),
(3, 'Cancelled', 'red', '2018-10-17 04:31:27', '2018-10-22 04:41:38'),
(5, 'Requested', 'violet', '2018-10-17 04:31:27', '2018-10-17 04:31:27'),
(6, 'Completed', 'Green', '2018-11-15 00:32:16', '2018-11-15 00:32:16');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
CREATE TABLE IF NOT EXISTS `pages` (
  `page_id` int(100) NOT NULL AUTO_INCREMENT,
  `slug` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`page_id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`page_id`, `slug`, `status`) VALUES
(1, 'privacy-policy', 1),
(2, 'term-services', 1),
(3, 'refund-policy', 1),
(4, 'about-us', 1),
(5, 'mobile-phones', 1),
(6, 'mobile-phones', 0),
(7, 'test', 0),
(8, 'test', 0),
(9, 'test', 0);

-- --------------------------------------------------------

--
-- Table structure for table `pages_description`
--

DROP TABLE IF EXISTS `pages_description`;
CREATE TABLE IF NOT EXISTS `pages_description` (
  `page_description_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `name_ar` varchar(600) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `description_ar` longtext COLLATE utf8_unicode_ci,
  `language_id` int(100) NOT NULL,
  `page_id` int(100) NOT NULL,
  PRIMARY KEY (`page_description_id`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `pages_description`
--

INSERT INTO `pages_description` (`page_description_id`, `name`, `name_ar`, `description`, `description_ar`, `language_id`, `page_id`) VALUES
(1, 'Privacy Policy', NULL, '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy</p>\r\n\r\n<p>text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen</p>\r\n\r\n<p>book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially</p>\r\n\r\n<p>unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages,</p>\r\n\r\n<p>and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem</p>\r\n\r\n<p>Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard</p>\r\n\r\n<p>dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type</p>\r\n\r\n<p>specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining</p>\r\n\r\n<p>essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum</p>\r\n\r\n<p>passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem</p>\r\n\r\n<p>Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s</p>\r\n\r\n<p>standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make</p>\r\n\r\n<p>a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting,</p>\r\n\r\n<p>remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing</p>\r\n\r\n<p>Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions</p>\r\n\r\n<p>of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been</p>\r\n\r\n<p>the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled</p>\r\n\r\n<p>it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting,</p>\r\n\r\n<p>remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing</p>\r\n\r\n<p>Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions</p>\r\n\r\n<p>of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been</p>\r\n\r\n<p>the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled</p>\r\n\r\n<p>it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting,</p>\r\n\r\n<p>remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing</p>\r\n\r\n<p>Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions</p>\r\n\r\n<p>of Lorem Ipsum.<br />\r\n<br />\r\n<img alt=\\\"\\\" src=\\\"http://localhost/services/resources/views/admin/plugins//vendor/ckeditor/plugins/imageuploader/uploads/90980f1.jpg\\\" style=\\\"height:256px; width:256px\\\" /></p>', NULL, 1, 4),
(2, 'Datenschutz-Bestimmungen', NULL, '<p>Lorem ipsum dolor sit amet, putant expetenda ex nec. Ea luptatum verterem vis, sadipscing conclusionemque id ius. Perpetua molestiae mei at, iudicabit interesset vel ad. Efficiendi concludaturque sed in, mazim exerci indoctum at est, nibh nostro ea ius.</p>\r\n\r\n<p>Eros libris theophrastus pri ei. Alii imperdiet an vim. Elitr euripidis vituperatoribus nam at, id qui quod ipsum contentiones, eu paulo ignota verterem pri. In quem posse efficiendi duo, eu per everti iuvaret saperet. Id vel constituam neglegentur, utinam atomorum ei ius.</p>\r\n\r\n<p>Amet mollis antiopam ei sea, iusto aperiri in eam, sed euismod splendide ne. Iuvaret molestie efficiendi in vim, eum cu choro alienum menandri, at choro recteque tincidunt duo. Novum diceret expetendis his id, nominavi facilisi ne est. Movet labore intellegebat has ne, cu veritus mentitum aliquando pri, utamur nominati forensibus duo ut. Quo graece oporteat concludaturque at, qui docendi salutatus assentior cu, latine vocibus dissentias ius ea.</p>\r\n\r\n<p>Ne delenit graecis pri, nec in perpetua mnesarchum. Ex per civibus luptatum. Choro civibus quo no, duo ne omnis salutatus pertinacia, ipsum rationibus eu est. Eos te soleat eripuit indoctum, nobis alienum contentiones vim ad. Mei ut expetenda qualisque constituto. Ex vel case recteque, ea quot debet mucius sed.</p>\r\n\r\n<p>Sea verear labores luptatum ut. Eu pri dico errem ubique, solet saepe mediocritatem nec ad. Cibo urbanitas comprehensam cu qui, ex summo gloriatur quo, quo eu etiam dolor urbanitas. Sed ancillae efficiantur cu, errem omnes salutatus id qui. Tation eloquentiam an per, dolorem perfecto eam at.</p>\r\n', NULL, 2, 1),
(3, 'سياسة الخصوصية', NULL, '<p>الجو تحرير السفن مع بين. تعد مع خيار المتّبعة, كلّ غينيا لعملة أن. الشطر الإثنان لم فقد, به، ٣٠ دارت الدول استراليا،. دنو تصفح موالية أي, عن تحرّك باستخدام لبلجيكا، دنو, و عرض هامش الشطر العدّ. حتى في لإعادة العاصمة.</p>\r\n\r\n<p>شعار وبدأت وتتحمّل من بين. ما أوزار أوروبا ويكيبيديا، لها, يكن تم الحكم ضمنها. دون أم الجنود ديسمبر. أحكم الإنذار، هو بعد, ضرب مدينة اعلان للمجهود عل, مدن بقصف العصبة التقليدية كل. لم حول عقبت بلديهما, وبعض أسيا الأعمال بـ بال. كلا الخطّة الشرقية كل, تم كما قبضتهم الحدود المنتصر, كرسي تصرّف ٣٠ دون.</p>\r\n\r\n<p>ولكسمبورغ واندونيسيا، كل لمّ, والتي لبولندا، بـ الى. أضف تم مسارح الثانية اليابان،, تم يكن دارت تصفح المارق, مع حين واحدة الأعمال المتّبعة. بـ على مشارف نهاية, فكان تحرّك الأمريكية في عدم. ٣٠ أملاً والنفيس التغييرات بها. غريمه الحيلولة أخذ ٣٠, قام عل موالية الهادي التكاليف. في مليارات والديون والإتحاد عدم, بـ ونتج استدعى أفريقيا به،.</p>\r\n\r\n<p>المشترك الشرقية تعد أي, وفي و وأكثرها الرئيسية المعاهدات. أي وبدأت المعاهدات نفس, من بلا عجّل الأمور بولندا،. الأمم إنطلاق عل ولم, عشوائية الأثناء، في مما. مقاطعة الوراء وتم تم, وتنصيب مواقعها بل يكن. تم كنقطة الشتاء، ومن, والإتحاد الرئيسية أي كلّ.</p>\r\n\r\n<p>أي انه الأثنان الجديدة،, لان كل مسرح تزامناً. ذات قد بزمام المضي الإحتفاظ, ٣٠ فسقط أعمال التغييرات لان. ليركز السيء مكثّفة هذه مع, بتطويق والديون تم فعل. بل ولم المبرمة واندونيسيا،, بقصف جيما أحكم هو دار. لم فقامت الشهير العسكري تحت, تصفح الستار والعتاد كما ٣٠.</p>\r\n\r\n<p>في بعض تطوير اوروبا أفريقيا. هو أسر وبعض انذار, أحدث إحكام قتيل، أن قام. مع فصل ونتج بخطوط المزيفة. مع النفط والمانيا لان. بـ تطوير بالرّد فقد.</p>\r\n\r\n<p>بالرّد بالرغم الأرواح أم نفس. من أخذ أخرى فاتّبع وانهاء. قد بشرية مساعدة الإقتصادية وتم, ومن ان جديدة المزيفة. أمّا سبتمبر أم بعض, الصفحة العالمية من أما. حيث بقسوة ابتدعها وقدّموا بل, كل الباهضة الخاسرة كما, تحت بشرية المشترك الاندونيسية و.</p>\r\n\r\n<p>العناد مقاطعة العالمي لم تلك. المزيفة الإقتصادية أن ذلك, إذ حاول اتفاق بالرغم كلّ. ثم أخرى المضي الدنمارك دنو, هو قامت قائمة للأراضي بلا. بلا من كنقطة عسكرياً, نتيجة لعملة المبرمة ان قبل.</p>\r\n\r\n<p>٣٠ دار الأخذ بريطانيا, ومن أم أوسع أواخر إستعمل. الى إستيلاء الواقعة لم, بل أدوات المارق وصل. تسمّى فرنسا وتزويده ٣٠ دنو, لغزو التي انتصارهم بـ حيث. ٣٠ قام فهرست الغالي قُدُماً. من بال أحدث وانتهاءً, حالية عسكرياً الموسوعة أن قبل. أدوات الحكومة ذات مع, الجوي بالتوقيع في على, بـ يبق الأعمال الأسيوي.</p>\r\n\r\n<p>إذ العالم، مسؤولية كلا, نفس إذ أراض بينما عسكرياً. أصقاع بقيادة عرض أم, ودول أسيا أراض ما لان. لمّ ان أجزاء وقامت المتّبعة, ونتج شاسعة المدن ان تحت. وقبل إحتار لمّ أي. شمال بالرّغم ٣٠ حين, عدد يطول اتّجة بتحدّي عل, أم وأزيز بأيدي الا. هذه قد بداية العناد.</p>\r\n', NULL, 4, 1),
(4, 'Term & Services', NULL, '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy</p>\r\n\r\n<p>text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen</p>\r\n\r\n<p>book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially</p>\r\n\r\n<p>unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages,</p>\r\n\r\n<p>and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem</p>\r\n\r\n<p>Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard</p>\r\n\r\n<p>dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type</p>\r\n\r\n<p>specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining</p>\r\n\r\n<p>essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum</p>\r\n\r\n<p>passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem</p>\r\n\r\n<p>Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s</p>\r\n\r\n<p>standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make</p>\r\n\r\n<p>a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting,</p>\r\n\r\n<p>remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing</p>\r\n\r\n<p>Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions</p>\r\n\r\n<p>of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been</p>\r\n\r\n<p>the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled</p>\r\n\r\n<p>it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting,</p>\r\n\r\n<p>remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing</p>\r\n\r\n<p>Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions</p>\r\n\r\n<p>of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been</p>\r\n\r\n<p>the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled</p>\r\n\r\n<p>it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting,</p>\r\n\r\n<p>remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing</p>\r\n\r\n<p>Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions</p>\r\n\r\n<p>of Lorem Ipsum.</p>\r\n', NULL, 1, 2),
(5, 'langfristige Dienstleistungen', NULL, '<p>Lorem ipsum dolor sit amet, putant expetenda ex nec. Ea luptatum verterem vis, sadipscing conclusionemque id ius. Perpetua molestiae mei at, iudicabit interesset vel ad. Efficiendi concludaturque sed in, mazim exerci indoctum at est, nibh nostro ea ius.</p>\r\n\r\n<p>Eros libris theophrastus pri ei. Alii imperdiet an vim. Elitr euripidis vituperatoribus nam at, id qui quod ipsum contentiones, eu paulo ignota verterem pri. In quem posse efficiendi duo, eu per everti iuvaret saperet. Id vel constituam neglegentur, utinam atomorum ei ius.</p>\r\n\r\n<p>Amet mollis antiopam ei sea, iusto aperiri in eam, sed euismod splendide ne. Iuvaret molestie efficiendi in vim, eum cu choro alienum menandri, at choro recteque tincidunt duo. Novum diceret expetendis his id, nominavi facilisi ne est. Movet labore intellegebat has ne, cu veritus mentitum aliquando pri, utamur nominati forensibus duo ut. Quo graece oporteat concludaturque at, qui docendi salutatus assentior cu, latine vocibus dissentias ius ea.</p>\r\n\r\n<p>Ne delenit graecis pri, nec in perpetua mnesarchum. Ex per civibus luptatum. Choro civibus quo no, duo ne omnis salutatus pertinacia, ipsum rationibus eu est. Eos te soleat eripuit indoctum, nobis alienum contentiones vim ad. Mei ut expetenda qualisque constituto. Ex vel case recteque, ea quot debet mucius sed.</p>\r\n\r\n<p>Sea verear labores luptatum ut. Eu pri dico errem ubique, solet saepe mediocritatem nec ad. Cibo urbanitas comprehensam cu qui, ex summo gloriatur quo, quo eu etiam dolor urbanitas. Sed ancillae efficiantur cu, errem omnes salutatus id qui. Tation eloquentiam an per, dolorem perfecto eam at.</p>\r\n', NULL, 2, 2),
(6, 'الخدمات المدى', NULL, '<p>الجو تحرير السفن مع بين. تعد مع خيار المتّبعة, كلّ غينيا لعملة أن. الشطر الإثنان لم فقد, به، ٣٠ دارت الدول استراليا،. دنو تصفح موالية أي, عن تحرّك باستخدام لبلجيكا، دنو, و عرض هامش الشطر العدّ. حتى في لإعادة العاصمة.</p>\r\n\r\n<p>شعار وبدأت وتتحمّل من بين. ما أوزار أوروبا ويكيبيديا، لها, يكن تم الحكم ضمنها. دون أم الجنود ديسمبر. أحكم الإنذار، هو بعد, ضرب مدينة اعلان للمجهود عل, مدن بقصف العصبة التقليدية كل. لم حول عقبت بلديهما, وبعض أسيا الأعمال بـ بال. كلا الخطّة الشرقية كل, تم كما قبضتهم الحدود المنتصر, كرسي تصرّف ٣٠ دون.</p>\r\n\r\n<p>ولكسمبورغ واندونيسيا، كل لمّ, والتي لبولندا، بـ الى. أضف تم مسارح الثانية اليابان،, تم يكن دارت تصفح المارق, مع حين واحدة الأعمال المتّبعة. بـ على مشارف نهاية, فكان تحرّك الأمريكية في عدم. ٣٠ أملاً والنفيس التغييرات بها. غريمه الحيلولة أخذ ٣٠, قام عل موالية الهادي التكاليف. في مليارات والديون والإتحاد عدم, بـ ونتج استدعى أفريقيا به،.</p>\r\n\r\n<p>المشترك الشرقية تعد أي, وفي و وأكثرها الرئيسية المعاهدات. أي وبدأت المعاهدات نفس, من بلا عجّل الأمور بولندا،. الأمم إنطلاق عل ولم, عشوائية الأثناء، في مما. مقاطعة الوراء وتم تم, وتنصيب مواقعها بل يكن. تم كنقطة الشتاء، ومن, والإتحاد الرئيسية أي كلّ.</p>\r\n\r\n<p>أي انه الأثنان الجديدة،, لان كل مسرح تزامناً. ذات قد بزمام المضي الإحتفاظ, ٣٠ فسقط أعمال التغييرات لان. ليركز السيء مكثّفة هذه مع, بتطويق والديون تم فعل. بل ولم المبرمة واندونيسيا،, بقصف جيما أحكم هو دار. لم فقامت الشهير العسكري تحت, تصفح الستار والعتاد كما ٣٠.</p>\r\n\r\n<p>في بعض تطوير اوروبا أفريقيا. هو أسر وبعض انذار, أحدث إحكام قتيل، أن قام. مع فصل ونتج بخطوط المزيفة. مع النفط والمانيا لان. بـ تطوير بالرّد فقد.</p>\r\n\r\n<p>بالرّد بالرغم الأرواح أم نفس. من أخذ أخرى فاتّبع وانهاء. قد بشرية مساعدة الإقتصادية وتم, ومن ان جديدة المزيفة. أمّا سبتمبر أم بعض, الصفحة العالمية من أما. حيث بقسوة ابتدعها وقدّموا بل, كل الباهضة الخاسرة كما, تحت بشرية المشترك الاندونيسية و.</p>\r\n\r\n<p>العناد مقاطعة العالمي لم تلك. المزيفة الإقتصادية أن ذلك, إذ حاول اتفاق بالرغم كلّ. ثم أخرى المضي الدنمارك دنو, هو قامت قائمة للأراضي بلا. بلا من كنقطة عسكرياً, نتيجة لعملة المبرمة ان قبل.</p>\r\n\r\n<p>٣٠ دار الأخذ بريطانيا, ومن أم أوسع أواخر إستعمل. الى إستيلاء الواقعة لم, بل أدوات المارق وصل. تسمّى فرنسا وتزويده ٣٠ دنو, لغزو التي انتصارهم بـ حيث. ٣٠ قام فهرست الغالي قُدُماً. من بال أحدث وانتهاءً, حالية عسكرياً الموسوعة أن قبل. أدوات الحكومة ذات مع, الجوي بالتوقيع في على, بـ يبق الأعمال الأسيوي.</p>\r\n\r\n<p>إذ العالم، مسؤولية كلا, نفس إذ أراض بينما عسكرياً. أصقاع بقيادة عرض أم, ودول أسيا أراض ما لان. لمّ ان أجزاء وقامت المتّبعة, ونتج شاسعة المدن ان تحت. وقبل إحتار لمّ أي. شمال بالرّغم ٣٠ حين, عدد يطول اتّجة بتحدّي عل, أم وأزيز بأيدي الا. هذه قد بداية العناد.</p>\r\n', NULL, 4, 2),
(7, 'Refund Policy', NULL, '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy</p>\r\n\r\n<p>text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen</p>\r\n\r\n<p>book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially</p>\r\n\r\n<p>unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages,</p>\r\n\r\n<p>and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem</p>\r\n\r\n<p>Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard</p>\r\n\r\n<p>dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type</p>\r\n\r\n<p>specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining</p>\r\n\r\n<p>essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum</p>\r\n\r\n<p>passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem</p>\r\n\r\n<p>Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s</p>\r\n\r\n<p>standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make</p>\r\n\r\n<p>a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting,</p>\r\n\r\n<p>remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing</p>\r\n\r\n<p>Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions</p>\r\n\r\n<p>of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been</p>\r\n\r\n<p>the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled</p>\r\n\r\n<p>it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting,</p>\r\n\r\n<p>remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing</p>\r\n\r\n<p>Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions</p>\r\n\r\n<p>of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been</p>\r\n\r\n<p>the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled</p>\r\n\r\n<p>it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting,</p>\r\n\r\n<p>remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing</p>\r\n\r\n<p>Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions</p>\r\n\r\n<p>of Lorem Ipsum.</p>\r\n', NULL, 1, 3),
(8, 'Rückgaberecht', NULL, '<p>Lorem ipsum dolor sit amet, putant expetenda ex nec. Ea luptatum verterem vis, sadipscing conclusionemque id ius. Perpetua molestiae mei at, iudicabit interesset vel ad. Efficiendi concludaturque sed in, mazim exerci indoctum at est, nibh nostro ea ius.</p>\r\n\r\n<p>Eros libris theophrastus pri ei. Alii imperdiet an vim. Elitr euripidis vituperatoribus nam at, id qui quod ipsum contentiones, eu paulo ignota verterem pri. In quem posse efficiendi duo, eu per everti iuvaret saperet. Id vel constituam neglegentur, utinam atomorum ei ius.</p>\r\n\r\n<p>Amet mollis antiopam ei sea, iusto aperiri in eam, sed euismod splendide ne. Iuvaret molestie efficiendi in vim, eum cu choro alienum menandri, at choro recteque tincidunt duo. Novum diceret expetendis his id, nominavi facilisi ne est. Movet labore intellegebat has ne, cu veritus mentitum aliquando pri, utamur nominati forensibus duo ut. Quo graece oporteat concludaturque at, qui docendi salutatus assentior cu, latine vocibus dissentias ius ea.</p>\r\n\r\n<p>Ne delenit graecis pri, nec in perpetua mnesarchum. Ex per civibus luptatum. Choro civibus quo no, duo ne omnis salutatus pertinacia, ipsum rationibus eu est. Eos te soleat eripuit indoctum, nobis alienum contentiones vim ad. Mei ut expetenda qualisque constituto. Ex vel case recteque, ea quot debet mucius sed.</p>\r\n\r\n<p>Sea verear labores luptatum ut. Eu pri dico errem ubique, solet saepe mediocritatem nec ad. Cibo urbanitas comprehensam cu qui, ex summo gloriatur quo, quo eu etiam dolor urbanitas. Sed ancillae efficiantur cu, errem omnes salutatus id qui. Tation eloquentiam an per, dolorem perfecto eam at.</p>\r\n', NULL, 2, 3),
(9, 'سياسة الاسترجاع', NULL, '<p>الجو تحرير السفن مع بين. تعد مع خيار المتّبعة, كلّ غينيا لعملة أن. الشطر الإثنان لم فقد, به، ٣٠ دارت الدول استراليا،. دنو تصفح موالية أي, عن تحرّك باستخدام لبلجيكا، دنو, و عرض هامش الشطر العدّ. حتى في لإعادة العاصمة.</p>\r\n\r\n<p>شعار وبدأت وتتحمّل من بين. ما أوزار أوروبا ويكيبيديا، لها, يكن تم الحكم ضمنها. دون أم الجنود ديسمبر. أحكم الإنذار، هو بعد, ضرب مدينة اعلان للمجهود عل, مدن بقصف العصبة التقليدية كل. لم حول عقبت بلديهما, وبعض أسيا الأعمال بـ بال. كلا الخطّة الشرقية كل, تم كما قبضتهم الحدود المنتصر, كرسي تصرّف ٣٠ دون.</p>\r\n\r\n<p>ولكسمبورغ واندونيسيا، كل لمّ, والتي لبولندا، بـ الى. أضف تم مسارح الثانية اليابان،, تم يكن دارت تصفح المارق, مع حين واحدة الأعمال المتّبعة. بـ على مشارف نهاية, فكان تحرّك الأمريكية في عدم. ٣٠ أملاً والنفيس التغييرات بها. غريمه الحيلولة أخذ ٣٠, قام عل موالية الهادي التكاليف. في مليارات والديون والإتحاد عدم, بـ ونتج استدعى أفريقيا به،.</p>\r\n\r\n<p>المشترك الشرقية تعد أي, وفي و وأكثرها الرئيسية المعاهدات. أي وبدأت المعاهدات نفس, من بلا عجّل الأمور بولندا،. الأمم إنطلاق عل ولم, عشوائية الأثناء، في مما. مقاطعة الوراء وتم تم, وتنصيب مواقعها بل يكن. تم كنقطة الشتاء، ومن, والإتحاد الرئيسية أي كلّ.</p>\r\n\r\n<p>أي انه الأثنان الجديدة،, لان كل مسرح تزامناً. ذات قد بزمام المضي الإحتفاظ, ٣٠ فسقط أعمال التغييرات لان. ليركز السيء مكثّفة هذه مع, بتطويق والديون تم فعل. بل ولم المبرمة واندونيسيا،, بقصف جيما أحكم هو دار. لم فقامت الشهير العسكري تحت, تصفح الستار والعتاد كما ٣٠.</p>\r\n\r\n<p>في بعض تطوير اوروبا أفريقيا. هو أسر وبعض انذار, أحدث إحكام قتيل، أن قام. مع فصل ونتج بخطوط المزيفة. مع النفط والمانيا لان. بـ تطوير بالرّد فقد.</p>\r\n\r\n<p>بالرّد بالرغم الأرواح أم نفس. من أخذ أخرى فاتّبع وانهاء. قد بشرية مساعدة الإقتصادية وتم, ومن ان جديدة المزيفة. أمّا سبتمبر أم بعض, الصفحة العالمية من أما. حيث بقسوة ابتدعها وقدّموا بل, كل الباهضة الخاسرة كما, تحت بشرية المشترك الاندونيسية و.</p>\r\n\r\n<p>العناد مقاطعة العالمي لم تلك. المزيفة الإقتصادية أن ذلك, إذ حاول اتفاق بالرغم كلّ. ثم أخرى المضي الدنمارك دنو, هو قامت قائمة للأراضي بلا. بلا من كنقطة عسكرياً, نتيجة لعملة المبرمة ان قبل.</p>\r\n\r\n<p>٣٠ دار الأخذ بريطانيا, ومن أم أوسع أواخر إستعمل. الى إستيلاء الواقعة لم, بل أدوات المارق وصل. تسمّى فرنسا وتزويده ٣٠ دنو, لغزو التي انتصارهم بـ حيث. ٣٠ قام فهرست الغالي قُدُماً. من بال أحدث وانتهاءً, حالية عسكرياً الموسوعة أن قبل. أدوات الحكومة ذات مع, الجوي بالتوقيع في على, بـ يبق الأعمال الأسيوي.</p>\r\n\r\n<p>إذ العالم، مسؤولية كلا, نفس إذ أراض بينما عسكرياً. أصقاع بقيادة عرض أم, ودول أسيا أراض ما لان. لمّ ان أجزاء وقامت المتّبعة, ونتج شاسعة المدن ان تحت. وقبل إحتار لمّ أي. شمال بالرّغم ٣٠ حين, عدد يطول اتّجة بتحدّي عل, أم وأزيز بأيدي الا. هذه قد بداية العناد.</p>\r\n', NULL, 4, 3),
(10, 'Privacy Policy', NULL, '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy</p>\r\n\r\n<p>text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen</p>\r\n\r\n<p>book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially</p>\r\n\r\n<p>unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages,</p>\r\n\r\n<p>and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem</p>\r\n\r\n<p>Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard</p>\r\n\r\n<p>dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type</p>\r\n\r\n<p>specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining</p>\r\n\r\n<p>essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum</p>\r\n\r\n<p>passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem</p>\r\n\r\n<p>Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s</p>\r\n\r\n<p>standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make</p>\r\n\r\n<p>a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting,</p>\r\n\r\n<p>remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing</p>\r\n\r\n<p>Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions</p>\r\n\r\n<p>of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been</p>\r\n\r\n<p>the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled</p>\r\n\r\n<p>it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting,</p>\r\n\r\n<p>remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing</p>\r\n\r\n<p>Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions</p>\r\n\r\n<p>of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been</p>\r\n\r\n<p>the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled</p>\r\n\r\n<p>it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting,</p>\r\n\r\n<p>remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing</p>\r\n\r\n<p>Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions</p>\r\n\r\n<p>of Lorem Ipsum.<br />\r\n<br />\r\n<img alt=\\\"\\\" src=\\\"http://localhost/services/resources/views/admin/plugins//vendor/ckeditor/plugins/imageuploader/uploads/90980f1.jpg\\\" style=\\\"height:256px; width:256px\\\" /></p>', NULL, 1, 4),
(11, 'Über uns', NULL, '<p>Lorem ipsum dolor sit amet, putant expetenda ex nec. Ea luptatum verterem vis, sadipscing conclusionemque id ius. Perpetua molestiae mei at, iudicabit interesset vel ad. Efficiendi concludaturque sed in, mazim exerci indoctum at est, nibh nostro ea ius.</p>\r\n\r\n<p>Eros libris theophrastus pri ei. Alii imperdiet an vim. Elitr euripidis vituperatoribus nam at, id qui quod ipsum contentiones, eu paulo ignota verterem pri. In quem posse efficiendi duo, eu per everti iuvaret saperet. Id vel constituam neglegentur, utinam atomorum ei ius.</p>\r\n\r\n<p>Amet mollis antiopam ei sea, iusto aperiri in eam, sed euismod splendide ne. Iuvaret molestie efficiendi in vim, eum cu choro alienum menandri, at choro recteque tincidunt duo. Novum diceret expetendis his id, nominavi facilisi ne est. Movet labore intellegebat has ne, cu veritus mentitum aliquando pri, utamur nominati forensibus duo ut. Quo graece oporteat concludaturque at, qui docendi salutatus assentior cu, latine vocibus dissentias ius ea.</p>\r\n\r\n<p>Ne delenit graecis pri, nec in perpetua mnesarchum. Ex per civibus luptatum. Choro civibus quo no, duo ne omnis salutatus pertinacia, ipsum rationibus eu est. Eos te soleat eripuit indoctum, nobis alienum contentiones vim ad. Mei ut expetenda qualisque constituto. Ex vel case recteque, ea quot debet mucius sed.</p>\r\n\r\n<p>Sea verear labores luptatum ut. Eu pri dico errem ubique, solet saepe mediocritatem nec ad. Cibo urbanitas comprehensam cu qui, ex summo gloriatur quo, quo eu etiam dolor urbanitas. Sed ancillae efficiantur cu, errem omnes salutatus id qui. Tation eloquentiam an per, dolorem perfecto eam at.</p>\r\n', NULL, 2, 4),
(12, 'معلومات عنا', NULL, '<p>الجو تحرير السفن مع بين. تعد مع خيار المتّبعة, كلّ غينيا لعملة أن. الشطر الإثنان لم فقد, به، ٣٠ دارت الدول استراليا،. دنو تصفح موالية أي, عن تحرّك باستخدام لبلجيكا، دنو, و عرض هامش الشطر العدّ. حتى في لإعادة العاصمة.</p>\r\n\r\n<p>شعار وبدأت وتتحمّل من بين. ما أوزار أوروبا ويكيبيديا، لها, يكن تم الحكم ضمنها. دون أم الجنود ديسمبر. أحكم الإنذار، هو بعد, ضرب مدينة اعلان للمجهود عل, مدن بقصف العصبة التقليدية كل. لم حول عقبت بلديهما, وبعض أسيا الأعمال بـ بال. كلا الخطّة الشرقية كل, تم كما قبضتهم الحدود المنتصر, كرسي تصرّف ٣٠ دون.</p>\r\n\r\n<p>ولكسمبورغ واندونيسيا، كل لمّ, والتي لبولندا، بـ الى. أضف تم مسارح الثانية اليابان،, تم يكن دارت تصفح المارق, مع حين واحدة الأعمال المتّبعة. بـ على مشارف نهاية, فكان تحرّك الأمريكية في عدم. ٣٠ أملاً والنفيس التغييرات بها. غريمه الحيلولة أخذ ٣٠, قام عل موالية الهادي التكاليف. في مليارات والديون والإتحاد عدم, بـ ونتج استدعى أفريقيا به،.</p>\r\n\r\n<p>المشترك الشرقية تعد أي, وفي و وأكثرها الرئيسية المعاهدات. أي وبدأت المعاهدات نفس, من بلا عجّل الأمور بولندا،. الأمم إنطلاق عل ولم, عشوائية الأثناء، في مما. مقاطعة الوراء وتم تم, وتنصيب مواقعها بل يكن. تم كنقطة الشتاء، ومن, والإتحاد الرئيسية أي كلّ.</p>\r\n\r\n<p>أي انه الأثنان الجديدة،, لان كل مسرح تزامناً. ذات قد بزمام المضي الإحتفاظ, ٣٠ فسقط أعمال التغييرات لان. ليركز السيء مكثّفة هذه مع, بتطويق والديون تم فعل. بل ولم المبرمة واندونيسيا،, بقصف جيما أحكم هو دار. لم فقامت الشهير العسكري تحت, تصفح الستار والعتاد كما ٣٠.</p>\r\n\r\n<p>في بعض تطوير اوروبا أفريقيا. هو أسر وبعض انذار, أحدث إحكام قتيل، أن قام. مع فصل ونتج بخطوط المزيفة. مع النفط والمانيا لان. بـ تطوير بالرّد فقد.</p>\r\n\r\n<p>بالرّد بالرغم الأرواح أم نفس. من أخذ أخرى فاتّبع وانهاء. قد بشرية مساعدة الإقتصادية وتم, ومن ان جديدة المزيفة. أمّا سبتمبر أم بعض, الصفحة العالمية من أما. حيث بقسوة ابتدعها وقدّموا بل, كل الباهضة الخاسرة كما, تحت بشرية المشترك الاندونيسية و.</p>\r\n\r\n<p>العناد مقاطعة العالمي لم تلك. المزيفة الإقتصادية أن ذلك, إذ حاول اتفاق بالرغم كلّ. ثم أخرى المضي الدنمارك دنو, هو قامت قائمة للأراضي بلا. بلا من كنقطة عسكرياً, نتيجة لعملة المبرمة ان قبل.</p>\r\n\r\n<p>٣٠ دار الأخذ بريطانيا, ومن أم أوسع أواخر إستعمل. الى إستيلاء الواقعة لم, بل أدوات المارق وصل. تسمّى فرنسا وتزويده ٣٠ دنو, لغزو التي انتصارهم بـ حيث. ٣٠ قام فهرست الغالي قُدُماً. من بال أحدث وانتهاءً, حالية عسكرياً الموسوعة أن قبل. أدوات الحكومة ذات مع, الجوي بالتوقيع في على, بـ يبق الأعمال الأسيوي.</p>\r\n\r\n<p>إذ العالم، مسؤولية كلا, نفس إذ أراض بينما عسكرياً. أصقاع بقيادة عرض أم, ودول أسيا أراض ما لان. لمّ ان أجزاء وقامت المتّبعة, ونتج شاسعة المدن ان تحت. وقبل إحتار لمّ أي. شمال بالرّغم ٣٠ حين, عدد يطول اتّجة بتحدّي عل, أم وأزيز بأيدي الا. هذه قد بداية العناد.</p>\r\n', NULL, 4, 4),
(13, 'Mobile Phones', NULL, '<p>test</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>test</p>', NULL, 1, 5),
(14, 'Mobile Phones', NULL, '<p>asass</p>', NULL, 1, 6),
(17, 'test page en', 'test page arabic', 'test des en', 'test des arabic', 1, 9);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('test@gmail.com', '$2y$10$nKgadMnI0X58QJFoECNuhOfOM8ztA1tcMQeGnUB90KcJtB5e7kzfi', '2019-01-04 03:46:45');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
CREATE TABLE IF NOT EXISTS `permissions` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'create-product', 'Create product', '', '2018-10-17 04:31:23', '2018-10-17 04:31:23'),
(2, 'view-product', 'View product', '', '2018-10-17 04:31:23', '2018-10-17 04:31:23'),
(3, 'update-product', 'Update product', '', '2018-10-17 04:31:23', '2018-10-17 04:31:23'),
(4, 'delete-product', 'Delete product', '', '2018-10-17 04:31:23', '2018-10-17 04:31:23'),
(5, 'update-order', 'Update order', '', '2018-10-17 04:31:23', '2018-10-17 04:31:23');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

DROP TABLE IF EXISTS `permission_role`;
CREATE TABLE IF NOT EXISTS `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_role_id_foreign` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(1, 2),
(2, 2),
(3, 2),
(4, 2),
(5, 2),
(1, 3),
(2, 3),
(3, 3);

-- --------------------------------------------------------

--
-- Table structure for table `permission_user`
--

DROP TABLE IF EXISTS `permission_user`;
CREATE TABLE IF NOT EXISTS `permission_user` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `user_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`user_id`,`permission_id`,`user_type`),
  KEY `permission_user_permission_id_foreign` (`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `brand_id` int(10) UNSIGNED DEFAULT NULL,
  `sku` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `short_description` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `duration` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cover` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quantity` int(11) DEFAULT '0',
  `payment` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` decimal(8,2) NOT NULL,
  `sale_price` decimal(8,2) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `length` decimal(8,2) DEFAULT NULL,
  `width` decimal(8,2) DEFAULT NULL,
  `height` decimal(8,2) DEFAULT NULL,
  `distance_unit` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `weight` decimal(8,2) DEFAULT '0.00',
  `mass_unit` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `new_product_flag` enum('1','0') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `products_brand_id_foreign` (`brand_id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `brand_id`, `sku`, `name`, `slug`, `description`, `short_description`, `duration`, `cover`, `quantity`, `payment`, `price`, `sale_price`, `status`, `length`, `width`, `height`, `distance_unit`, `weight`, `mass_unit`, `created_at`, `updated_at`, `new_product_flag`) VALUES
(1, 2, NULL, 'Leakages', 'leakages', 'derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.', NULL, NULL, NULL, 500, NULL, '25.00', NULL, 1, NULL, NULL, NULL, NULL, '0.00', NULL, '2018-12-03 05:05:58', '2018-12-27 04:55:46', '0'),
(2, 1, NULL, 'Blockages', 'blockages', 'derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.', NULL, NULL, NULL, 500, NULL, '23.00', NULL, 1, NULL, NULL, NULL, NULL, '0.00', NULL, '2018-12-03 05:06:27', '2018-12-27 04:55:46', '0'),
(3, 2, NULL, 'water heaters', 'water-heaters', 'derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.', NULL, NULL, NULL, 500, NULL, '21.00', NULL, 1, NULL, NULL, NULL, NULL, '0.00', NULL, '2018-12-03 05:07:00', '2019-01-02 06:10:49', '0'),
(4, 2, NULL, 'Gas', 'gas', 'derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.', NULL, NULL, NULL, 498, NULL, '15.00', NULL, 1, NULL, NULL, NULL, NULL, '0.00', NULL, '2018-12-03 05:07:28', '2019-02-15 00:04:22', '0'),
(5, 1, NULL, 'AC Cleaning', 'ac-cleaning', 'derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.', NULL, NULL, '1546665805.prod_1492692312.jpg', 500, NULL, '22.00', NULL, 1, NULL, NULL, NULL, NULL, '0.00', NULL, '2018-12-03 05:07:48', '2019-01-12 03:58:01', '0'),
(6, 1, NULL, 'AC Fixing', 'ac-fixing', 'derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.', NULL, NULL, NULL, 500, NULL, '22.00', NULL, 1, NULL, NULL, NULL, NULL, '0.00', NULL, '2018-12-03 05:08:12', '2018-12-03 05:08:12', '0'),
(7, 1, NULL, 'AC Relocation', 'ac-relocation', 'derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.', NULL, NULL, NULL, 500, NULL, '25.00', NULL, 1, NULL, NULL, NULL, NULL, '0.00', NULL, '2018-12-03 05:08:34', '2018-12-03 05:08:37', '0'),
(8, 2, NULL, 'Tiles Fixing', 'tiles-fixing', 'derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.', NULL, NULL, NULL, 10, NULL, '21.00', NULL, 1, NULL, NULL, NULL, NULL, '0.00', NULL, '2018-12-03 05:12:08', '2018-12-03 05:12:08', '0'),
(9, 2, NULL, 'Tiles removel', 'tiles-removel', 'derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.', NULL, NULL, NULL, 0, NULL, '22.00', NULL, 1, NULL, NULL, NULL, NULL, '0.00', NULL, '2018-12-03 05:12:36', '2018-12-03 05:12:36', '0'),
(10, 1, NULL, 'Cleaning', 'cleaning', 'derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.', NULL, NULL, '1545902786.logo_market.png', 0, NULL, '36.00', NULL, 1, NULL, NULL, NULL, NULL, '0.00', NULL, '2018-12-03 05:13:31', '2018-12-27 03:56:26', '0'),
(11, 1, NULL, 'Shifting', 'shifting', 'derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.derehrejh eyrryef erey reygtre.hgeruy eyertueryt eyteryu.eruywe eyteruy feryueruer.', NULL, NULL, '1545290168.vnecoms_marketplace.png', 0, NULL, '45.00', NULL, 1, NULL, NULL, NULL, NULL, '0.00', NULL, '2018-12-03 05:13:55', '2018-12-20 01:46:08', '0'),
(12, 2, NULL, 'test', 'test', 'rtesrt', NULL, NULL, '1548329092.2019-01-21 (2).jpg', 0, NULL, '34.00', NULL, 1, NULL, NULL, NULL, NULL, '0.00', NULL, '2019-01-24 05:54:52', '2019-01-24 05:54:52', '0'),
(19, 2, NULL, 'testservice', 'testservice', 'sfs reer tere eertr rtert', NULL, NULL, '1548651987.2019-01-21 (2).jpg', 0, NULL, '36.00', NULL, 1, NULL, NULL, NULL, NULL, '0.00', NULL, '2019-01-25 00:26:35', '2019-01-27 23:36:27', '0'),
(23, 2, NULL, 'dgfdfgd', 'dgfdfgd', 'sdfsd', NULL, NULL, NULL, 0, NULL, '34.00', NULL, 1, NULL, NULL, NULL, NULL, '0.00', NULL, '2019-01-25 04:32:26', '2019-01-25 04:32:26', '0'),
(24, 2, NULL, 'fdgfh', 'fdgfh', 'fghbfcgh', NULL, NULL, '1548410619.2019-01-21 (2).jpg', 0, NULL, '23.00', NULL, 1, NULL, NULL, NULL, NULL, '0.00', NULL, '2019-01-25 04:33:39', '2019-01-25 04:33:39', '0'),
(25, 2, NULL, 'fdgfhxvbcgnh', 'fdgfhxvbcgnh', 'fghbfcgh', NULL, NULL, '1548657035.2019-01-21 (4).jpg', 0, NULL, '23.00', NULL, 1, NULL, NULL, NULL, NULL, '0.00', NULL, '2019-01-25 04:35:26', '2019-01-29 04:41:24', '0'),
(27, 2, NULL, 'yuyu', 'yuyu', 'yuyuyu', NULL, NULL, '1548742056.2019-01-21 (4).jpg', 0, NULL, '78.00', NULL, 1, NULL, NULL, NULL, NULL, '0.00', NULL, '2019-01-29 00:37:36', '2019-01-29 00:37:36', '0'),
(28, 2, NULL, 'qwqe', 'qwqe', 'erer erwer wrtw tetw e re', 'qeweq erwserw rere', 'time 10 min', NULL, 0, NULL, '56.00', NULL, 1, NULL, NULL, NULL, NULL, '0.00', NULL, '2019-01-29 03:48:45', '2019-01-29 04:50:14', '0');

-- --------------------------------------------------------

--
-- Table structure for table `product_attributes`
--

DROP TABLE IF EXISTS `product_attributes`;
CREATE TABLE IF NOT EXISTS `product_attributes` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `quantity` int(11) NOT NULL,
  `price` decimal(8,2) DEFAULT NULL,
  `sale_price` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `default` tinyint(4) NOT NULL DEFAULT '0',
  `product_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `product_attributes_product_id_foreign` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

DROP TABLE IF EXISTS `product_images`;
CREATE TABLE IF NOT EXISTS `product_images` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `product_id` int(10) UNSIGNED NOT NULL,
  `src` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `product_images_product_id_index` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `provinces`
--

DROP TABLE IF EXISTS `provinces`;
CREATE TABLE IF NOT EXISTS `provinces` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country_id` int(10) UNSIGNED NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `provinces_country_id_index` (`country_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `related_products`
--

DROP TABLE IF EXISTS `related_products`;
CREATE TABLE IF NOT EXISTS `related_products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `related_product_id` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=39 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `request_attachment`
--

DROP TABLE IF EXISTS `request_attachment`;
CREATE TABLE IF NOT EXISTS `request_attachment` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `order_id` int(15) DEFAULT NULL,
  `images` varchar(900) DEFAULT NULL,
  `type` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `request_attachment`
--

INSERT INTO `request_attachment` (`id`, `order_id`, `images`, `type`) VALUES
(1, 1, 'orderAttachment/cKyIqq3GSM85ockGCTsq0OuQZPrlZHaX0CwG2QBR.pdf', 'document'),
(2, 2, 'orderAttachment/nKr93Zz2qfbLJQGkITE7uit5AgepedP3g47c9WbX.png', 'image'),
(3, 2, 'orderAttachment/YqNWBbxvDKpT5o44RgcOMkEAd5sPlEIqcWQy9jID.png', 'image'),
(4, 3, 'orderAttachment/M5XaA8S0gHvkmA6JSsZDOw47pFp3igHlEyeHhrsY.png', 'image'),
(5, 3, 'orderAttachment/NJ2Esmr3RfdS0y0WSttTVnLItqEC3MauTY0ZXxyR.png', 'image'),
(7, 6, 'orderAttachment/dzKmjWixqWqMFTE7EE0ygFOTjNPHG6IRjhNHOqSv.jpeg', 'image'),
(9, 7, 'orderAttachment/ZrgXoV9WkKOwK0UUSP41IPc5MmoyWG05BJByMS5a.jpeg', 'image'),
(10, 7, 'orderAttachment/UcWH9hJNI8zFcbWql0cCd1gD8rjzVRyzerCVtkP6.jpeg', 'image'),
(11, 7, 'orderAttachment/CeMvZGwp7TiDNIfkaDPEsYrUnGukS1OWxHsGYtWu.jpeg', 'image'),
(12, 7, 'orderAttachment/u2az0eNcpPAoouf61qO30gMURloP8kIRr7rWJR7Q.jpeg', 'image'),
(13, 7, 'orderAttachment/AWU6KHVr3zFaflLaH7vgCtB6fnCQOWDsuriQXhsM.jpeg', 'image'),
(14, 7, 'orderAttachment/2aqOrZkWlbEIjHeWHYFE6hNo1uXhMuaukeZwzQkz.jpeg', 'image'),
(15, 7, 'orderAttachment/mVYhEOXXGHKBCe6skQKREKGU4p42le9MOfhZKQPL.jpeg', 'image'),
(16, 21, 'orderAttachment/g8Vd4LQWk0HCFUuxpzy8tchIYWYXCos3SirZy86k.mp4', 'video'),
(17, 21, 'orderAttachment/KtUBmIFcQKhKrB6EOUbRsRffVkE7ihwWIF6SkOLg.mp4', 'video'),
(18, 21, 'orderAttachment/OGZvLr3P795nRA1zFIpw9eAg1l8M3ZmWEu5WQ6Jh.mp4', 'video');

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

DROP TABLE IF EXISTS `reviews`;
CREATE TABLE IF NOT EXISTS `reviews` (
  `reviews_id` int(11) NOT NULL AUTO_INCREMENT,
  `products_id` int(11) NOT NULL,
  `customers_id` int(11) DEFAULT NULL,
  `customers_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `reviews_rating` int(1) DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `last_modified` datetime DEFAULT NULL,
  `reviews_status` tinyint(1) NOT NULL DEFAULT '0',
  `reviews_read` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`reviews_id`),
  KEY `idx_reviews_products_id` (`products_id`),
  KEY `idx_reviews_customers_id` (`customers_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `reviews`
--

INSERT INTO `reviews` (`reviews_id`, `products_id`, `customers_id`, `customers_name`, `reviews_rating`, `date_added`, `last_modified`, `reviews_status`, `reviews_read`) VALUES
(1, 99, 17, 'Alvin jose', 3, '2018-10-22 01:10:41', '2018-10-22 01:10:41', 0, 0),
(2, 7, 17, 'Alvin jose', 4, '2018-10-22 01:10:20', '2018-10-22 01:10:20', 0, 0),
(3, 9, 17, 'Alvin jose', 4, '2018-10-22 01:10:43', '2018-10-22 01:10:43', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `reviews_description`
--

DROP TABLE IF EXISTS `reviews_description`;
CREATE TABLE IF NOT EXISTS `reviews_description` (
  `reviews_id` int(11) NOT NULL,
  `languages_id` int(11) NOT NULL,
  `reviews_text` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`reviews_id`,`languages_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `reviews_description`
--

INSERT INTO `reviews_description` (`reviews_id`, `languages_id`, `reviews_text`) VALUES
(1, 1, 'Test ratingggg'),
(2, 1, 'Test ratinggggggggggg'),
(3, 1, 'Hiiiii');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'superadmin', 'Super Admin', '', '2018-10-17 04:31:24', '2018-10-17 04:31:24'),
(2, 'admin', 'Admin', '', '2018-10-17 04:31:24', '2018-10-17 04:31:24'),
(3, 'clerk', 'Clerk', '', '2018-10-17 04:31:24', '2018-10-17 04:31:24');

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

DROP TABLE IF EXISTS `role_user`;
CREATE TABLE IF NOT EXISTS `role_user` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `user_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`,`user_type`),
  KEY `role_user_role_id_foreign` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`role_id`, `user_id`, `user_type`) VALUES
(1, 1, 'App\\Shop\\Employees\\Employee'),
(2, 2, 'App\\Shop\\Employees\\Employee'),
(3, 3, 'App\\Shop\\Employees\\Employee');

-- --------------------------------------------------------

--
-- Table structure for table `service_countries`
--

DROP TABLE IF EXISTS `service_countries`;
CREATE TABLE IF NOT EXISTS `service_countries` (
  `id` int(56) NOT NULL AUTO_INCREMENT,
  `product_id` int(56) NOT NULL,
  `country_id` int(57) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `shoppingcart`
--

DROP TABLE IF EXISTS `shoppingcart`;
CREATE TABLE IF NOT EXISTS `shoppingcart` (
  `identifier` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `instance` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`identifier`,`instance`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

DROP TABLE IF EXISTS `states`;
CREATE TABLE IF NOT EXISTS `states` (
  `state` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country_id` int(10) UNSIGNED NOT NULL,
  KEY `states_country_id_foreign` (`country_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`state`, `state_code`, `country_id`) VALUES
('Alaska', 'AK', 226),
('Alabama', 'AL', 226),
('Arkansas', 'AR', 226),
('Arizona', 'AZ', 226),
('California', 'CA', 226),
('Colorado', 'CO', 226),
('Connecticut', 'CT', 226),
('District of Columbia', 'DC', 226),
('Delaware', 'DE', 226),
('Florida', 'FL', 226),
('Georgia', 'GA', 226),
('Hawaii', 'HI', 226),
('Iowa', 'IA', 226),
('Idaho', 'ID', 226),
('Illinois', 'IL', 226),
('Indiana', 'IN', 226),
('Kansas', 'KS', 226),
('Kentucky', 'KY', 226),
('Louisiana', 'LA', 226),
('Massachusetts', 'MA', 226),
('Maryland', 'MD', 226),
('Maine', 'ME', 226),
('Michigan', 'MI', 226),
('Minnesota', 'MN', 226),
('Missouri', 'MO', 226),
('Mississippi', 'MS', 226),
('Montana', 'MT', 226),
('North Carolina', 'NC', 226),
('North Dakota', 'ND', 226),
('Nebraska', 'NE', 226),
('New Hampshire', 'NH', 226),
('New Jersey', 'NJ', 226),
('New Mexico', 'NM', 226),
('Nevada', 'NV', 226),
('New York', 'NY', 226),
('Ohio', 'OH', 226),
('Oklahoma', 'OK', 226),
('Oregon', 'OR', 226),
('Pennsylvania', 'PA', 226),
('Rhode Island', 'RI', 226),
('South Carolina', 'SC', 226),
('South Dakota', 'SD', 226),
('Tennessee', 'TN', 226),
('Texas', 'TX', 226),
('Utah', 'UT', 226),
('Virginia', 'VA', 226),
('Vermont', 'VT', 226),
('Washington', 'WA', 226),
('Wisconsin', 'WI', 226),
('West Virginia', 'WV', 226),
('Wyoming', 'WY', 226);

-- --------------------------------------------------------

--
-- Table structure for table `subscribers`
--

DROP TABLE IF EXISTS `subscribers`;
CREATE TABLE IF NOT EXISTS `subscribers` (
  `subscriber_id` int(11) NOT NULL AUTO_INCREMENT,
  `email_address` varchar(50) NOT NULL,
  `created_date` datetime NOT NULL,
  PRIMARY KEY (`subscriber_id`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subscribers`
--

INSERT INTO `subscribers` (`subscriber_id`, `email_address`, `created_date`) VALUES
(12, 'sanil63711@gmail.com', '2018-11-21 04:50:12'),
(11, 'sanil637@gmail.com', '2018-11-21 04:48:48'),
(10, 'fadfsdfsdf@gdfgd.com', '2018-11-13 12:21:35'),
(13, 'chinju@gmail.com', '2018-12-14 08:56:56'),
(14, 'testtest2@gmail.com', '2018-12-26 10:21:29');

-- --------------------------------------------------------

--
-- Table structure for table `subscriptions`
--

DROP TABLE IF EXISTS `subscriptions`;
CREATE TABLE IF NOT EXISTS `subscriptions` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `customer_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stripe_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stripe_plan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` int(11) NOT NULL,
  `trial_ends_at` timestamp NULL DEFAULT NULL,
  `ends_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `tax_class`
--

DROP TABLE IF EXISTS `tax_class`;
CREATE TABLE IF NOT EXISTS `tax_class` (
  `tax_class_id` int(11) NOT NULL AUTO_INCREMENT,
  `tax_class_title` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `tax_class_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_modified` datetime DEFAULT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`tax_class_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tax_class`
--

INSERT INTO `tax_class` (`tax_class_id`, `tax_class_title`, `tax_class_description`, `last_modified`, `date_added`) VALUES
(1, 'Handling Charge', 'This charge is applied on products related to Qatar areas.', '2018-10-18 07:06:34', '2017-08-07 07:06:53');

-- --------------------------------------------------------

--
-- Table structure for table `tax_rates`
--

DROP TABLE IF EXISTS `tax_rates`;
CREATE TABLE IF NOT EXISTS `tax_rates` (
  `tax_rates_id` int(11) NOT NULL AUTO_INCREMENT,
  `tax_zone_id` int(11) NOT NULL,
  `tax_class_id` int(11) NOT NULL,
  `tax_priority` int(5) DEFAULT '1',
  `tax_rate` decimal(7,2) NOT NULL,
  `tax_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_modified` datetime DEFAULT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`tax_rates_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tax_rates`
--

INSERT INTO `tax_rates` (`tax_rates_id`, `tax_zone_id`, `tax_class_id`, `tax_priority`, `tax_rate`, `tax_description`, `last_modified`, `date_added`) VALUES
(1, 1, 1, 1, '0.00', '', '2018-10-03 07:22:00', '2017-08-07 07:07:45'),
(2, 2, 1, 1, '0.00', '', NULL, '2018-10-17 17:38:55');

-- --------------------------------------------------------

--
-- Table structure for table `wishlist`
--

DROP TABLE IF EXISTS `wishlist`;
CREATE TABLE IF NOT EXISTS `wishlist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `created_date` timestamp NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq` (`product_id`,`customer_id`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `zones`
--

DROP TABLE IF EXISTS `zones`;
CREATE TABLE IF NOT EXISTS `zones` (
  `zone_id` int(11) NOT NULL AUTO_INCREMENT,
  `zone_country_id` int(11) UNSIGNED NOT NULL,
  `zone_code` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `zone_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`zone_id`),
  KEY `idx_zones_country_id` (`zone_country_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `zones`
--

INSERT INTO `zones` (`zone_id`, `zone_country_id`, `zone_code`, `zone_name`) VALUES
(1, 174, 'QA-DA', 'Al Daayen'),
(2, 174, 'QA-DH', 'Doha'),
(3, 174, 'QA-KH', 'Al Khor'),
(4, 174, 'QA-RA', 'Al Rayyan'),
(5, 174, 'QA-SH', 'Shahaniya'),
(6, 174, 'QA-SM', 'Shamal'),
(7, 174, 'QA-US', 'Um Salal'),
(8, 174, 'QA-WA', 'Al Wakrah');

-- --------------------------------------------------------

--
-- Table structure for table `zones_to_geo_zones`
--

DROP TABLE IF EXISTS `zones_to_geo_zones`;
CREATE TABLE IF NOT EXISTS `zones_to_geo_zones` (
  `association_id` int(11) NOT NULL AUTO_INCREMENT,
  `zone_country_id` int(11) NOT NULL,
  `zone_id` int(11) DEFAULT NULL,
  `geo_zone_id` int(11) DEFAULT NULL,
  `last_modified` datetime DEFAULT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`association_id`),
  KEY `idx_zones_to_geo_zones_country_id` (`zone_country_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `zone_courier_mapping`
--

DROP TABLE IF EXISTS `zone_courier_mapping`;
CREATE TABLE IF NOT EXISTS `zone_courier_mapping` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `zone_id` int(11) UNSIGNED NOT NULL,
  `courier_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `zone_id` (`zone_id`) USING BTREE,
  KEY `courier_id` (`courier_id`)
) ENGINE=MyISAM AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `zone_courier_mapping`
--

INSERT INTO `zone_courier_mapping` (`id`, `zone_id`, `courier_id`) VALUES
(9, 3, 1),
(8, 2, 1),
(7, 1, 1),
(10, 4, 1),
(11, 5, 1),
(12, 6, 1),
(13, 7, 1),
(14, 8, 1),
(26, 5, 2),
(25, 3, 2),
(24, 1, 2);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `addresses`
--
ALTER TABLE `addresses`
  ADD CONSTRAINT `addresses_country_id_foreign` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`),
  ADD CONSTRAINT `addresses_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`);

--
-- Constraints for table `attribute_values`
--
ALTER TABLE `attribute_values`
  ADD CONSTRAINT `attribute_values_attribute_id_foreign` FOREIGN KEY (`attribute_id`) REFERENCES `attributes` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `attribute_value_product_attribute`
--
ALTER TABLE `attribute_value_product_attribute`
  ADD CONSTRAINT `attribute_value_product_attribute_attribute_value_id_foreign` FOREIGN KEY (`attribute_value_id`) REFERENCES `attribute_values` (`id`),
  ADD CONSTRAINT `attribute_value_product_attribute_product_attribute_id_foreign` FOREIGN KEY (`product_attribute_id`) REFERENCES `product_attributes` (`id`);

--
-- Constraints for table `category_product`
--
ALTER TABLE `category_product`
  ADD CONSTRAINT `category_product_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`),
  ADD CONSTRAINT `category_product_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`);

--
-- Constraints for table `cities`
--
ALTER TABLE `cities`
  ADD CONSTRAINT `cities_province_id_foreign` FOREIGN KEY (`province_id`) REFERENCES `provinces` (`id`);

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_address_id_foreign` FOREIGN KEY (`address_id`) REFERENCES `addresses` (`id`),
  ADD CONSTRAINT `orders_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`),
  ADD CONSTRAINT `orders_order_status_id_foreign` FOREIGN KEY (`order_status_id`) REFERENCES `order_statuses` (`id`);

--
-- Constraints for table `order_product`
--
ALTER TABLE `order_product`
  ADD CONSTRAINT `order_product_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`),
  ADD CONSTRAINT `order_product_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`);

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `permission_user`
--
ALTER TABLE `permission_user`
  ADD CONSTRAINT `permission_user_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_brand_id_foreign` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`id`);

--
-- Constraints for table `product_attributes`
--
ALTER TABLE `product_attributes`
  ADD CONSTRAINT `product_attributes_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`);

--
-- Constraints for table `product_images`
--
ALTER TABLE `product_images`
  ADD CONSTRAINT `product_images_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `provinces`
--
ALTER TABLE `provinces`
  ADD CONSTRAINT `provinces_country_id_foreign` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`);

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `states`
--
ALTER TABLE `states`
  ADD CONSTRAINT `states_country_id_foreign` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`);

--
-- Constraints for table `zones`
--
ALTER TABLE `zones`
  ADD CONSTRAINT `fk_country` FOREIGN KEY (`zone_country_id`) REFERENCES `countries` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
